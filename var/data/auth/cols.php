<?
namespace Anon;


$role = tron
([
   'name' => ['base'=>'TEXT NOT NULL UNIQUE', 'test'=>':word:', 'auto'=>null],
   'meta' => ['base'=>'TEXT NOT NULL', 'test'=>':bool:', 'auto'=>false],
]);


$user = tron
([
   'name' => ['base'=>'TEXT NOT NULL UNIQUE', 'test'=>':word:'],
   'pass' => ['base'=>'TEXT NOT NULL', 'test'=>':hash:'],
   'mail' => ['base'=>'TEXT NOT NULL', 'test'=>':mail:'],
   'rank' => ['base'=>'TEXT NOT NULL', 'test'=>':numr:', 'auto'=>0.1],
   'face' => ['base'=>'TEXT NOT NULL', 'test'=>':path:', 'auto'=>'/$/gui/mug2.jpg'],
   'tags' => ['base'=>'TEXT NOT NULL', 'test'=>':tupl:', 'auto'=>[]],
   'note' => ['base'=>'TEXT NOT NULL', 'test'=>':text:', 'auto'=>''],
   'made' => ['base'=>'INT NOT NULL', 'test'=>':numr:', 'auto'=>time()],
   'seen' => ['base'=>'INT NOT NULL', 'test'=>':numr:', 'auto'=>0],
   'meta' => ['base'=>'TEXT NOT NULL', 'test'=>':bool:', 'auto'=>false],
]);


$clan = tron
([
   'user' => ['base'=>'TEXT NOT NULL', 'test'=>':word:'],
   'role' => ['base'=>'TEXT NOT NULL', 'test'=>':text:', 'isin'=>'role:name'],
   'levl' => ['base'=>'TEXT NOT NULL', 'test'=>':numr:', 'auto'=>0.1],
]);


$path = tron
([
   'path' => ['base'=>'TEXT NOT NULL UNIQUE', 'test'=>':path:'],
   'role' => ['base'=>'TEXT NOT NULL', 'test'=>':tupl:'],
   'rank' => ['base'=>'TEXT NOT NULL', 'test'=>':numr:', 'auto'=>0],
   'mode' => ['base'=>'TEXT NOT NULL', 'test'=>':word:', 'auto'=>''],
   'face' => ['base'=>'TEXT NOT NULL', 'test'=>':word:', 'auto'=>''],
   'deed' => ['base'=>'TEXT NOT NULL', 'test'=>':word:', 'auto'=>''],
   'dbug' => ['base'=>'TEXT NOT NULL', 'test'=>':bool:', 'auto'=>true],
   'meta' => ['base'=>'TEXT NOT NULL', 'test'=>':bool:', 'auto'=>false],
]);


$cred = tron
([
   'name' => ['base'=>'TEXT NOT NULL', 'test'=>':word:'],
   'whom' => ['base'=>'TEXT NOT NULL', 'test'=>':tupl:', 'isin'=>'user:name'],
   'host' => ['base'=>'TEXT NOT NULL', 'test'=>':host:'],
   'user' => ['base'=>'TEXT NOT NULL', 'test'=>':word:'],
   'pass' => ['base'=>'TEXT NOT NULL', 'test'=>':pass:'],
   'mail' => ['base'=>'TEXT NOT NULL', 'test'=>':mail:'],
]);
