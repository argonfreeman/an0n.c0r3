<?
namespace Anon;


$boot = // list
[
   ['SITEMODE','DEVL',["DEVL","LIVE","DOWN"]],
   ['INTRFACE','ANY',["ANY","API"]],
   ['VIEWDIRS',false,[true,false]],
   ['DENYBOTS',false,[true,false]],
   ['HOMEPATH','/c0r3/doc/aard.md'],
   ['AUTOPATH','/c0r3/htm/aard.htm'],
   ['BOOTPATH','/c0r3/htm/boot.htm'],
   ['DBUGPATH','/c0r3/htm/dbug.htm'],
   ['STATPATH','/c0r3/htm/stat.htm'],
   ['GAGROBOT','/contact-mobile-email-address-book'],
   ['DIRINDEX',tron(["name"=>["aard","index"],"type"=>["php","htm","md","js"]])],
   ['CORSFROM','*'],
   ['CSPRULES',[]],
   ['PACKVRSN','0.1.0'],
   ['NEEDSPHP','5.6'],
   ['MAILFROM',null],
];


$path = // list
[
   [COREPATH,404],
   ['*/.*',404],
   ['*.php',404],
   ['*.sdb',404],
   ['*.sql',404],
   ['/c0r3/var/*',404],
];


$stat = // list
[
   [100,"Continue"],
   [101,"Switching Protocols"],
   [102,"Processing"],
   [200,"OK"],
   [201,"Created"],
   [202,"Accepted"],
   [203,"Non-Authoritative Information"],
   [204,"No Content"],
   [205,"Reset Content"],
   [206,"Partial Content"],
   [207,"Multi-Status"],
   [300,"Multiple Choices"],
   [301,"Moved Permanently"],
   [302,"Moved Temporarily"],
   [303,"See Other"],
   [304,"Not Modified"],
   [305,"Use Proxy"],
   [307,"Temporary Redirect"],
   [400,"Bad Request"],
   [401,"Unauthorized"],
   [402,"Payment Required"],
   [403,"Forbidden"],
   [404,"Not Found"],
   [405,"Method Not Allowed"],
   [406,"Not Acceptable"],
   [407,"Proxy Authentication Required"],
   [408,"Request Time-out"],
   [409,"Conflict"],
   [410,"Gone"],
   [411,"Length Required"],
   [412,"Precondition Failed"],
   [413,"Request Entity Too Large"],
   [414,"Request-URI Too Large"],
   [415,"Unsupported Media Type"],
   [416,"Requested Range Not Satisfiable"],
   [417,"Expectation Failed"],
   [418,"I'm a teapot"],
   [420,"Wrong Interface"],
   [422,"Unprocessable Entity"],
   [423,"Locked"],
   [424,"Failed Dependency"],
   [425,"Unordered Collection"],
   [426,"Upgrade Required"],
   [428,"Precondition Required"],
   [429,"Too Many Requests"],
   [431,"Request Header Fields Too Large"],
   [500,"Internal Server Error"],
   [501,"Not Implemented"],
   [502,"Bad Gateway"],
   [503,"Service Unavailable"],
   [504,"Gateway Time-out"],
   [505,"HTTP Version Not Supported"],
   [506,"Variant Also Negotiates"],
   [507,"Insufficient Storage"],
   [509,"Bandwidth Limit Exceeded"],
   [510,"Not Extended"],
   [511,"Network Authentication Required"],
];


$mime = // list
[
   ['3gp','video/3gpp'],
   ['au','audio/basic'],
   ['avi','video/x-msvideo'],
   ['bin','application/octet-stream'],
   ['bmp','image/bmp'],
   ['crt','application/x-x509-ca-cert'],
   ['css','text/css'],
   ['csv','text/csv'],
   ['fldr','inode/directory'],
   ['file','file/unknown'],
   ['none','none/unknown'],
   ['flac','audio/x-flac'],
   ['fnt','text/css'],
   ['gif','image/gif'],
   ['htm','text/html'],
   ['html','text/html'],
   ['ico','image/x-icon'],
   ['ics','text/calendar'],
   ['img','image/png'],
   ['inf','text/plain'],
   ['iso','application/x-iso9660-image'],
   ['jar','application/java-archive'],
   ['jpeg','image/jpeg'],
   ['jpg','image/jpeg'],
   ['js','application/javascript'],
   ['jsm','application/javascript'],
   ['jso','application/json'],
   ['json','application/json'],
   ['md','text/x-markdown'],
   ['mkv','video/x-matroska'],
   ['mobi','application/x-mobipocket-ebook'],
   ['mp3','audio/mpeg'],
   ['mp4','video/mp4'],
   ['mpeg','video/mpeg'],
   ['oga','audio/ogg'],
   ['ogg','audio/ogg'],
   ['ogv','video/ogg'],
   ['ogx','application/ogg'],
   ['otf','application/x-font-otf'],
   ['pdf','application/pdf'],
   ['png','image/png'],
   ['php','application/x-httpd-php'],
   ['snd','audio/ogg'],
   ['svg','image/svg+xml'],
   ['svgz','image/svg+xml'],
   ['swf','application/x-shockwave-flash'],
   ['ttf','application/x-font-ttf'],
   ['txt','text/plain'],
   ['uri','text/uri-list'],
   ['vmp','text/vamp'],
   ['vcf','text/x-vcard'],
   ['vcs','text/x-vcalendar'],
   ['wav','audio/x-wav'],
   ['weba','audio/webm'],
   ['webm','video/webm'],
   ['webp','image/webp'],
   ['woff','application/font-woff'],
   ['xml','application/xml'],
   ['zip','application/zip'],
];
