# anon.c0r3

*Anon system core*

***
<br>

### Description
This is the basis upon which the anon framework is built. It provides tools that aid in boot-strapping, functionality, collaboration and automation for any project.<br>
It is a sub-repository; as such it is a *feature* which can be updated upon command; as with all Anon features.
<br>

### Installation
This feature is automatically installed when you install the Anon framework on your server; however, it also provides the 2 hidden *bootstrapper* files *(`.htaccess` `.auto.php`)* -which replace the framework-installation-bootsrapper; consequently providing an automated means to update the bootstrapper -which is outside this feature itself -in the web-server's  `docroot`. This allows for updates *(like security, compatibility, speed-optimization, etc)* to the bootstrapper itself without manual intervention. Of coarse it is up to you when (and how) to update your own software -as per the Anon code of conduct: *"No feature may be updated without the ranking power-user's concent"* -hence updates require your interaction by simply clicking a button, or typing a command. Only logged-in *power-users* such as *`geeker`, `ganger`, or `leader`* with high rank can perform updates, so it is fairly secure -if you trust the remote origin of your software.
All this and more is explained in the documentation.
<br>

### Collaboration
Anon follows a simple yet effective method for allowing multiple users to collaborate; the following explains:
- each fresh Anon installation has a "clean" docroot as "system files" are hidden
- a clean docroot allows structure/design freedom for any project without having any foreign files/folders imposed
- Anon supports **Git** integration in the docroot and/or as individual/independent repos inside the docroot as sub-folders

##### Methodology
The reasons why Anon has a specific merging strategy is because it allows automatic and safe operations for:
- application updates and deployment
- worker time-sheet recording on real work done
- warning users of potential merge conflicts -before any merging is done -without any changes to the active branch

##### Repositories
- the *remote master* branch is used as current/staged final-product as source from which another party can clone/pull
- when a git-repository is to be imported, the *remote* repo's **master** branch is cloned and a **tanker** branch is created (-if not exist)
- from within the Anon GUI, it is impossible to work directly on the *master* or *tanker* branches
- the *native tanker* branch is used as `origin` for a `worktree` with several *native worker* branches -used for active work to be done
- the *remote tanker* branch is used as collection vehicle for multiple collaborating users
- any *native worker* branch `origin` is set to the *native tanker* branch of the same repo
- Anon automatically performs "micro-commits" on each user's worker-branch (per repo) as users do their work in real-time
- each *native worker* pull commits from -and push commits to the *native tanker* branch upon user-command -limited to 1 second time-gap
- the *native tanker* pull commits from -and push commits to the *remote tanker* branch upon user-command -or every 1 minute
- the *remote master* should use a **stable** branch from which to pull the result of testing *remote tanker*, every 1 hour



### Documentation
This feature's documentation ***is*** the **Anon Manual** and it's available in the `doc` folder in `markdown` format.<br>
You can view this documentation as web pages in your browser by using the "docs" button (book icon) on the w0rk-panel's side-menu.

Via browser interface, you can only view the documentation on your own Anon server if you are logged in as any `geeker`, `ganger`, or `leader`.

To log in as a power user:
- visit your website in your favorite browser
- using your keyboard, press: `Ctrl Meta x` -together, which pops up the *w0rk* panel
- in the terminal, type in: `su supra` and hit the `Enter` key on your keyboard
- it asks for a password, type in `0m1cr0n` and hit `Enter` again.

The username and password provided above are the defaults for the *Super User* in every new Anon installation; however directly after installation it is mandatory to change the `Super User` password and/or username; so if this has already been changed, replace the username and password in the instructions above with your own instead.

Once you are logged in, you have access to various w0rk features, including the Anon Manual.
Your user may be limited to certain features according to your *roles* and *rank*.
<br>
