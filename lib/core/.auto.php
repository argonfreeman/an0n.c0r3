<?php
## no namespace here


# info :: file : read this
# ---------------------------------------------------------------------------------------------------------------------------------------------
# this file is the main entry-point of any interface; it's compatible with ancient PHP (versions < 4) and used for graceful fail
# it is written in a minified way for a reason -> please do not tamper with it (much) .. if you need to tweak, do it at own risk
# here we are in "the swamp" we don't know if the PHP-version is supported -or if the request is valid or if the framework is OK
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: (tools) : for DRYKIS .. Don't Repeat Yourself Keep It Simple
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function ican($d)
   {
      $l=explode(' ',$d); $s=count($l); $e=TRUE;
      for($i=0; $i<$s; $i++){$v=$l[$i]; if((FALSE===function_exists($v))&&(FALSE===class_exists($v))){$e=FALSE; break;};}; return $e;
   };


   function envi($d)
   {
      if((FALSE===isset($_SERVER))||($d==='')){return '';}; $l=explode(' ',$d); $s=count($l); $v=$_SERVER; $f=array(); for($i=0; $i<$s; $i++)
      {$k=$l[$i]; if(!isset($v[$k])){if(isset($v["X_$k"])){$k="X_$k";}elseif(isset($v["HTTP_$k"])){$k="HTTP_$k";}else{$k="REDIRECT_$k";};};
      if(isset($_SERVER[$k])&&(strlen($_SERVER[$k])>0)){$f[$i]=$_SERVER[$k];};}; $c=count($f);
      if($c<$s){return '';}; if($c===1){return $f[0];}; return $f;
   }


   function halt($c,$m,$f=null,$l=null)
   {
      if(FALSE===function_exists('php_sapi_name')){die("FAIL :: $c : $m");}; if($f){$f=str_replace(envi('COREPATH'),'',$f);};
      if((FALSE===isset($_SERVER))||(php_sapi_name()==='cli')){die("FAIL :: $c : $m");}; $b=envi('BOTMATCH'); $i=envi('INTRFACE');
      if(preg_match(("/$b/i"),envi('USER_AGENT'))||(envi('URL')===envi('GAGROBOT'))||($i==='SYS')){$b=1;}else{$b=0;};
      $p=psee(envi('DBUGPATH')); $hm=$m; if((strpos($hm,"\n")!==false)||(strlen($hm)>64)){$hm='Internal Server Error';};
      header("HTTP/1.1 $c $hm"); if($b||!is_readable($p)){exit();}; $d=array('mesg'=>$m,'file'=>$f,'line'=>$l); $d=json_encode($d);
      $d=base64_encode($d); if($i!=='GUI'){die($d);}; $r=pget(envi('DBUGPATH')); $r=str_replace('{:DBUGDATA:}',$d,$r); echo $r; exit();
   }


   function pfix($p)
   {
      if(!is_string($p)||(trim($p)==='')){return;}; $r=getcwd(); if(strpos($p,$r)===0){return $p;}; $c=envi('COREPATH'); $h=$r;
      if($p==='/'){return $r;}; if($p==='$'){return $c;}; if($p[0]==='/'){$x=1;}elseif($p[0]==='$'){$x=2; $h=$c;}else{$x=0;}; $p=substr($p,$x);
      $p="$h/$p"; $p=str_replace('//','/',$p); return $p;
   }


   function psee($d)
   {
      if(!is_string($d)||($d==='')){return 0;}; $l=explode(' ',$d); $s=count($l); $r=getcwd(); $c=envi('COREPATH'); $f=array();
      for($i=0; $i<$s; $i++){$v=pfix($l[$i]); if($v&&is_readable($v)){$f[$i]=$v;}else{continue;};}; if(count($f)<$s){return;};
      if($s===1){return $f[0];}; return $f;
   };


   function pget($p)
   {$p=psee($p); if(!$p){return;}; if(!is_dir($p)){return file_get_contents($p);}; $r=array_diff(scandir($p),array('.','..')); return $r;};


   function pset($p,$v='')
   {
      $p=pfix($p); $d=0; if(substr($p,-1,1)==='/'){$d=1;}; $u=umask(); umask(0);
      if($d){if(is_dir($p)){$r=true;}else{$r=@mkdir($p);};}else{$r=@file_put_contents($p,$v);};
      umask($u); return $r;
   };
# ---------------------------------------------------------------------------------------------------------------------------------------------



# init :: default : check essentils and set internal defaults
# ---------------------------------------------------------------------------------------------------------------------------------------------
   chdir(envi('DOCUMENT_ROOT')); $p=envi('DBUGPATH'); if(strpos($p,'/')){$c=explode('/',$p); $c=$c[0];}else{$c='';};
   $_SERVER['COREPATH']=rtrim((getcwd()."/$c"),'/'); $_SERVER['COREFOLD']=$c;
   if(envi('URL')===('/'.envi('DBUGPATH'))){echo pget(envi('DBUGPATH')); exit();}; $t=(envi('REQUEST_TIME_FLOAT')*1);
   if(envi('COREFOLD')){$_SERVER['DBUGPATH']=('/'.str_replace(envi('COREFOLD'),'$',envi('DBUGPATH')));};
   if((FALSE===function_exists('version_compare'))||(version_compare(phpversion(),'5.6','<'))){halt(424,'Failed Dependency');};
   if(FALSE===ican('curl_init ftp_get mb_strlen gmp_strval SQLite3')){halt(424,'Failed Dependency');};
   if(''===trim(trim(strtolower((ini_get('short_open_tag').'')),'off'),'0')){halt(424,'Failed Dependency');}; date_default_timezone_set("UTC");
   if((php_sapi_name()=='cli')||(FALSE===isset($_SERVER))){halt(420,'Wrong Interface');}; ini_set('expose_php',false);
   if(1>envi('HOST BOTMATCH GAGROBOT SERVER_ADMIN SERVER_ADDR REQUEST_TIME_FLOAT URL')){halt(417,'Expectation Failed');}; $h='$/c0r3';
   if(1>envi('REQUEST_SCHEME REQUEST_METHOD USER_AGENT ACCEPT')){halt(400,'Bad Request');};$p=psee("$h/var/info/dbug.inf $h/lib/core/dbug.php");
   if($c){if(!$p){halt(424,'Failed Dependency');}; $x=(trim(pget($p[0]))*1); if(($t-$x)>3600){require($p[1]);};};
   ini_set('display_errors',true); error_reporting(E_ALL); ini_set('default_charset','UTF-8'); ini_set('input_encoding','UTF-8');
   ini_set('output_encoding','UTF-8'); set_time_limit(60); mb_internal_encoding('UTF-8'); mb_http_output('UTF-8'); unset($c,$h,$p,$x,$t);
# ---------------------------------------------------------------------------------------------------------------------------------------------



# dbug :: protocol : force https .. this cannot be done reliably in .htaccess
# ---------------------------------------------------------------------------------------------------------------------------------------------
   if((envi('USER_AGENT')==='SYS:Verify-SSL')&&(envi('REQUEST_SCHEME')==='https')){echo('OK'); exit();}; if(envi('REQUEST_SCHEME')!=='https')
   {
      $a='SYS:Verify-SSL'; if(envi('USER_AGENT')===$a){exit();}; $p=('https://'.envi('HOST')); $c=curl_init();
      curl_setopt_array($c,array(CURLOPT_RETURNTRANSFER=>1,CURLOPT_SSL_VERIFYPEER=>false,CURLOPT_URL=>$p,CURLOPT_USERAGENT=>$a));
      $r=curl_exec($c); $e='invalid config'; if(!$r){$x=curl_error($c); if($x){$e=$x;};}; curl_close($c);
      if($r!=='OK'){halt(500,"SSL :: $e");}; $p=($p.envi('QUERY_STRING')); header("Location: $p"); exit();
   };
# ---------------------------------------------------------------------------------------------------------------------------------------------



# dbug :: request : fail on invalid/not-supported request; detect-and-set client-IP-address; detect-and-set interface
# ---------------------------------------------------------------------------------------------------------------------------------------------
   $l=explode(' ','CLIENT_IP FORWARDED_FOR FORWARDED REMOTE_ADDR'); $y=0; $s=count($l); for($i=0; $i<$s; $i++)
   {$v=$l[$i]; $x="X_$v"; $z="$v"; if(envi($x)){$y=$x;}elseif(envi($z)){$y=$z;}elseif(envi($v)){$y=$v;}else{$y=0;}; if($y){break;};};
   if(!$y){halt(400,'Bad Request');}; $_SERVER['CLIENT_ADDR']=$_SERVER[$y]; unset($l,$y,$s,$i,$v,$x,$z); $x=envi('REQUEST_METHOD');
   $l=array('OPTIONS'=>'permit','GET'=>'select','PUT'=>'update','POST'=>'insert','HEAD'=>'descry','DELETE'=>'delete','CONNECT'=>'listen');
   if(isset($l[$x])){$_SERVER['CLIENT_METHOD']=$l[$x];}else{header('HTTP/1.1 405 Method Not Allowed'); exit();};
   if($x=='OPTIONS'){$l=implode(array_keys($l,', ')); header('HTTP/1.1 200 OK'); header("Allow: $l"); exit();}; unset($x,$l);

   $a=str_replace(array(' ','*','.','-','/'),'',envi('USER_AGENT')); $s=(strpos(envi('REFERER'),('https://'.envi('HOST')))===0);
   $b=envi('BOTMATCH'); $m=envi('ACCEPT'); $i=strpos($m,'/'); $p=envi('URL'); if((strlen($a)<3)||!$i){halt(400,'Bad Request');};
   $x=strpos($p,'.'); if($x){$x=explode('.',$p); $x=array_pop($x);}else{$x=null;};
   if(preg_match(("/$b/i"),$a)||(envi('URL')===envi('GAGROBOT'))){$i='BOT';}elseif(strpos($a,'SYS:',true)===0){$i='SYS';}
   elseif(envi('INTRFACE')){$i=envi('INTRFACE');}elseif(($m==='application/json')&&(!strpos($p,'.json'))){$i='API';}
   elseif($s&&$x){$i='DPI';}else{$i='GUI';}; if(($p===envi('DBUGPATH'))&&($i!=='BOT')){$i='GUI';};
   $_SERVER['INTRFACE']=$i; if($i==='SYS'){$k=explode(':',$a); $k=$k[1]; if($k!==pget('$/var/inf/host.key')){halt(401,'Unauthorized');};};
   if(($i==='BOT')&&!in_array(envi('CLIENT_METHOD'),array('descry','select'))){halt(405,'Method Not Allowed');}; unset($a,$b,$m,$i,$p,$c);
# ---------------------------------------------------------------------------------------------------------------------------------------------



# init :: system : here we are out of "the swamp" .. `aard` will load `abec` -which runs the debugger so we can get out of "the woods"
# ---------------------------------------------------------------------------------------------------------------------------------------------
   if(envi('COREFOLD')){require(psee('$/c0r3/aard.php'));}else{require(psee('/.make.php'));}; exit();
# ---------------------------------------------------------------------------------------------------------------------------------------------
