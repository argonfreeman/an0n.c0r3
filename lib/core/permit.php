<?
namespace Anon;



# tool :: permit : user authentication
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class permit
   {
      static function auth($v)
      {
         if(isScal($v)){$v=tron($v);}; expect::tron($v); if(!isWord($v->user)){return;}; $u=$v->user; $p=hashed($v->pass);
         if($v->user==='anonymous'){return;}; 

         $r=target(AUTHDATA)->select
         ([
            using => "user",
            fetch => ["name","mail","pass","rank"],
            where => ["name = '$u'","pass = '$p'"],
            limit => 1,
         ]);

         if(count($r)<1){return false;}; $r=$r[0]; $r->hash=hashed($r); unset($r->pass);
         $r->role=target("sqlite::/w0rk/user/$u/data")->select([using=>"clan",fetch=>"role",shape=>"[role]"]);

         return $r;
      }


      static function make($v=null)
      {
         $v=self::args($v);
      }


      static function mode()
      {
         $a=func_get_args(); if(isTupl($a[0])){$a=$a[0];}; if(span($a)<1){return;}; $d=SITEMODE; $l=implode($a,', or ');
         if(!locate($a,$d)){fail("this feature is not permitted for SITEMODE: $d");};
      }


      static function role()
      {
         $a=func_get_args(); if(isTupl($a[0])){$a=$a[0];}; if(span($a)<1){return;}; $d=vars('permit')->role; $l=implode($d,', or ');
         if(!locate($a,$d)){fail("this feature is not permitted for user-role: $l");};
      }


      static function user()
      {
         $a=func_get_args(); if(isTupl($a[0])){$a=$a[0];}; if(span($a)<1){return;}; $d=vars('permit')->name;
         if(!locate($a,$d)){fail("this feature is not permitted for user-name: $d");};
      }


      static function rank($q=null)
      {
         if(!$q){return;}; $d=vars('permit')->rank;
         if($d<$q){fail("this feature is not permitted for user-rank: $d");};
      }


      static function rate($q=null)
      {
         if(!$q){return;}; $d=vars('permit')->rate;
         if($d<$q){fail("this feature is not permitted for user-rate: $d");};
      }
   }

   // $export='Anon\\permit';
# ---------------------------------------------------------------------------------------------------------------------------------------------
