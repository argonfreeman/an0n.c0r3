
// mode :: strict : prevents bugs
// --------------------------------
   "use strict";
// --------------------------------



// func :: harden : define constants, immutable functions, immutable properties
// --------------------------------------------------------------------------------------------------------------------------------------------
   Object.defineProperty(window,'Main',{writable:false,enumerable:false,configurable:false,value:window});
   (Object.getOwnPropertyNames(Main)).forEach(function(g)
   {
      if(['GLOBAL','root','Main','webkitURL','webkitStorageInfo'].indexOf(g)>-1){return}; var t=(typeof Main[g])[0];
      if((t=='f')||(t=='g')||(t=='c')){Object.defineProperty(Main[g],'ROOTED',{writable:false,enumerable:false,configurable:false,value:true})}
   });
   Object.defineProperty(Main,'VOID',{writable:false,enumerable:false,configurable:false,value:(function(){}())});
// --------------------------------------------------------------------------------------------------------------------------------------------




// func :: Extend : define hardened properties -- neat shorthand to define multiple immutable properties of multiple targets
// --------------------------------------------------------------------------------------------------------------------------------------------
   Object.defineProperty(Main,'Extend',{writable:false,enumerable:false,configurable:false,value:function()
   {
      var a = [].slice.call(arguments);
      return (function(d)
      {
         if ((typeof d) != 'object'){fail('invalid dataType :: expecting: `Extend(args)({...})`'); return};

         var o = {writable:false,enumerable:false,configurable:false,value:true};  var r=true;  a.forEach((i)=>
         {
            if((['o','f'].indexOf((typeof i)[0])<0)){fail('expecting extensible identifier'); return};
            var m=(i.MAINROLE?true:false);  var t=null;  for(var p in d)
            {
               if(!d.hasOwnProperty(p)){continue;};  var v=d[p];  var c={enumerable:false,configurable:false};
               if(v&&(v.name=='Trap')&&(p!='Trap'))
               {
                  if(!!v.get){c.get=v.get}; if(!!v.set){c.set=v.set}; if(!!v.try){fail('`try` is not implemented, yet');return};
               }
               else
               {
                  t=(typeof v)[0];if(v&&m&&((t=='f')||(t=='o'))){try{Object.defineProperty(v,'ROOTED',o)}catch(e){fail(e)}};c.writable=false;
                  c.value=v; if(t=='f'){Object.defineProperty(v,'name',{writable:false,enumerable:false,configurable:false,value:p})};
               };

               try{Object.defineProperty(i,p,c)}catch(e){r=false;};
            };
            return r;
         });
      });
   }});
// --------------------------------------------------------------------------------------------------------------------------------------------




// shim :: standardize : names
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)({name:'Main'}); Extend(Math)({name:'Math'});  Extend(console)({name:'console'});
// --------------------------------------------------------------------------------------------------------------------------------------------




// func :: Define : globals only -- extends `Main`
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      Define:function(defn,arg1,arg2)
      {
         var type,list,span,echo;  type=(typeof defn);

         if ((type == 'string'))
         {
            list = defn.split(',');  defn={};  type='object';

            if ((list.length < 2) && arg1)
            { defn[list[0]] = arg1;}
            else
            {
               list.forEach(function(word)
               {
                  span = word.length;
                  if ((span > 0) && (span < 13) && (word === word.toUpperCase()))
                  { echo=(':'+word+':'); defn[word]=echo; return; }
                  fail('expecting :WORD:');
               });
            }
         }

         if (type == 'object') {Extend(Main)(defn); return echo;}
         fail('expecting :NODE:');
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// conf :: (global) : important globals - these are used with some tools defined below
// --------------------------------------------------------------------------------------------------------------------------------------------
   Define
   ({
      NONE:null,
      TRUE:true,
      FALS:false,
   });


   Define
   (
      'AUTO,INFO,DROP,DUPL,HASH,UPPR,LOWR,CAML,PROP,NEXT,SKIP,STOP,DONE,KEYS,VALS,ONCE,BFOR,AFTR,EVRY,UNTL,EVNT,EVEN,ODDS,PULL,PUSH,FAIL,'+
      'MAKE,EXIT,XACT,DUPE,PATH,NODE,TAGS,SELF,GOOD,NEED,WARN,TL,TM,TR,RT,RM,RB,BR,BM,BL,LB,LM,LT,CURSOR'
   );


   Define
   ({
      RunLib:
      {
         TEXT:
         {
            emogi:":) ;) :P :D :| :/ :O :( ;( :'( :-) ;-) :-P :-D :-| :-/ :-O :-( ;-( 8) \\O O/ \\O/ :*".split(' '),
         },

         EVNT:{},
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// func :: typeOf : primary data-types - `r` for crude type  ..  `TOOL` is i.e: Math, RegExp, console  ..  `PROC` is like a: worker, timer, XHR
// --------------------------------------------------------------------------------------------------------------------------------------------
   Define('typeOf',function(d,r,n,t,o)
   {
      if(d&&d.MAINROLE){return this[9];};  if((d===null)||d===VOID){return this[0]};
      n=(d.name||((!!d.constructor)?d.constructor.name:VOID));  if(n&&((n=='RegExp')||d.ROOTED)){return this[7]};
      if((n&&(n.substr(1,3)=='ime'))||((typeof d.send)[0]=='f')||(d.platform)){return this[6]};
      t=(Object.prototype.toString.call(d).match(/\s([a-zA-Z]+)/)[1].toLowerCase());if(r){return t};t=(t[0]+t[1]);o='ob da ht er'.split(' ');
      return this[((t=='bo')?1:((t=='nu')?2:((t=='st')?3:((t=='fu')?4:(((t=='ar')||(t=='no'))?5:((o.indexOf(t)>-1)?8:0))))))];
   }
   .bind(function(o,f)
   {
      o={};  ('VOID BOOL NUMR TEXT FUNC LIST PROC TOOL TRON MAIN'.split(' ')).forEach(function(t,i)
      {
         f=('is'+t[0]+t.substr(1).toLowerCase());  o[i]=Define(t);
         Define(f,function(v){return (typeOf(v)==this.t)}.bind({t:o[i]}));
      });

      return o;
   }()));
// --------------------------------------------------------------------------------------------------------------------------------------------




// func :: kindOf : secondary dataType identification  ..  `TOSHTEXT` is all-white-space
// --------------------------------------------------------------------------------------------------------------------------------------------
   Define('kindOf',function(d,r,t)
   {
      r=typeOf(d,1);  t=typeOf(d).substr(1,4);
      return (this[t](d,r));
   }
   .bind(function(d,t,g,f)
   {
      d={};
      ([
         ['VOID','NONE',function(d,r){return this[0]}],
         ['BOOL','TRUE FALS',function(d,r){return this[(d?0:1)]}],
         ['NUMR','VOID DGIT FRAC',function(d,r){return this[(!d?0:(((d+'').indexOf('.')<0)?1:2))]}],

         ['TEXT','VOID TOSH NUMR CHAR BOOL WORD SLUG WRAP EMOG PATH MAIL PASS EXPR FUNC MESG BLOG SOME',function(d,r,e)
         {
            if(!d){return this[0]};  if(!d.trim()){return this[1]};  if(!isNaN((d))){return this[2]};  if(d.length<2){return this[3]};
            if('true fals false yes no on off'.split(' ').indexOf(d.toLowerCase())>-1){return this[4]};
            if((/^[a-zA-Z0-9_]{2,36}$/).test(d)){return this[5]};  if((/^[a-zA-Z0-9_-]{3,256}$/).test(d)){return this[6]};
            if(['""',"''",'``','()','[]','{}','<>','::'].indexOf((d[0]+d.slice(-1)))>-1){return this[7]};
            if((d.length<4) && (d.length>1) && (RunLib.TEXT.emogi.indexOf(d.toUpperCase())>-1)){return this[8]}
            if(d.indexOf('/')>-1){if(d.indexOf('://')>-1){d=d.split('://')[1]}; d=d.split('?')[0]; if(d.match(/^[a-zA-Z0-9-\/\$\._]{1,256}$/)){return this[9]}};
            // if((d.indexOf('://')>0)||(d.indexOf('?')>0)){var p=d.indexOf('://');};
            if(d.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/)){return this[10]};
            if(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{6,12}/.test(d)){return this[11]};
            if(d.match(/^\/[\s\S]+\/$/)){return this[12]};
            if(d.trim().split('\n').join('').split(' ').join('').split('function(').length>1){return this[13]};
            if((d.length>5)&&((d.indexOf('\n')>-1)||(d.indexOf(' ')>-1))&&(/^(?=.*[a-z])/.test(d))){ return this[((d.length<=60)?14:15)]};
            return this[16];
         }],

         ['FUNC','VOID TOOL BOND DATA SOME',function(d,r)
         {
            d=d.toString().trim().split('\n').join('').split(' ').join('').stub('){',')=>{')[2].split('}')[0].trim();
            if(d.length<1){return this[0]};
            if(d.indexOf('[nativecode]')>-1){return this[1]};  return this[2];
         }],

         ['LIST','VOID ARGS NODE VECT TEXT DATA FUNC DEEP SOME',function(d,r)
         {
            if(d.length<1){return this[0]}; if(r.indexOf('coll')>-1){return this[1]}; // TODO <-- here: arguments nodelist
            var ve=1,te=1,da=1,fu=1,de=0,pk=VOID,pv;  ([].slice.call(d)).forEach(function(cv,ck)
            {
               if(!isNumr(ck)||!isNumr(cv)){ve=0};  if(!isText(cv)){te=0};  if(!isTron(cv)){da=0};  if(!isFunc(cv)){fu=0};  if(!ve){return};
               if(pk===VOID){pk=ck;pv=cv;return};  if(((pk+1)!=ck)||(cv<=pv)){ve=0;return};  pk=ck; pv=cv;
            });
            return this[(ve?3:(te?4:(da?5:(fu?6:8))))];
         }],

         ['TRON','VOID HTML EVNT FAIL DATA TOOL DEEP SOME',function(d,r)
         {
            if(r.indexOf('elem')>-1){return this[1]}; if((!!d.constructor)&&(d.constructor.ROOTED)&&(d.name)){return this[5]};
            if(Object.getOwnPropertyNames(d).length<1){return this[0]};
            if(r.indexOf('even')>-1){return this[2]};
            if(r.indexOf('erro')>-1){return this[3]};  var da=1,to=1,de=0;  for(var i in d){if(!d.hasOwnProperty(i)){continue};
            if(!(/^[a-zA-Z0-9_]{2,36}$/).test(i)||(!isVoid(d[i])&&!isBool(d[i])&&!isNumr(d[i])&&!isText(d[i]))){da=0};
            if(!isFunc(d[i])){to=0};  if(isList(d[i])||isTron(d[i])){de=1};}
            return this[(da?4:(to?5:(de?6:7)))];
         }],

         ['TOOL','BOOL NUMR TEXT LIST FUNC NODE MATH EXPR TIME DEVL SOME',function(d,r,n,i)
         {
            n=(d.name||((!!d.constructor)?(d.constructor.name):((!!d.prototype)?d.prototype.name:'')));  if(!n){return this[10]};
            i=(['Boolean','Number','String','Array','Function','Object','Math','RegExp','Date']).indexOf(n);  if(i>(0-1)){return this[i]};
            if((n=='dump')||(n=='console')){return this[9]};  return this[10];
         }],

         ['PROC','MAIN MULE CRON BIOS SOME',function(d,r,n)
         {
            if(d.name=='process'){return this[(ISMAIN?0:1)]};
            n=(d.name||((!!d.constructor)?(d.constructor.name):((!!d.prototype)?d.prototype.name:'')));
            if(n.substr(1,3)=='ime'){return this[2]}; if((typeof d.send)[0]=='f'){return this[3];};  return this[4];
         }],

         ['MAIN','ROOT',function(d,r)
         {
            return this[0];
         }]
      ])
      .forEach(function(l)
      {
         var o,t,f; o={}; t=l[0];  l[1].split(' ').forEach(function(k,i)
         {
            f=('is'+k[0]+k.substr(1).toLowerCase()+t[0]+t.substr(1).toLowerCase()); k=(k+t); o[i]=Define(k);
            Define(f,function(v){return (kindOf(v)==this.k)}.bind({k:o[i]}));
         });
         d[t]=l[2].bind(o);
      });

      return d;
   }()));
// --------------------------------------------------------------------------------------------------------------------------------------------



// shiv :: tools : essentials
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      harden:function(t,o,l)
      {
         o=(o||Main); if(!o.hasOwnProperty){fail('invalid parent'); return;}; var k=(isText(t)?t:t.name);
         if(!k||!o.hasOwnProperty(k)){fail('invalid attribute `'+k+'`'); return;};
         Object.defineProperty(o,k,{writable:false,enumerable:false,configurable:false,value:o[k]});
      },

      isNode:function(v){return (v instanceof Element);},
      isPath:function(v){if(!isText(v)){return}; v=v.split('?')[0]; return ((v.indexOf('/')>-1)&&(!!v.match(/^[a-zA-Z0-9-\/\$\._]{1,432}$/)));},

      isWrap:function(v){return isText(v,2)&&-1<"\"\" '' `` {} [] () <> // ::".split(" ").indexOf(v.substr(0,1)+v.substr(v.length-1,1))?!0:!1},
      wrapOf:function(a){return isWrap(a)?a.substr(0,1)+a.substr(a.length-1,1):""},
      unwrap:function(v){return isWrap(v)?v.substr(1,v.length-2):v},
      parsed:function(a)
      {
         if(!isText(a)){return}; a=a.trim(); var b=a.toLowerCase(); if((1>b.length)||("null"===b)||("undefined"===b)){return null};
         if("true"===b){return!0}; if("false"===b){return!1}; var r=undefined;
         if((a[0]=='+')&&!isNaN(a.substr(1))){a=a.substr(1)}; if(!isNaN(a)){return (a*1)};
         if(["{}","[]"].indexOf(wrapOf(a))>-1){try{b=JSON.parse(a)}catch(c){return null}return b};
         if(['""',"''","``"].indexOf(wrapOf(a))>-1){return unwrap(a)}; if(wrapOf(a)==='<>')
         {
            var p=(new DOMParser()); var x=p.parseFromString(a,"text/html"); if(x.documentElement.nodeName=="parsererror"){return;};
            r=[]; let l=([].slice.call(x.documentElement.childNodes)); if(l.length<1){l=[x.documentElement];};
            l.forEach((n,i)=>{if(n.parentNode.nodeName!='#document'){n=n.parentNode;}; r[i]=document.createElement(n.nodeName);
            let a=([].slice.call(n.attributes)); a.forEach((q)=>{r[i].setAttribute(q.name,q.value)}); r[i].innerHTML=n.innerHTML});
            return r;
         };
         if((a.indexOf("\n")<0)&&(a.indexOf(":")<1)){return a;};
         a=a.split("\r\n").join("\n").split("\n");  r=null; a.forEach((l)=>{let p=l.stub(':'); if(!r){r=(p?{}:[]);};
         if(!p){r[r.length]=parsed(l);return;}; let k=p[0].trim().toCamelCase();
         let v=(((p[2].indexOf(':')>-1)&&(p[2].indexOf('{')<0))?p[2].trim():parsed(p[2])); r[k]=v;}); return r;
      },


      expect:function(v,t,r)
      {
         slog(MAKE);
         if(!t||(t==='*')){return true}; if(isText(t)&&(t.indexOf('|')>0)){t=t.split('|')}; var f,s;
         if(isText(t,2,36))
         {
            if((t[0]+t.substr(-1))=='::'){t=t.substr(1,(t.length-2))}; t=t.toUpperCase(); f=(t[0]+t.substr(1).toLowerCase());
            if(f.length==8){f=(f.substr(0,4)+f.substr(4,1).toUpperCase()+f.substr(5))}; f=('is'+f); if(!Main[f]){return};
            if(!Main[f](v)){if(r){return}; throw new Error('expecting :'+t+':'); return}; return true;
            // if(!Main[f](v)){let m=(isText(r)?(' '+r+' as'):''); throw new Error('expecting'+m+' :'+t+':'); return}; return true;
         };
         if(isList(t)){Each(t,function(x){f=expect(v,x,1);if(f){return 1}});if(!f){throw new Error('expecting '+t.join(', or '));};return;};
         if(isTron(t)){if(!isTron(v)){throw new Error('expecting :TRON:'); return}; Each(t,function(x,k){expect(v[k],x);});};
      },


      entity:function(o,r)
      {r=(new EventTarget()); for(var k in o){r[k]=o[k];}; return r;},


      cStack:function(e,skp)
      {
         var h,s,r,f,p,l,x; h=(location.protocol+'//'+location.hostname+''); if(!isTron(e)||!e.stack){e=(new Error('...'))} s=(e.stack+'');
         if(!e.stack||(s.indexOf('\n')<0)||(s.indexOf(' at ')<0)){return []}; s=s.split('\n'); s.shift(); r=[]; x=0; s.forEach(function(i)
         {
            i=i.split(' at ')[1].split('(').join('').split(')').join('').split(' ');if(i.length<2){i.unshift('anonymous')};if(i.length>2){return};
            f=i[0]; i=i[1].split(h).join('').split(':'); p=(i[0]+'').trim(); if(p.substr(0,2)=='/$'){p=p.substr(1)}; l=(i[1]*1);
            if(((x<1)&&(f=='cStack'))||((x<2)&&(f=='slog'))){return;}; skp=false; if(isList(cStack.skip)){Each(cStack.skip,(o)=>
            {
               if(skp){return STOP}; let sf,sp,sl; sf=(o.func&&(o.func===f)); sp=(o.path&&(o.path===p)); sl=(o.line&&(o.line===l));
               if((sf&&sp&&sl)||(sf&&sp)||(sp&&sl)){skp=true};
            })}

            if(!skp&&f&&isPath(p)&&l){r.push({func:f,path:p,line:l});};
         });
         return r;
      },

      slog:function(o,n,s)
      {
         if(!cStack.log){cStack.log=[]}; let l=((o==PULL)?cStack.log:cStack()); o=(o||MAKE); n=(n||l.length); s=(s||{}); let r,x; r=[]; x=0;
         Each(l,(i)=>
         {
            let sf,sp,sl; sf=(s.func&&(s.func==i.func)); sp=(s.path&&(s.path==i.path)); sl=(s.line&&(s.line==i.line));
            if((sf&&sp&&sl)||(sf&&sp)||(sp&&sl)||sp||sf){return SKIP};
            r[r.length]=i; x++; if(x>n){return STOP};
         });
         if(o==MAKE){cStack.log=r; return true};
         if(o==PUSH){r.reverse(); r.forEach((i)=>{cStack.log.unshift(i)}); return true};
         return r;
      },

      cStyle:function(n,p)
      {
         if(!n||!n.style||!isText(p)){fail('expecting args as (:node:,:text:)');return};
         let s=getComputedStyle(n); let v=s.getPropertyValue(p); if(v&&!isNaN(v.rtrim('px'))){v=(v.rtrim('px')*1)}; return v;
      },

      Each:function(d,f)
      {
         var t=typeOf(d);  if(t==NUMR){d=list(d); t=LIST};

         for (var k in d)
         {
            if(((k+'').length<1)||!d.hasOwnProperty(k)){continue}; if((t==LIST)&&!isNaN(k)){k=(k*1)};
            var z = f.apply(d,[d[k],k]);  if((z!==VOID)&&(z!==NEXT)&&(z!==SKIP)){break};
         };
      },

      halt:function(m,f,h,p,n)
      {
         window.HALT=1; f=':: EPIC FAIL ::\n\nThis site is being violated.\nPlease come back later.'; h='stop breaking sh!t';
         if(!('cookie' in window)||!('atob' in window)){alert(f); console.error(h); return};
         p=cookie.select('dbugPath'); if(!p||((p+'').length<1)){alert(f); console.error(h); return};
         n=document.createElement('iframe'); n.id="failPane"; n.className='view dbugPanl'; n.onerror=function(){alert(f); console.error(h)};
         if(p[0]!='/'){p=('/'+p)}; if(((typeof m)=='string')&&(m.length==5)&&(m[0]=='#')){p=(p+'?'+m)};
         n.onload=function(){setTimeout(function(){n.onload=VOID; window.HALT=false; if(window.Busy){Busy.fail=false;}},500)};
         n.src=p; document.body.appendChild(n); var c=document.createElement('div'); c.id='failExit'; c.innerHTML='x'; c.onclick=function()
         {let o=document.getElementById('failPane'); o.parentNode.removeChild(o); this.parentNode.removeChild(this);};
         document.body.appendChild(c);
      },


      dump:function()
      {
         slog(PUSH);Main.dispatchEvent((new Event('dump'))); let args=([].slice.call(arguments)); console.log.apply(console,args);
         if(!!window.repl&&!!repl.say){args.forEach((i)=>
         {
            if(isMain(i)){repl.say(':MAIN:'); return};
            if(isTool(i)){repl.say(':'+i.name+' TOOL:'); return};
            if(isTron(i)||isList(i)){repl.say(JSON.stringify(i)); return};
            if(isFunc(i)){repl.say(i.toString()); return};
            repl.say(i);
         })};
      },


      fail:function(m,t, o)
      {
         slog(PUSH); t=(t||'usage'); Main.dispatchEvent((new Event('fail'))); o=(new Error((m+''))); o.name=t;
         throw o;
      },


      list:function(v,o)
      {
         if(isNumr(v))
         {
            if((v+'').indexOf('.')>0){return [v]}; // TODO :: float vector
            if(v===0){return []}; if(!o||!isNumr(o)){o=d; d=1;}; if(d===o){return [d];};  var r = [];
            if(d<o){for(d; d<=o; d++){r.push(d);}}else{for(o; o<=d; d--){r.push(d);}};  return r;
         };
         if(isText(v))
         {
            if(v.length<1){return []}; if(!isText(o)&&!isNumr(o)){return [v]};
            if((o===0)||(o==='')){return v.split('')}; if(isText(o)){return v.split(o)};
            var x=(new RegExp(('.{1,'+o+'}'),'g')); return v.match(x);
         };
         if(isList(v)){return ([].slice.call(v));};
      },


      span:function(d,x)
      {
         if(!d&&isNaN(d)){return 0};  if(!isNaN(d)){d=(d+'')};
         if(x&&((typeof x)=='string')&&((typeof d)=='string')){d=(d.split(x).length-1); return d};
         var s = d.length;  if(!isNaN(s)){return s;}; try{s=Object.getOwnPropertyNames(d).length; return s;}catch(e){return 0;}
      },

      keys:function(n,o,x, h,r,k)
      {
         if(' und nul boo num str'.indexOf(((typeof n).substr(0,3)))>0){return []}; if(o==VOID){o=SELF}; if(o==VOID){o=SELF};
         h=[]; if(o==SELF){h=Object.getOwnPropertyNames(n)}else{for(k in n){h.push(k)}}; if(x==VOID){return h};
         if(isNumr(x)){if(x<1){x=h.length-1}; return h[x]}; if(!isText(x)||(x.length<2)||((x[0]!='*')&&((x.frag(-1,1)!='*')))){return []};
         r=[]; let b,e,s; if(x[0]=='*'){e=1}else{b=1}; x=x.Trim('*'); s=x.length; if(s<1){return h};
         h.forEach((i)=>{if(b&&(i.substr(0,s)==x)){r.push(i)}else if(e&&i.frag((0-s),s)==x){r.push(i)}}); return r;
      },

      vals:function(d)
      {
         var r = [];  keys(d).forEach(function(k){r.push(d[k])});  return r;
      },

      vect:function(d,c)
      {
         var r=[]; d=(d*1); if(isNaN(d)||((d+'').indexOf('.')>-1)||(d<1)){fail('expecting a whole positive number');return};
         if(!c){c=2}; c=(c*1); if(isNaN(c)||((c+'').indexOf('.')>-1)||(c<1)){fail('expecting a whole positive number');return};
         c-=1; if(c<1){c=1}; var x=(d/c); for(var i=0; i<d; i+=x){r.push(i)}; r.push(d); return r;
      },

      vectLine:function(a,w,h)
      {
         let wr,hr,x1,y1,x2,y2; wr=(w/90); hr=(h/90); if((a==90)||(a==270)){x1=(w/2)}; if((a==0)||(a==180)){y1=(h/2)};
         if(!x1){if((a<=45)||(a>=315)){x1=0}else if((a<=225)&&(a>=135)){x1=w}else{x1=((((a>45)&&(a<135))?(a-45):(90-(a-225)))*wr)}};
         if(!y1){if((a>=45)&&(a<=135)){y1=0}else if((a>=225)&&(a<=315)){y1=h}else{y1=(((a>315)?(360-a+45):((a<45)?(45-a):(a-135)))*hr)}};
         x1=Math.round(x1); y1=Math.round(y1); x2=(w-x1); y2=(h-y1); return {x1:x1,y1:y1,x2:x2,y2:y2};
      },

      purl:function(p,d,f, q)
      {
         if(window.HALT){return}; slog(PUSH);
         q={}; if(isTron(p)){q=p;}else if(isPath(p)){q.target=p}; if(isFunc(d)){f=d; d=undefined}; if(d!==undefined){q.convey=d};
         expect(q,'tron'); if(isFunc(f)){q.listen={loadend:f}}; if(!q.method){q.method='GET'}; d=q.convey; if(!q.header){q.header={}};
         if((d!==undefined)&&!isText(d)){if(q.method=='GET'){q.target+='?'+encode.URL(d);q.convey=null}else{q.convey=encode.JSON(d)}};
         expect(q,{target:'text', method:'text', listen:'tron', expect:'void|text', header:'void|tron', convey:'void|text'}); var l;
         if(q.target.indexOf('://')<0){l=location; if(q.target[0]!='/'){q.target=('/'+q.target);};q.target=(l.protocol+'//'+l.host+q.target);};
         var x=(new XMLHttpRequest()); if(q.silent){x.silent=1}; x.open(q.method,q.target); if(!!q.expect){x.responseType=q.expect};
         q.header['GUIREADY']='true'; Each(q.listen,function(v,k){expect(v,'func'); let w=v.toString().trim().stub('(')[0].split(' ')[0]; if(w!='function')
         {Busy.tint('red'); Busy.kill(); fail('expecting `function(){}` as callback .. NOT `()=>{}`'); return STOP;};x.addEventListener(k,v)});
         Object.defineProperty(x,'echo',{get:function(){let rsp={head:parsed(this.getAllResponseHeaders()),body:this.response}; rsp.head.path=this.purl; return rsp}});
         if(!q.header){q.header={}}; if(!q.header.INTRFACE){q.header.INTRFACE='DPI'};
         if(!!q.header){Each(q.header,function(v,k){expect(v,'text'); x.setRequestHeader(k,v)});}; x.send(q.convey);
      },

      durl:function(p,f)
      {
         purl({target:p,expect:'blob'},function()
         {
            let b=(new Blob([this.response],{type:this.echo.head.contentType})); decode.BLOB(b,function(r){f(r)});
         });
      },

      dupe:function(v, r)
      {
         if(!v||isNumr(v)||isText(v)){return v}; if(isList(v)){r=[]; v.Each((i)=>{r.push(dupe(i))});return r};
         if((v instanceof Element)){return (v.cloneNode(true))};
         if(isTron(v)||isFunc(v)){r={}; v.Each((v,k)=>{r[k]=dupe(v)}); return (isFunc(v)?v.bind(r):r)};
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------



// tool :: cStack : ignore
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(cStack)
   ({
      ignore:function(l)
      {
         if(!isList(cStack.skip)){cStack.skip=[]}; if(!isList(l)){l=[l]};
         l.forEach((i)=>{if(!i||(!i.func&&!i.path)){return}; cStack.skip.push(i)});
      },
   });
   cStack.ignore
   ([
      {func:"MutationObserver.",path:"/c0r3/htm/boot.htm"},
      {func:"anonymous",path:"/c0r3/htm/boot.htm"},
      {func:"EventTarget.listen",path:"/c0r3/lib/core/abec.js"},
   ]);
// --------------------------------------------------------------------------------------------------------------------------------------------



// shiv :: Each : (prototype)
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Number.prototype,String.prototype,Array.prototype,Object.prototype)({Each:function(f){return Each(this,f);}});
// --------------------------------------------------------------------------------------------------------------------------------------------



// shiv :: concat : (prototype)
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Object.prototype)
   ({
      concat:function(o,x, self)
      {
         self=this; x=(x||DUPE); if(!isTron(o)&&!isList(o)){return self};
         o.Each((v,k)=>{self[k]=((x==DUPE)?dupe(v):v)}); return self;
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------



// shiv :: tools : data-kind
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      isOnlyVoid:function(v){return (v===VOID)}, isNoneVoid:function(v){return (v===NONE)},
      isTrueBool:function(v){return (v===TRUE)}, isFalsBool:function(v){return (v===FALS)},
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// shiv :: Cookies : https://github.com/js-cookie/js-cookie
// --------------------------------------------------------------------------------------------------------------------------------------------
   !function(e){var n=!1;if("function"==typeof define&&define.amd&&(define(e),n=!0),"object"==typeof exports&&(module.exports=e(),n=!0),!n){var o=window.Cookies,t=window.Cookies=e();t.noConflict=function(){return window.Cookies=o,t}}}(function(){function g(){for(var e=0,n={};e<arguments.length;e++){var o=arguments[e];for(var t in o)n[t]=o[t]}return n}return function e(l){function C(e,n,o){var t;if("undefined"!=typeof document){if(1<arguments.length){if("number"==typeof(o=g({path:"/"},C.defaults,o)).expires){var r=new Date;r.setMilliseconds(r.getMilliseconds()+864e5*o.expires),o.expires=r}o.expires=o.expires?o.expires.toUTCString():"";try{t=JSON.stringify(n),/^[\{\[]/.test(t)&&(n=t)}catch(e){}n=l.write?l.write(n,e):encodeURIComponent(String(n)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g,decodeURIComponent),e=(e=(e=encodeURIComponent(String(e))).replace(/%(23|24|26|2B|5E|60|7C)/g,decodeURIComponent)).replace(/[\(\)]/g,escape);var i="";for(var c in o)o[c]&&(i+="; "+c,!0!==o[c]&&(i+="="+o[c]));return document.cookie=e+"="+n+i}e||(t={});for(var a=document.cookie?document.cookie.split("; "):[],s=/(%[0-9A-Z]{2})+/g,f=0;f<a.length;f++){var p=a[f].split("="),d=p.slice(1).join("=");this.json||'"'!==d.charAt(0)||(d=d.slice(1,-1));try{var u=p[0].replace(s,decodeURIComponent);if(d=l.read?l.read(d,u):l(d,u)||d.replace(s,decodeURIComponent),this.json)try{d=JSON.parse(d)}catch(e){}if(e===u){t=d;break}e||(t[u]=d)}catch(e){}}return t}}return(C.set=C).get=function(e){return C.call(C,e)},C.getJSON=function(){return C.apply({json:!0},[].slice.call(arguments))},C.defaults={},C.remove=function(e,n){C(e,"",g(n,{expires:-1}))},C.withConverter=e,C}(function(){})});
   Cookies.defaults.path='/'; harden('Cookies');

   Extend(Main)
   ({
      cookie:
      {
         exists:function(b,v){v=Cookies.get(b); return isVoid(v);},
         create:function(b,a,c,d){Cookies.set(b,btoa(JSON.stringify(a)),{expires:c||null,path:d||"/"}); return true;},
         select:function(b,v){v=Cookies.get(b); if(!isVoid(v)){try{v=JSON.parse(atob(v))}catch(e){v=null}; return v}},
         delete:function(b,a){return Cookies.remove(b,{path:a||"/"})},
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------



// shiv :: md5 : hash
// --------------------------------------------------------------------------------------------------------------------------------------------
   !function(n){"use strict";function t(n,t){var r=(65535&n)+(65535&t);return(n>>16)+(t>>16)+(r>>16)<<16|65535&r}function r(n,t){return n<<t|n>>>32-t}function e(n,e,o,u,c,f){return t(r(t(t(e,n),t(u,f)),c),o)}function o(n,t,r,o,u,c,f){return e(t&r|~t&o,n,t,u,c,f)}function u(n,t,r,o,u,c,f){return e(t&o|r&~o,n,t,u,c,f)}function c(n,t,r,o,u,c,f){return e(t^r^o,n,t,u,c,f)}function f(n,t,r,o,u,c,f){return e(r^(t|~o),n,t,u,c,f)}function i(n,r){n[r>>5]|=128<<r%32,n[14+(r+64>>>9<<4)]=r;var e,i,a,d,h,l=1732584193,g=-271733879,v=-1732584194,m=271733878;for(e=0;e<n.length;e+=16)i=l,a=g,d=v,h=m,g=f(g=f(g=f(g=f(g=c(g=c(g=c(g=c(g=u(g=u(g=u(g=u(g=o(g=o(g=o(g=o(g,v=o(v,m=o(m,l=o(l,g,v,m,n[e],7,-680876936),g,v,n[e+1],12,-389564586),l,g,n[e+2],17,606105819),m,l,n[e+3],22,-1044525330),v=o(v,m=o(m,l=o(l,g,v,m,n[e+4],7,-176418897),g,v,n[e+5],12,1200080426),l,g,n[e+6],17,-1473231341),m,l,n[e+7],22,-45705983),v=o(v,m=o(m,l=o(l,g,v,m,n[e+8],7,1770035416),g,v,n[e+9],12,-1958414417),l,g,n[e+10],17,-42063),m,l,n[e+11],22,-1990404162),v=o(v,m=o(m,l=o(l,g,v,m,n[e+12],7,1804603682),g,v,n[e+13],12,-40341101),l,g,n[e+14],17,-1502002290),m,l,n[e+15],22,1236535329),v=u(v,m=u(m,l=u(l,g,v,m,n[e+1],5,-165796510),g,v,n[e+6],9,-1069501632),l,g,n[e+11],14,643717713),m,l,n[e],20,-373897302),v=u(v,m=u(m,l=u(l,g,v,m,n[e+5],5,-701558691),g,v,n[e+10],9,38016083),l,g,n[e+15],14,-660478335),m,l,n[e+4],20,-405537848),v=u(v,m=u(m,l=u(l,g,v,m,n[e+9],5,568446438),g,v,n[e+14],9,-1019803690),l,g,n[e+3],14,-187363961),m,l,n[e+8],20,1163531501),v=u(v,m=u(m,l=u(l,g,v,m,n[e+13],5,-1444681467),g,v,n[e+2],9,-51403784),l,g,n[e+7],14,1735328473),m,l,n[e+12],20,-1926607734),v=c(v,m=c(m,l=c(l,g,v,m,n[e+5],4,-378558),g,v,n[e+8],11,-2022574463),l,g,n[e+11],16,1839030562),m,l,n[e+14],23,-35309556),v=c(v,m=c(m,l=c(l,g,v,m,n[e+1],4,-1530992060),g,v,n[e+4],11,1272893353),l,g,n[e+7],16,-155497632),m,l,n[e+10],23,-1094730640),v=c(v,m=c(m,l=c(l,g,v,m,n[e+13],4,681279174),g,v,n[e],11,-358537222),l,g,n[e+3],16,-722521979),m,l,n[e+6],23,76029189),v=c(v,m=c(m,l=c(l,g,v,m,n[e+9],4,-640364487),g,v,n[e+12],11,-421815835),l,g,n[e+15],16,530742520),m,l,n[e+2],23,-995338651),v=f(v,m=f(m,l=f(l,g,v,m,n[e],6,-198630844),g,v,n[e+7],10,1126891415),l,g,n[e+14],15,-1416354905),m,l,n[e+5],21,-57434055),v=f(v,m=f(m,l=f(l,g,v,m,n[e+12],6,1700485571),g,v,n[e+3],10,-1894986606),l,g,n[e+10],15,-1051523),m,l,n[e+1],21,-2054922799),v=f(v,m=f(m,l=f(l,g,v,m,n[e+8],6,1873313359),g,v,n[e+15],10,-30611744),l,g,n[e+6],15,-1560198380),m,l,n[e+13],21,1309151649),v=f(v,m=f(m,l=f(l,g,v,m,n[e+4],6,-145523070),g,v,n[e+11],10,-1120210379),l,g,n[e+2],15,718787259),m,l,n[e+9],21,-343485551),l=t(l,i),g=t(g,a),v=t(v,d),m=t(m,h);return[l,g,v,m]}function a(n){var t,r="",e=32*n.length;for(t=0;t<e;t+=8)r+=String.fromCharCode(n[t>>5]>>>t%32&255);return r}function d(n){var t,r=[];for(r[(n.length>>2)-1]=void 0,t=0;t<r.length;t+=1)r[t]=0;var e=8*n.length;for(t=0;t<e;t+=8)r[t>>5]|=(255&n.charCodeAt(t/8))<<t%32;return r}function h(n){return a(i(d(n),8*n.length))}function l(n,t){var r,e,o=d(n),u=[],c=[];for(u[15]=c[15]=void 0,o.length>16&&(o=i(o,8*n.length)),r=0;r<16;r+=1)u[r]=909522486^o[r],c[r]=1549556828^o[r];return e=i(u.concat(d(t)),512+8*t.length),a(i(c.concat(e),640))}function g(n){var t,r,e="";for(r=0;r<n.length;r+=1)t=n.charCodeAt(r),e+="0123456789abcdef".charAt(t>>>4&15)+"0123456789abcdef".charAt(15&t);return e}function v(n){return unescape(encodeURIComponent(n))}function m(n){return h(v(n))}function p(n){return g(m(n))}function s(n,t){return l(v(n),v(t))}function C(n,t){return g(s(n,t))}function A(n,t,r){return t?r?s(t,n):C(t,n):r?m(n):p(n)}"function"==typeof define&&define.amd?define(function(){return A}):"object"==typeof module&&module.exports?module.exports=A:n.md5=A}(this);
// --------------------------------------------------------------------------------------------------------------------------------------------



// shiv :: sha1 : hash
// --------------------------------------------------------------------------------------------------------------------------------------------
   !function(){"use strict";function t(t){t?(f[0]=f[16]=f[1]=f[2]=f[3]=f[4]=f[5]=f[6]=f[7]=f[8]=f[9]=f[10]=f[11]=f[12]=f[13]=f[14]=f[15]=0,this.blocks=f):this.blocks=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],this.h0=1732584193,this.h1=4023233417,this.h2=2562383102,this.h3=271733878,this.h4=3285377520,this.block=this.start=this.bytes=this.hBytes=0,this.finalized=this.hashed=!1,this.first=!0}var h="object"==typeof window?window:{},s=!h.JS_SHA1_NO_NODE_JS&&"object"==typeof process&&process.versions&&process.versions.node;s&&(h=global);var i=!h.JS_SHA1_NO_COMMON_JS&&"object"==typeof module&&module.exports,e="function"==typeof define&&define.amd,r="0123456789abcdef".split(""),o=[-2147483648,8388608,32768,128],n=[24,16,8,0],a=["hex","array","digest","arrayBuffer"],f=[],u=function(h){return function(s){return new t(!0).update(s)[h]()}},c=function(){var h=u("hex");s&&(h=p(h)),h.create=function(){return new t},h.update=function(t){return h.create().update(t)};for(var i=0;i<a.length;++i){var e=a[i];h[e]=u(e)}return h},p=function(t){var h=eval("require('crypto')"),s=eval("require('buffer').Buffer"),i=function(i){if("string"==typeof i)return h.createHash("sha1").update(i,"utf8").digest("hex");if(i.constructor===ArrayBuffer)i=new Uint8Array(i);else if(void 0===i.length)return t(i);return h.createHash("sha1").update(new s(i)).digest("hex")};return i};t.prototype.update=function(t){if(!this.finalized){var s="string"!=typeof t;s&&t.constructor===h.ArrayBuffer&&(t=new Uint8Array(t));for(var i,e,r=0,o=t.length||0,a=this.blocks;r<o;){if(this.hashed&&(this.hashed=!1,a[0]=this.block,a[16]=a[1]=a[2]=a[3]=a[4]=a[5]=a[6]=a[7]=a[8]=a[9]=a[10]=a[11]=a[12]=a[13]=a[14]=a[15]=0),s)for(e=this.start;r<o&&e<64;++r)a[e>>2]|=t[r]<<n[3&e++];else for(e=this.start;r<o&&e<64;++r)(i=t.charCodeAt(r))<128?a[e>>2]|=i<<n[3&e++]:i<2048?(a[e>>2]|=(192|i>>6)<<n[3&e++],a[e>>2]|=(128|63&i)<<n[3&e++]):i<55296||i>=57344?(a[e>>2]|=(224|i>>12)<<n[3&e++],a[e>>2]|=(128|i>>6&63)<<n[3&e++],a[e>>2]|=(128|63&i)<<n[3&e++]):(i=65536+((1023&i)<<10|1023&t.charCodeAt(++r)),a[e>>2]|=(240|i>>18)<<n[3&e++],a[e>>2]|=(128|i>>12&63)<<n[3&e++],a[e>>2]|=(128|i>>6&63)<<n[3&e++],a[e>>2]|=(128|63&i)<<n[3&e++]);this.lastByteIndex=e,this.bytes+=e-this.start,e>=64?(this.block=a[16],this.start=e-64,this.hash(),this.hashed=!0):this.start=e}return this.bytes>4294967295&&(this.hBytes+=this.bytes/4294967296<<0,this.bytes=this.bytes%4294967296),this}},t.prototype.finalize=function(){if(!this.finalized){this.finalized=!0;var t=this.blocks,h=this.lastByteIndex;t[16]=this.block,t[h>>2]|=o[3&h],this.block=t[16],h>=56&&(this.hashed||this.hash(),t[0]=this.block,t[16]=t[1]=t[2]=t[3]=t[4]=t[5]=t[6]=t[7]=t[8]=t[9]=t[10]=t[11]=t[12]=t[13]=t[14]=t[15]=0),t[14]=this.hBytes<<3|this.bytes>>>29,t[15]=this.bytes<<3,this.hash()}},t.prototype.hash=function(){var t,h,s=this.h0,i=this.h1,e=this.h2,r=this.h3,o=this.h4,n=this.blocks;for(t=16;t<80;++t)h=n[t-3]^n[t-8]^n[t-14]^n[t-16],n[t]=h<<1|h>>>31;for(t=0;t<20;t+=5)s=(h=(i=(h=(e=(h=(r=(h=(o=(h=s<<5|s>>>27)+(i&e|~i&r)+o+1518500249+n[t]<<0)<<5|o>>>27)+(s&(i=i<<30|i>>>2)|~s&e)+r+1518500249+n[t+1]<<0)<<5|r>>>27)+(o&(s=s<<30|s>>>2)|~o&i)+e+1518500249+n[t+2]<<0)<<5|e>>>27)+(r&(o=o<<30|o>>>2)|~r&s)+i+1518500249+n[t+3]<<0)<<5|i>>>27)+(e&(r=r<<30|r>>>2)|~e&o)+s+1518500249+n[t+4]<<0,e=e<<30|e>>>2;for(;t<40;t+=5)s=(h=(i=(h=(e=(h=(r=(h=(o=(h=s<<5|s>>>27)+(i^e^r)+o+1859775393+n[t]<<0)<<5|o>>>27)+(s^(i=i<<30|i>>>2)^e)+r+1859775393+n[t+1]<<0)<<5|r>>>27)+(o^(s=s<<30|s>>>2)^i)+e+1859775393+n[t+2]<<0)<<5|e>>>27)+(r^(o=o<<30|o>>>2)^s)+i+1859775393+n[t+3]<<0)<<5|i>>>27)+(e^(r=r<<30|r>>>2)^o)+s+1859775393+n[t+4]<<0,e=e<<30|e>>>2;for(;t<60;t+=5)s=(h=(i=(h=(e=(h=(r=(h=(o=(h=s<<5|s>>>27)+(i&e|i&r|e&r)+o-1894007588+n[t]<<0)<<5|o>>>27)+(s&(i=i<<30|i>>>2)|s&e|i&e)+r-1894007588+n[t+1]<<0)<<5|r>>>27)+(o&(s=s<<30|s>>>2)|o&i|s&i)+e-1894007588+n[t+2]<<0)<<5|e>>>27)+(r&(o=o<<30|o>>>2)|r&s|o&s)+i-1894007588+n[t+3]<<0)<<5|i>>>27)+(e&(r=r<<30|r>>>2)|e&o|r&o)+s-1894007588+n[t+4]<<0,e=e<<30|e>>>2;for(;t<80;t+=5)s=(h=(i=(h=(e=(h=(r=(h=(o=(h=s<<5|s>>>27)+(i^e^r)+o-899497514+n[t]<<0)<<5|o>>>27)+(s^(i=i<<30|i>>>2)^e)+r-899497514+n[t+1]<<0)<<5|r>>>27)+(o^(s=s<<30|s>>>2)^i)+e-899497514+n[t+2]<<0)<<5|e>>>27)+(r^(o=o<<30|o>>>2)^s)+i-899497514+n[t+3]<<0)<<5|i>>>27)+(e^(r=r<<30|r>>>2)^o)+s-899497514+n[t+4]<<0,e=e<<30|e>>>2;this.h0=this.h0+s<<0,this.h1=this.h1+i<<0,this.h2=this.h2+e<<0,this.h3=this.h3+r<<0,this.h4=this.h4+o<<0},t.prototype.hex=function(){this.finalize();var t=this.h0,h=this.h1,s=this.h2,i=this.h3,e=this.h4;return r[t>>28&15]+r[t>>24&15]+r[t>>20&15]+r[t>>16&15]+r[t>>12&15]+r[t>>8&15]+r[t>>4&15]+r[15&t]+r[h>>28&15]+r[h>>24&15]+r[h>>20&15]+r[h>>16&15]+r[h>>12&15]+r[h>>8&15]+r[h>>4&15]+r[15&h]+r[s>>28&15]+r[s>>24&15]+r[s>>20&15]+r[s>>16&15]+r[s>>12&15]+r[s>>8&15]+r[s>>4&15]+r[15&s]+r[i>>28&15]+r[i>>24&15]+r[i>>20&15]+r[i>>16&15]+r[i>>12&15]+r[i>>8&15]+r[i>>4&15]+r[15&i]+r[e>>28&15]+r[e>>24&15]+r[e>>20&15]+r[e>>16&15]+r[e>>12&15]+r[e>>8&15]+r[e>>4&15]+r[15&e]},t.prototype.toString=t.prototype.hex,t.prototype.digest=function(){this.finalize();var t=this.h0,h=this.h1,s=this.h2,i=this.h3,e=this.h4;return[t>>24&255,t>>16&255,t>>8&255,255&t,h>>24&255,h>>16&255,h>>8&255,255&h,s>>24&255,s>>16&255,s>>8&255,255&s,i>>24&255,i>>16&255,i>>8&255,255&i,e>>24&255,e>>16&255,e>>8&255,255&e]},t.prototype.array=t.prototype.digest,t.prototype.arrayBuffer=function(){this.finalize();var t=new ArrayBuffer(20),h=new DataView(t);return h.setUint32(0,this.h0),h.setUint32(4,this.h1),h.setUint32(8,this.h2),h.setUint32(12,this.h3),h.setUint32(16,this.h4),t};var y=c();i?module.exports=y:(h.sha1=y,e&&define(function(){return y}))}();
// --------------------------------------------------------------------------------------------------------------------------------------------



// evnt :: errors : global error handler
// --------------------------------------------------------------------------------------------------------------------------------------------
   (function()
   {
      window.addEventListener('error',function(event)
      {
         var e,m,f,l,s,i,n; event.preventDefault(); event.stopPropagation(); if(window.HALT){return}; window.HALT=1; e=event.error;
         f=event.filename; l=event.lineno; if(!e||isText(e)||((e.stack+'').indexOf('\n')<0)){e=(new Error((e+'')))}; n=(e.name||'usage');
         m=e.message; if(!f){f=fail.maybe;}; s=slog(PULL); i={};
         i.DBUGNAME=(i.DBUGNAME||n); i.DBUGMESG=(i.DBUGMESG||m); i.DBUGFILE=(i.DBUGFILE||f); i.DBUGLINE=(i.DBUGLINE||l);
         i.DBUGMODE='fail'; i.STACKLOG=(i.STACKLOG||s); cookie.create('dbugInfo',i); halt();
      });
   }());
// --------------------------------------------------------------------------------------------------------------------------------------------




// func :: requires : multi-function resource testing and acquisition
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      requires:function(d,f, e,m,spn,dne)
      {
         if(span(d)<1){f();return}; if(!isList(d)){d=[d]}; m=['js','jsm','es6','css','fnt']; spn=d.length; dne=0;  d.forEach(function(i)
         {
            if(e){return}; var t,r,a,x,n; t=(typeof i)[0]; if(t=='f'){r=i(); if(((typeof r)=='string')&&(r.length>1)){e=1; fail(r+'')}};
            if((t!='s')||(i.length<2)){e=1; fail('invalid require'); return};
            if((i.indexOf('/')<0)||(i.indexOf('.')<1)){e=1; fail('invalid require'); return}; if(Main.HALT){e=1; return};
            x=i.split('?')[0].split('.').pop(); if(m.indexOf(x)<0){e=1; fail('expecting: '+m.join(',')); return};
            if(i[0]!='/'){i=('/'+i)}; t=([].slice.call(document.head.querySelectorAll(('[purl="'+i+'"]')))); if(t.length>0){dne++; return};
            n=document.createElement((((x=='css')||(x=='fnt'))?'link':'script')); n.setAttribute('purl',i); n.purl=i;
            if((x=='css')||(x=='fnt')){n.rel='stylesheet'; n.href=i}else{n.type='text/javascript';  n.onerror=function(){e=1};
            n.async=true; n.src=i;}; document.head.appendChild(n);
         });

         if(dne>=spn){f(); return}; Busy.Listen('done',ONCE,f);
      }
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// tool :: (Encode/Decode)
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      encode:
      {
         BLOB:function(data,type)
         {
            var resl = (new Blob([data],{type:(type||'text/plain')}));
            return resl;
         },

         JSON:function(data)
         {
            return JSON.stringify(data);
         },

         URL:function(o,x)
         {
            var r,p,k,v; r=[]; for (p in o)
            {
               if (!o.hasOwnProperty(p)){continue}; k=(x?(x+"["+p+"]"):p); v=o[p];
               r.push(((v!==null)&&((typeof v)==="object"))?encode.URL(v,k):(encodeURIComponent(k)+"="+encodeURIComponent(v)));
            }
            return r.join("&");
         },

         b64:function(s){return btoa(s);},
      },


      decode:
      {
         BLOB:function(data,func)
         {
            var pars = new FileReader(); pars.onloadend=function(){func(pars.result);};
            pars.readAsDataURL(data);
         },

         JSON:function(data)
         {
            return JSON.parse(data);
         },

         b64:function(s){return atob(s);},
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// tool :: Array.prototype : string case
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Array.prototype)
   ({
      toProperCase:function(f)
      {
         var r = this; r.forEach(function(v,i){if((typeof v)!='string'){return}; r[i]=v.toProperCase()}); return r;
      },

      toCamelCase:function()
      {
         var r = this; r.forEach(function(v,i)
         {if((typeof v)!='string'){return}; r[i]=((i<1)?v.toLowerCase():(v[0].toUpperCase()+v.substr(1).toLowerCase()))}); return r;
      },

      toUpperCase:function()
      {var r = this; r.forEach(function(v,i){if((typeof v)!='string'){return}; r[i]=v.toUpperCase()}); return r;},

      toLowerCase:function()
      {var r = this; r.forEach(function(v,i){if((typeof v)!='string'){return}; r[i]=v.toLowerCase()}); return r;},

      hasAny:function()
      {let s=this; let a=list(arguments); if(isList(a[0])){a=a[0]}; let r=0; a.forEach((i)=>{if(s.indexOf(i)>-1){r++}}); return (r>0);},

      hasAll:function()
      {let s,a,c,r;s=this;a=list(arguments);if(isList(a[0])){a=a[0]};c=span(a);r=0;a.forEach((i)=>{if(s.indexOf(i)>-1){r++}});return (r===c);},
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// tool :: String.prototype : string tools
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(String.prototype)
   ({
      toProperCase:function()
      {
         let s=this.toString();if(s.length<1){return s};if(!s.hasAny(' ','-')){s=s.toLowerCase(); return (s[0].toUpperCase()+s.substr(1))};
         return (r.split(' ').toProperCase().join(' ').split('-').toProperCase().join('-'));
      },
      isProperCase:function(){var t = this.toString(); return (t===t.toProperCase(f))},

      toCamelCase:function(){return (this.toString().split('-').join(' ').split(' ').toCamelCase().join(''));},
      isCamelCase:function(f){var t = this.toString(); return (t===t.toCamelCase(f))},

      isLowerCase:function(){var t = this.toString(); return (t===t.toLowerCase())},
      isUpperCase:function(){var t = this.toString(); return (t===t.toUpperCase())},

      locate:function()
      {
         var s,f,t,a;  s=this.toString();  a=list(arguments);  if(isList(a[0])){a=a[0]};
         for(var i in a){if(a.hasOwnProperty(i)&&((s.indexOf(a[i]))>-1)){return a[i]}};
      },

      hasAny:function()
      {
         var s,f,t,a;  s=this.toString();  a=list(arguments);  if(isList(a[0])){a=a[0]};
         for(var i in a){if(a.hasOwnProperty(i)&&((s.indexOf(a[i]))>-1)){return true}};
      },

      hasAll:function()
      {
         var s,f,t,a;  s=this.toString();  a=list(arguments);  if(isList(a[0])){a=a[0];};  f=0;  t=span(a);
         a.forEach(function(i){if(s.locate(i)){f++}}); return (f==t);
      },

      expose:function(b,e,c,u)
      {
         var t=this.toString(); if((t.length<1)||!isText(b)||!isText(e)||(t.indexOf(b)<0)||(t.indexOf(e)<0)){return;};
         var r,m,n,a,i,z,x,y; r=[]; m=b.length; n=e.length;
         do
         {
            a=t.indexOf(b,0); i=(a+m); z=t.indexOf(t,e,i); do{i++; z=t.indexOf(e,i);}while(u&&(n<2)&&(z>0)&&(t[(z-1)]===u));
            if((a<0)||(z<1)){break;}; z+=n; x=t.substr((a+m),(z-a)); y=x.substr(0,x.indexOf(e));
            if(y&&(!c||(!!c&&y.hasAny(c)))){r[r.length]=y;}; t=t.substr(z); if(x===''){break;};
         }
         while(t); return r;
      },

      depose:function(b,e,c,u)
      {
         var r=this.toString(); var l=r.expose(b,e,c,u); if(!l){return r}; l.forEach((i)=>{r=r.swap((b+i+e),'')}); return r;
      },

      stub:function()
      {
         var t,c,i,b,e,a,s;  t=this.toString(); a=list(arguments);  if(isList(a[0])){a=a[0];};  c=t.locate(a);  if(!c){return};
         s=c.length; i=t.indexOf(c);  b=((i>0)?t.substr(0,i):'');  e=(t[(i+s)]?t.substr((i+s)):'');  return [b,c,e];
      },

      rstub:function()
      {
         var t,c,i,b,e,a,s;  t=this.toString(); a=list(arguments);  if(isList(a[0])){a=a[0];};  c=t.locate(a);  if(!c){return};
         s=c.length; i=t.lastIndexOf(c);  b=((i>0)?t.substr(0,i):'');  e=(t[(i+s)]?t.substr((i+s)):'');  return [b,c,e];
      },

      frag:function(x,l)
      {
         var s,t,r,q;s=this.toString(); t=(typeof x).substr(0,3); q=(typeof l).substr(0,3); if((s.length<1)||((t!='str')&&(t!='num'))){return};
         if((l===VOID)&&(t=='str')){return s.split(x);};if((t=='num')&&(q=='num')){if(x<0){x=s.length+x};if(l<0){l=s.length+l};return s.substr(x,l)};
         if((l===VOID)&&(t=='num')){r=[]; while(s){if(s.length<x){r.push(s);break;}else{r.push(s.substr(0,x)); s=s.substr(x);}}; return r;};
      },

      swap:function(f,r, s)
      {
         if(isNumr(f)){f=(f+'')}; if(isNumr(r)){r=(r+'')}; s=this.toString();
         if(isText(f)&&isText(r)){s=s.split(f).join(r); return s};
         if(isList(f)&&isText(r)){f.forEach((i,x)=>{s=s.split(i).join(r);});; return s};
         if(isList(f)&&isList(r)){f.forEach((i,x)=>{s=s.split(i).join(r[x]);});; return s};
         return s;
      },

      ltrim:function(c)
      {
         var t=this.toString(); if(t.length<1){return ''}; if(c===VOID){return t.replace(/^\s+/g,'')};
         if(isNumr(c)){c=(c+'')}; if(!isText(c)){return t}; let s=c.length; while(t.indexOf(c)===0){t=t.substr(s);}; return t;
      },

      rtrim:function(c)
      {
         var t=this.toString(); if(t.length<1){return ''}; if(c===VOID){return t.replace(/\s+$/g,'')};
         if(isNumr(c)){c=(c+'')}; if(!isText(c)){return t}; let s=c.length; while(t.frag((0-s),s)==c){t=t.frag(0,(0-s));}; return t;
      },

      Trim:function(b,e)
      {
         var t=this.toString(); if((b===VOID)&&(e===VOID)){return t.trim();}; if(isNumr(b)){b=(b+'')};
         if(e===VOID){e=b}else if(isNumr(e)){e=(b+'')}; if(b===e){t=t.ltrim(b).rtrim(e); return t;};
         if(b&&!e){return t.ltrim(b)}; if(e&&!b){return t.rtrim(e)}; return t;
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// func :: (misc) : tools
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      escapeHtml:function(text)
      {
         if(!isText(text)){return text};
         var map = // object
         {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
         };

         return text.replace(/[&<>"']/g, function(m){return map[m];});
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// tool :: copyToClipboard : happiness
// --------------------------------------------------------------------------------------------------------------------------------------------
   const copyToClipboard = str =>
   {
     const el=document.createElement('textarea'); el.value=str; el.setAttribute('readonly',''); el.style.position='absolute';
     el.style.left='-9999px'; document.body.appendChild(el); el.select(); document.execCommand('copy'); document.body.removeChild(el);
   };
// --------------------------------------------------------------------------------------------------------------------------------------------




// tool :: (events)
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(EventTarget.prototype)
   ({
      Signal:function(e,d,o, n,self,evnt)
      {
         slog(MAKE); self=(this||Main); expect(e,WORDTEXT); n=('on'+e); if(isText(d)&&d.hasAny(ONCE,EVRY)){o=d;d=VOID};
         if(o!=EVRY){o=ONCE}; if((d!==VOID)&&!d.detail){d={detail:d}}; evnt=(d?(new CustomEvent(e,d)):(new Event(e)));
         if(self[n]&&isFunc(self[n])){self[n].apply(self,[evnt]);
         if(o==ONCE){if(self[n].__evntID){delete Listen.jobs[self[n].__evntID]};self[n]=null}; return;};
         self.dispatchEvent(evnt);
      },


      Listen:function(evt,opt,hash,cbf, self,obst,fltr)
      {
         slog(MAKE); if(isFunc(evt)){cbf=evt;evt=VOID}; if(isFunc(opt)){cbf=opt;opt=EVRY}else if(isTron(opt)){fltr=opt; opt=EVRY};
         if(!Listen.jobs){Extend(Listen)({jobs:{},hash:function(f,x){this.x+=1; return sha1(this.x+cbf.toString())}.bind({x:0})})};
         if(isFunc(hash)){cbf=hash;hash=VOID}; self=(this||Main); if(!isText(hash)){hash=Listen.hash(cbf)}else{cbf=Listen.jobs[hash]};
         if(!opt){opt=EVRY}else if(!opt.hasAny([ONCE,EVRY])){opt=EVRY}; expect(cbf,FUNC); if(evt==VOID){evt=AUTO}; let ice;
         if(evt==AUTO){evt=keys(self,AUTO,'on*');}; if(!isList(evt)){if(evt.hasAny(' ')){ice=evt; evt=['keydown','mousedown']}else{evt=[evt]}};
         obst=this; evt.forEach((e)=>
         {
            if(e.substr(0,2)=='on'){e=e.substr(2)};
            Listen.jobs[hash]=[e,cbf]; let alt=VOID;

            if(obst&&(e=='dragstart')){obst.draggable=true; obst.setAttribute('draggable',true);};
            if(obst&&(e=='drop'))
            {
               if(!obst.ondragover){obst.ondragover=function(e){e.preventDefault();e.stopPropagation();}};
               alt=function(evnt,s)
               {
                  evnt.preventDefault(); evnt.stopPropagation(); var d,l,z; d=evnt.dataTransfer; l=d.files; s=this; z=([...l]); if(z.length<1)
                  {
                     let r=d.getData('text/plain'); if(!isPath(r)){Extend(evnt)({detail:r}); s.cbf.apply(s.tgt,[evnt]);return};
                     durl(r,function(t){Extend(evnt)({detail:t}); s.cbf.apply(s.tgt,[evnt]);}); return;
                  };
                  z.forEach(function(f){decode.BLOB(f,function(r){Extend(evnt)({detail:{name:f.name,data:r}}); s.cbf.apply(s.tgt,[evnt]);})});
               }
               .bind({tgt:obst,cbf:cbf});
            };

            if(e.hasAny('down','up','key','click','contextmenu'))
            {
               let kpr; if(e.hasAll('key',':')){kpr=e.stub(':'); e=kpr[0]; kpr=kpr[2]; if(e=='key'){e='keydown'}};

               alt=function(evnt)
               {
                  let evn,btn,tgt,kcl,hcn,cmb,dev,crd,rpt,pvk,rkc,rsp,grb,key; evn=evnt.type; tgt=evnt.target; cmb=[];
                  dev=(evn.hasAny('key')?'keyboard':'pointer'); pvk=this.pvk; rpt=evnt.repeat; key=this.kpr;
                  if((evnt instanceof MouseEvent)){dev='pointer'};

                  if(dev=='keyboard')
                  {
                     btn=evnt.key; if(btn==' '){btn='Space'};
                     if(key&&(btn.substr(0,key.length)==key)){key=btn}else{key=VOID};
                     if(!this.ice&&this.kpr){if(key==btn){this.run(evnt);};return};
                  }
                  else
                  {
                     if(evnt.which==null){btn=((evnt.button<2)?"LeftClick":((event.button==4)?"MiddleClick":"RightClick"))}
                     else{(btn=(evnt.which<2)?"LeftClick":((evnt.which==2)?"MiddleClick":"RightClick"))}; crd=[evnt.clientX,evnt.clientY];
                  };

                  if((btn=='RightClick')){grb=1;};
                  kcl={ctrlKey:'Control',shiftKey:'Shift',metaKey:'Meta',altKey:'Alt'};
                  kcl.Each((v,k)=>{if(evnt[k]||(btn.hasAny(v))){cmb.push(v)}; if(btn.hasAny(v)){hcn=1}});
                  if(span(cmb>0)&&!rpt&&!hcn){if(!pvk.hasAny(btn)){pvk.push(btn)}; this.pvk=pvk; cmb=cmb.concat(pvk);};

                  cmb=cmb.join(' ').trim(); if(!cmb.hasAny(' ')){cmb=VOID}; if(!cmb){this.pvk=[];}; if(cmb&&rpt){return};
                  evnt.device=dev; evnt.signal=(cmb||btn); evnt.coords=crd;
                  if(!this.ice){this.run(evnt,grb); return};
                  if(cmb&&(this.ice==cmb)){grb=1; this.run(evnt,grb); return};
                  return false;
               }
               .bind({tgt:self,cbf:cbf,ice:ice,pvk:[],kpr:kpr,run:function(fe,ge)
               {
                  if(ge){fe.preventDefault(); fe.stopPropagation();};
                  fe.Target=fe.currentTarget; this.cbf.apply(this.tgt,[fe]);
               }});
            };

            if(!alt)
            {
               alt=function(evnt){evnt.Target=evnt.currentTarget; this.cbf.apply(this.tgt,[evnt]);}.bind({tgt:self,cbf:cbf});
               if(e.hasAny('mutation'))
               {
                  alt.worker=(new MutationObserver(function(l)
                  {
                     let k,v,h,r; for(var m of l)
                     {
                        if(!this.flt){this.tgt.Signal(this.evt,{detail:m});continue};
                        k=keys(this.flt)[0]; v=this.flt[k]; h=m[k]; r=[]; if(!h||(h.length<1)){continue}; h=list(h);
                        h.Each((n)=>{let x=n.Select(v); if(!isList(x)){x=[x]}; r=r.concat(x)}); if(r.length<1){continue};
                        this.tgt.Signal(this.evt,{detail:r});
                     };
                  }.bind({tgt:(obst||self),evt:e,flt:fltr})));
                  alt.worker.observe((obst||document.documentElement),{childList:true,subtree:true});
               };
            };

            Listen.jobs[hash][1]=alt;

            if(opt==EVRY){self.addEventListener(e,Listen.jobs[hash][1],true);return};
            if(opt==ONCE){self[('on'+e)]=Listen.jobs[hash][1]; self[('on'+e)].__evntID=hash;return};
         });
         return hash;
      },


      Ignore:function(e,f, n,self,hash,x)
      {
         slog(MAKE); expect(e,TEXT); self=(this||Main); if(isFunc(f)){hash=sha1(f.toString());}else{hash=e};
         x=Listen.jobs[hash]; if(f===VOID){e=x[0]; f=x[1]}; expect(e,WORDTEXT); expect(f,FUNC); n=('on'+e);
         if(self[n]===f){self[n]=VOID}else{self.removeEventListener(e,f,true);}; delete Listen.jobs[hash]; return true;
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// tool :: Tunnel : get/set/rip keys of objects by dot-path reference
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      Tunnel:function(obj,key,val)
      {
         var map,end,rsl; map=key.split('.'); end=(map.length -1);

         map.forEach(function(ref,idx)
         {
            if (!obj.hasOwnProperty(ref)){obj[ref] = ((idx < end) ? {} : val);};
            if (idx < end){obj = obj[ref]; return;};
            rsl = ((val === undefined) ? obj[ref] : true);
         });

         return rsl;
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// tool :: fusion : prototype
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Function.prototype)
   ({
      fusion:function(obj, slf)
      {
         slf=this; obj.Each((v,k)=>{slf[k]=v}); return slf;
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// func :: unique : returns unique hash .. (timestamp + document-lifetime + 10-random-chars) .. `a` is the algorithm used but md5 is default
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      unique:function(a)
      {
         if(a!='sha1'){a='md5';}; let ml=Date.now(); let mc=performance.now();
         let rc=(Math.random().toString(36).substr(2,10)); return Main[a](ml+mc+rc);
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// func :: Auth : verifies user authenticity and returns user info as object
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      Hack:function(m){console.clear(); User(EXIT); console.error('stop breaking sh!t'); fail(m);},

      User:function(o)
      {
         let sc,sh; sc=this.info;
         if(o==VOID){return sc}; if(!isText(o)&&!isTron(o)){return;}; if(isText(o)){return sc[o]};
         // let qc,qh; qc=cookie.select('USERINFO');
      }
      .bind
      ({
         info:cookie.select('USERINFO'),
      }),

      userIs:function()
      {
         let a=list(arguments); let s=span(a); if(s<1){return}; var r=false; var un=User('name'); var uc=User('role');
         a.forEach((i)=>
         {
            if(r){return}; if(!isText(i)||(span(i)<2)){return}; let x=i[0]; i=i.substr(1);
            let c=(((x=='#')&&(un==i))?1:(((x=='.')&&uc.hasAny(i))?1:0)); if(c){r=true};
         });
         return r;
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// func :: (time) : tools
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      timeDiff:function(f,n, d,r,q,x)
      {
         if(!isNumr(f)||(span(f)<10)){fail('invalid timestamp');return}; f=(f.toFixed(3)*1);
         if(n){if(!isNumr(n)||(span(f)<10)){fail('invalid timestamp');return}; n=(n.toFixed(3)*1);}else{n=(Date.now()/1000)};
         d=((f<n)?(n-f):(f-n)); r={yrs:0,mth:0,wks:0,day:0,hrs:0,min:0,sec:0,ms:0}; if(d<1){r.ms=(d%1);return r};
         q={yrs:31557600,mth:2629800,wks:604800,day:86400,hrs:3600,min:60,sec:1};
         q.Each((v,k)=>{x=Math.floor(d/v); if(x){r[k]=x; d-=(v*x);}}); r.ms=(d%1); r.ms=(r.ms.toFixed(3)*1);
         return r;
      },

      timePast:function(f,n, d,l,r)
      {
         d=timeDiff(f,n); l={yrs:'years',mth:'months',wks:'weeks',day:'days',hrs:'hours',min:'minutes',sec:'seconds'}; r=VOID;
         d.Each((v,k)=>{if(v){r=(v+' '+((v>1)?l[k]:l[k].rtrim('s'))+' ago'); return STOP}});
         return r;
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------




// tool :: Server : operations
// --------------------------------------------------------------------------------------------------------------------------------------------
   Extend(Main)
   ({
      Server:
      {
         Stream:VOID,
         Sensor:{open:0,shut:0},

         Vivify:function(f)
         {
            let p=('/c0r3/lib/core/events/listen');
            this.Stream=(new EventSource(p,{withCredentials:true})); this.Stream.purl=p;
            this.Stream.Listen('open',function(evnt){let ts=(Date.now()/1000); Server.Sensor.open=ts;});
            this.Stream.Listen('init',function(evnt){f(evnt.data)});
            this.Stream.Listen('shut',function(evnt){Server.Stream.close();});
            this.Stream.Listen('fail',function(evnt){Server.Stream.close(); fail(evnt.data);});

            this.Stream.Listen('error',function(evnt)
            {
               if((evnt.type=='error')&&(evnt.eventPhase==2))
               {
                  if(this.readyState==0){this.Signal('reconnect');return};
                  purl(this.purl,function()
                  {
                     let h=this.echo.head.contentType.split(';')[0]; let m='text/event-stream';
                     if(h!=m){fail('expecting mime `'+m+'` from `'+this.purl+'`','EventSource');};
                  });
                  return;
               };

               let ts=(Date.now()/1000);
               if((ts-Server.Sensor.open)<=3000)
               {
                  Server.Stream.close(); fail("the server event emitter has issues\nhave a look in `"+Server.Stream.purl+"`");
               }
            });
         },

         Listen:function(e,p,f, c)
         {
            expect(e,WORDTEXT); if(isFunc(p)){f=p; p=VOID;}; expect(f,FUNC);
            c=function(evnt){let d=parsed(decode.b64(evnt.data)); this.cbfn.apply(this.cbfn,[d])}.bind({cbfn:f});
            if(!!this.Stream){this.Stream.Listen(e,c);}else{this.Vivify(function(m){this.Stream.Listen(e,c);}.bind(this));};
            if(!p){return;}; expect(p,PATHTEXT); purl({target:'/c0r3/lib/core/events/enhook',convey:{purl:p,emit:e},method:'POST'},function()
            {
               dump(this.echo.body);
            });
         },
      },
   });
// --------------------------------------------------------------------------------------------------------------------------------------------
