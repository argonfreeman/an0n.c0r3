<?
namespace Anon;


# info :: file : read this
# ---------------------------------------------------------------------------------------------------------------------------------------------
# this file is the base tool-library of any interface; it is used to define global constants, functions, classes and class-auto-loading
# ---------------------------------------------------------------------------------------------------------------------------------------------



# refs :: (constants) : initial
# ---------------------------------------------------------------------------------------------------------------------------------------------
   $l= 'AUTO KEYS VALS WORD XACT VOID NONE STEM TOOL FUNC PATH FOLD FILE LINK DUMP DONE GOOD INFO WARN FAIL MINI MIDI MAXI SKIP STOP TODO ';
   $l.='REPO DENY AFTR BFOR FLAT DEEP TUPL TRON NOFAIL NOINIT NOMAKE OK A B C D E F G H I J K L M N O P Q R S T U V W X Y Z';
   $l=explode(' ',$l); foreach($l as $i){define($i,":$i:");}; unset($l,$i);
   define('ALPHABET','0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'); define('SPECIALS','_^~|.-*+=#$@$!%?:;&/');
   define('ND',':not-defined:'); define('RO',':read-only:'); define('WO',':write-only:'); define('RW',':read-and-write:');
   define('NA',':not-accessible:'); define('LP',':lock-protected:');
   define('EXPROPER','!= !~ >= <= << >> /* */ // ## : = ~ < > & | ! ? + - * / % ^ @ . , ; # ( ) [ ] { } `');
   define('ACTWORDS','adjure vivify pacify exists create insert select update delete modify descry invoke verify import export deploy');
   $l=pget('$/c0r3/var/data'); foreach($l as $i){define((strtoupper($i).'DATA'),"sqlite::/c0r3/var/data/$i");}; unset($l,$i);

   define('ROOTPATH',getcwd());                       define('COREPATH',envi('COREPATH'));
   define('TECHMAIL',$_SERVER['SERVER_ADMIN']);       define('BOOTTIME',$_SERVER['REQUEST_TIME_FLOAT']);
   define('HOSTNAME',$_SERVER['HTTP_HOST']);          define('HOSTADDR',$_SERVER['SERVER_ADDR']);
   define('USERADDR',$_SERVER['CLIENT_ADDR']);        define('USERDEED',$_SERVER['CLIENT_METHOD']);
   define('USERMIME',$_SERVER['HTTP_ACCEPT']);        define('PROTOCOL',$_SERVER['REQUEST_SCHEME']);
   define('NAVIHOST',(PROTOCOL.'://'.HOSTNAME));      define('NAVIPURL',$_SERVER['REQUEST_URI']);
   define('NAVIFURI',(NAVIHOST.NAVIPURL));            define('NAVIPATH',$_SERVER['REDIRECT_URL']);
# ---------------------------------------------------------------------------------------------------------------------------------------------





# func :: emit : quickly reply with a clean text response
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function emit()
   {
      $a=func_get_args(); if(!headers_sent()){header_remove();}; if(ob_get_level()){$t=ob_get_clean();};
      header('HTTP/1.1 200 OK'); header('Content-Type: text/plain');
      foreach($a as $v){$v=(($v===null)?':NULL:':(($v===true)?':TRUE:':(($v===false)?':FALSE:':$v))); print_r($v);};
      if(function_exists('Anon\\done')){done(0);}else{exit;};
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: type : returns 4-char data-type
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function type($d)
   {
      if($d===null){return 'null';}; if(($d===true)||($d===false)){return 'bool';}; $t=strtolower(gettype($d));
      if(in_array($t,['integer','double','float'])){return 'numr';} if($t=='object')
      {return (($d instanceof \Closure)?'func':((property_exists($d,'info')&&property_exists($d,'stat'))?'pipe':'tron'));}
      elseif($t=='string'){return 'text';}elseif($t=='array'){return 'list';}elseif($t=='unknown type'){return 'none';}; return substr($t,0,4);
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: keys : returns the keys of the given identifier as list -or- null (if not array and not object)
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function keys($d,$h=0)
   {
      if(is_array($d)){return array_keys($d);};if(is_object($d)){$l=array_keys((get_object_vars($d)));$j=get_class_methods($d);
      foreach($j as $k){if(!$h&&strpos($k,'__')===0){continue;}; $l[]=$k;}; return $l;}elseif(is_string($d)&&class_exists($d,false))
      {$j=get_class_methods($d); if(!$j){$j=[];}; $l=array_keys(get_class_vars($d)); foreach($j as $k){$l[]=$k;};}else{$l=[];};
      $r=[]; foreach($l as $i){if(!$h&&strpos($i,'__')===0){continue;}; $r[]=$i;}; return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: vals : returns the values of the given identifier -or- null (if not array and not object)
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function vals($d)
   {
      if(is_array($d)){return array_values($d);}; if(!is_object($d)){return;}; $l=keys($d); $r=[];
      foreach($l as $k){$r[]=$d->$k;}; return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: span : returns the number of items in `$d`, func is number of args
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function span($d,$s=null)
   {
      if(($d===null)||($d===false)||($d==='')){return 0;}; $t=type($d);  if($t=='bool'){return 1;};  if($t=='numr'){$t='text'; $d=("$d");};
      if(($t=='text')||($t=='blob')){return ($s?mb_substr_count($d,$s):mb_strlen($d));};
      if(($t=='list')||($t=='tron')||($t=='link')){return count(keys($d));};
      if($t=='func'){$i=(new \ReflectionFunction($d)); return $i->getNumberOfParameters();};
   }

   function spanIs($d,$g=0,$l=0)
   {$s=span($d); $g=($g?$g:0); $l=($l?$l:$s); return (($s>=$g)&&($s<=$l));}
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: fail : throw exception shorthand
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function fail($m)
   {if(is_string($m)){$m=trim($m);}; if(!is_string($m)||(strlen($m)<3)){$m='misuse of `fail()`';}; throw new \ErrorException("$m");};
# ---------------------------------------------------------------------------------------------------------------------------------------------



# shiv :: (text-case) : turns text into case indicated by the function name .. returns text -or- null if invalid
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function lowerCase($d){if(is_string($d)){return strtolower($d);};}
   function upperCase($d){if(is_string($d)){return strtoupper($d);};}
   function proprCase($d){if(is_string($d)){return ucwords($d);};}

   function isLowerCase($d){return (lowerCase($d)===$d);}
   function isUpperCase($d){return (upperCase($d)===$d);}
   function isProprCase($d){return (proprCase($d)===$d);}
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: verify : shorthand for `preg_match` .. arguments swapped .. returns bool
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function verify($v,$x)
   {if(!is_string($v)||(strlen($v)<1)){return;}; return (preg_match($x,$v)?true:false);}
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: dval : returns runtime-value from simple string
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function dval($d)
   {
      if(!is_string($d)){return $d;}; $w=wrapOf($d); if($w&&isin(['""',"''",'``'],$w)){$d=unwrap($d);}; $v=trim($d);
      if(strlen($v)<1){return ($w?'':null);}elseif($w){return $d;}; $v=strtolower($v); if($v==='null'){return;};
      if($v==='true'){return true;}; if($v==='false'){return false;}; if(verify($v,'/^([+-]?([0-9]*)(\.([0-9]+))?)$/')){return ($v*1);};
      if(isUpperCase($d)){$n=(defined($d)?$d:(defined("Anon\\$d")?"Anon\\$d":null)); if($n){return constant($n);};}; $d=trim($d,',');
      if(!strpos($d,',')){return $d;}; $l=explode(',',$d); $r=[]; foreach($l as $i){$r[]=dval($i);}; return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: path : get full-path-of-mini-path  -or-  full-path-of-stem-name .. returns null if invalid/undefined
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function path($d)
   {
      if(!is_string($d)){return;}; $d=trim($d); $d=str_replace(['//','./'],'/',$d); if(strlen($d)<1){return;}; $r=ROOTPATH; $c=COREPATH;
      if(strpos($d,$r)===0){return $d;}; if(($d==='/')||($d==='.')){return $r;}; $p=trim($d,'/'); $a=explode('/',$p);
      $s=lpop($a); $p=implode($a,'/'); $p=($p?"$s/$p":$s); $r=(file_exists("$c/$s")?"$c/$p":"$r/$p");
      if(!preg_match('/^[a-zA-Z0-9-\/\.\$~_]{1,432}$/',$d)||(($d[0]!=='/')&&!file_exists($r))){$r=null;}; return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: knob : plain object
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class knob
   {
      function __construct($d){foreach($d as $k => $v){$this->$k=$v;};}
      function __get($k){if(property_exists($this,$k)){return $this->$k;};}
      function __call($k,$a){if(property_exists($this,$k)){return call_user_func_array($this->$k,$a);}; fail("undefined method `$k`");}
   }

   function knob($d=[])
   {
      if(is_string($d)){$d=trim($d); if($d===''){$d=[];}}elseif(!is_array($d)){$d=[];}; if(is_array($d)){return (new knob($d));};
      $d=str_replace(' ',';',$d); $l=explode(';',$d); $d=[]; foreach($l as $k => $v){$v=trim($v); if($v===''){continue;};
      if(!strpos($v,':')){$d[$v]=null;continue;}; $v=explode(':',$v); $k=trim($v[0]); $d[$k]=dval($v[1]);}; return (new knob($d));
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: (type-assert) : check if an identifier matches a specific data-type
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function isNull($d){return ($d===null);}
   function isVoid($d){return (($d===null)||($d===false)||($d===''));}
   function isBool($d){return (($d===true)||($d===false));}
   function isNumr($d,$t=null){if($t){$d=dval($d);}; return (is_int($d)||is_float($d)||is_real($d));}
   function isText($d,$g=0,$l=0){return (is_string($d)&&spanIs($d,$g,$l));}
   function isChar($d,$g=0,$l=0){return (verify($d,'/^([a-zA-Z]){1,36}$/')&&spanIs($d,$g,$l));}
   function isWord($d,$g=0,$l=0){return (verify($d,'/^([a-zA-Z])([a-zA-Z0-9_]){2,36}$/')&&spanIs($d,$g,$l));}
   function isHash($d){return verify($d,'/^[a-z0-9]{64}$/');}
   function isHost($d){return verify($d,'/^\w+([\.-]?\w+)*(\.\w{2,4})+$/');}
   function isMail($d){return verify($d,'/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/');}
   function isPass($d,$g=0,$l=0){$s=SPECIALS; return (verify($d,"/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$s])[A-Za-z\d$s]{6,36}/")&&spanIs($d,$g,$l));}
   function isList($d,$g=0,$l=0){return (is_array($d)&&spanIs($d,$g,$l));}
   function isTupl($d,$g=0,$l=0){if(!isList($d,$g,$l)){return false;}; return (empty($d)||(array_keys($d)===range(0,(count($d)-1))));}
   function isScal($d,$g=0,$l=0){return (is_array($d)&&!empty($d)&&(count(array_filter(array_keys($d),'is_string'))>0));}

   function isPath()
   {
      $a=func_get_args(); $d=path(array_shift($a)); if(!$d){return false;}; $s=count($a); if(($s<1)||!$a[0]){return $d;};
      $y=[FILE=>F,FOLD=>D,LINK=>L]; if(!file_exists($d)){return (($a[0]===ND)?true:false);}; if($a[0]===ND){return false;};
      if(($s<2)&&($a[0]===X)){return $d;}; $s=0; $t=(is_file($d)?F:(is_dir($d)?D:(is_link($d)?L:null))); if(!$t){return;};
      if(in_array(E,$a)){if($t===D){$s=count(array_diff(scandir($d),['.','..']));}else{$s=filesize($d);}}; $E=($s<1); $F=false;
      $R=is_readable($d); $W=is_writable($d); $l=[X,E,R,W,F,D,L,RO,WO,RW,NA]; foreach($a as $v){if(!isText($v,3,6)){return;};
      if(!in_array($v,$l)){if(in_array($v,$y)){$v=$y[$v];}else{return;}}; if($v===X){continue;}; if(($v===E)&&!$E){return $F;};
      $vr=strpos($v,'R'); $vw=strpos($v,'W'); if(($vr&&!$R)||($vw&&!$W)){return $F;}; if(($v===RW)&&(!$R||!$W)){return $F;};
      if(($v===RO)&&$vr){return $F;}; if(($v===WO)&&$vw){return $F;}; if(in_array($v,[F,D,L])&&($t!==$v)){return $F;};}; return $d;
   }

   function isPurl($d){$b=is_string($d); if(!$b||($b&&!strpos($d,'://'))){return false;}; $i=parse_url($d); return $i;}

   function isMixd($d)
   {
      if(!isList($d)&&!isTron($d)){return false;}; $fkt=0; $fvt=0; $r=false; foreach($d as $k => $v)
      {
         if(!$fkt){$fkt=type($k); $fvt=type($v); if($fvt==='list'){$fvt=(isTupl($v)?'tupl':'scal');}; continue;};
         $ckt=type($k); $cvt=type($v); if($cvt==='list'){$cvt=(isTupl($v)?'tupl':'scal');}; if(($ckt!==$fkt)||($cvt!==$fvt)){$r=true;break;};
      };
      return $r;
   }

   function isFlat($d)
   {
      if(!isTupl($d)){return false;}; $l=['null','bool','numr','text']; $r=true;
      foreach($d as $i){if(!in_array(type($i),$l)){$r=false;break;};}; return $r;
   }

   function isDeep($d){return (isList($d)&&!isFlat($d));}

   function isFunc($d)
   {
      if(!is_object($d)&&!isText($d,1)){return false;};if(is_object($d)){return (($d instanceof \Closure)?true:false);};$d=swap($d,'Anon\\','');
      if(isWord($d)){return (function_exists($d)||function_exists("Anon\\$d"));}; if(!isText($d,4)||!strpos($d,'::')){return false;};
      $p=explode('::',$d); $c=$p[0]; $c=(class_exists($c,false)?$c:(class_exists("Anon\\$c",false)?"Anon\\$c":0)); if(!$c){return false;};
      $m=$p[1]; return method_exists($c,$m);
   }

   function isPipe($d){return (is_object($d)&&!isFunc($d)&&(property_exists($d,'info')&&property_exists($d,'stat')));}
   function isTron($d){return (is_object($d)&&!isFunc($d)&&!isPipe($d));};
   function isTool($d){return (is_string($d)&&(class_exists($d,false)||class_exists("Anon\\$d",false)));}
   function isFold($d){$d=path($d); return ($d&&file_exists($d)&&is_dir($d));}
   function isFile($d){$d=path($d); return ($d&&file_exists($d)&&is_file($d));}

   function isBare($d,$ih=true)
   {
      if(isNull($d)||isBool($d)){return false;}; if(span($d)<1){return true;}; $p=path($d); if(!$p||($p&&!is_readable($p))){return;};
      if(is_dir($p)){$l=array_diff(scandir($p),['.','..']); $s=count($l); if($ih){$s=0;foreach($l as $i){if($i[0]!=='.'){$s++;}}};}
      else{$s=filesize($p); if($s<3){$l=trim(file_get_contents($p)); if(!$l){$s=0;}}}; return ($s<1);
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: defn : shorthand for `define()` and `constant()`; 1 arg gets; 2 args sets; compatible with Anon namespace & type/kind constant vals
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function defn($n,$v=null)
   {
      if($v===null)
      {
         if(isText($n,2)&&strpos($n,' ')){$n=explode(' ',$n);}; if(isTupl($n)){foreach($n as $k){$k=trim($k); defn($k,":$k:");}; return true;};
         if(!isWord($n)){return;}; $n=(defined($n)?$n:(defined("Anon\\$n")?"Anon\\$n":null)); return ($n?constant($n):null);
      };
      if(isWord($n)){define($n,$v); return true;}; if(isScal($n)){$n=tron($n);};
      if(isTron($n)){foreach($n as $k => $v){defn($k,$v);}; return true;};
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: expect : assert-or-fail
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function expect($t)
   {
      $r=knob(); $r->from=function($a)use($t)
      {
         if(isText($t)){$t=knob($t);}; if(!isScal($t)&&!isTron($t)){return;}; $p=0; $e=[]; foreach($t as $k => $v)
         {
            $k=lowerCase($k); if(in_array($k,['path','fold','file'],true)){$p=1;}; $f=('Anon\\is'.ucwords($k)); if(!isList($v)){$v=[$v];};
            array_unshift($v,$a); $r=call_user_func_array($f,$v); if($r){return $r;}; array_shift($v);
            $fl=[E=>'empty',R=>'readable',W=>'writable',F=>'file',D=>'folder',L=>'link'];
            if($p){foreach($v as $vx =>$vv){if(isset($fl[$vv])){$v[$vx]=$fl[$vv];}}}; $x=implode(' ',$v);
            if($x==='1'){$x='non-empty';}elseif($x==='0'){$x='empty';}elseif(isNumr(str_replace(' ','',$x),1))
            {$x=$k; $k=implode(' to ',$v); $k="with content-length from $k";}; $e[]=("expecting $x $k");
         };
         $e=implode(",\n or ",$e); fail($e);
      };
      return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: expect : assert-or-fail
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class expect
   {
      static function __callStatic($t,$a)
      {
         $s=span($a); if($s!==1){fail('`expect` requires exactly 1 argument from which to assert data-type');};
         return expect($t)->from($a[0]);
      }
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: swap : shorthand for `str_replace`
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function swap($x,$f,$r,$o=KEYS)
   {
      if(isNumr($x)){$x="$x";};
      if(is_string($x)){return str_replace($f,$r,$x);}; if(isList($x)||isTron($x)){$y=(isList($x)?[]:tron()); foreach($x as $k => $v)
      {if($o===KEYS){$k=swap($k,$f,$r); $z=dupe($v);}else{$z=swap($v,$f,$r);}; if(isList($x)){$y[$k]=$z;}else{$y->$k=$z;};}; return $y;};
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: frag : fragment tools like: `explode, str_split, substr, array_slice`
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function frag($x,$d=null,$i=null)
   {
      if(!isText($x,1)){return;}; if((($d===null)||($d==='')||is_int($d))&&($i===null))
      {if(!$d){$d=1;}; $s=span($x); $r=[]; for ($i=0;$i<$s;$i++){$r[]=mb_substr($x,$i,1);}; return $r;};
      if(is_int($d)&&is_int($i)){if($i<0){$i=(mb_strlen($x)+$i);}; return mb_substr($x,$d,$i);}; $r=$x; if(is_string($d))
      {$r=explode($d,$r); if(is_int($i)){$d=$i; $i=null;}elseif(is_array($i)&&(count($i)===2)){$d=$i[0]; $i=$i[1];}else{return $r;};};
      if(is_array($r)&&is_int($d))
      {
         if($i===null){if($d<0){$d=(count($r)+$d);}; return $r[$d];};
         if($i<0){$i=(count($r)+$i);}; $r=array_slice($r,$d,$i); return $r;
      };
      // dump($x,$d);
      fail::usage('invalid fragment arguments');
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: call
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function call($x,$a=[])
   {
      if(!isTupl($a)){$a=[$a];}; if(isText($x)){$x=swap($x,'Anon\\',''); $x="Anon\\$x";};
      if(isText($x)&&isin($x,'::')){$p=frag($x,'::'); return call_user_func_array([$p[0],$p[1]],$a);};
      if(isFunc($x)){return call_user_func_array($x,$a);}; fail('expecting 1st arg as any: class::method, or function, or closure');
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: wait : convenient `usleep` in milliseconds
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function wait($n=1)
   {if(!is_int($n)){$n=1;}; $t=($n*1000); usleep($t);};
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: msec : $a is str_time -or- int_offset .. $p is precision
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function msec($a=null,$p=null)
   {
      if(is_string($a)){$a=swap($a,'/','-'); return strtotime($a);}; $r=microtime(true); if(is_int($a)&&($p===null)){$p=$a; $a=null;};
      if(!is_int($a)){$a=0;}; $r+=$a; if(!is_int($p)||($p>8)||($p<0)){return $r;}; return round($r,$p);
   };
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: wait : example `wait::upon(function(){return true})->call(function(){...})`
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class wait
   {
      static function upon($u)
      {expect::func($u); return tron(['call'=>function($t)use($u){$w=1;do{$w=isVoid(call($u));if($w){wait(10);};}while($w);call($t);}]);}
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: crop : nice and tidy
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function crop($d,$f=[COREPATH,ROOTPATH,'Anon\\'],$r=['','',''])
   {
      if((!is_string($d)&&!is_array($d))||(is_string($d)&&(strlen($d)<1))||(is_array($d)&&(count($d)<1))){return $d;};
      $rp=getcwd(); $cp=COREPATH; if($f===null){$f=[$cp,$rp,'Anon\\'];}; if($r===null){$r=['','',''];};
      if(is_array($d)){$z=[]; foreach($d as $i){$z[]=crop($i,$f,$r);}; return $z;};
      if(is_int($f)){$t=crop($d); $p=((strlen($t)>$f)?'...':''); return (substr($t,0,$f).$p);}; return (str_replace($f,$r,$d));
   };
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: indx : returns key/index of $h that contains $n .. returns null if invalid or not-found
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function indx($h,$n=0,$p=0)
   {
      if(!is_int($p)){$p=0;}; if(is_int($n)&&($n<0)){$n=(span($h)+$n);};

      if(is_string($h))
      {
         if(!isset($h[$p])){return;}; if(isText($n)&&isset($h[$p])){$i=mb_strpos($h,$n,$p); return(($i===false)?null:$i);};
         if(is_int($n)){if(!isset($h[$n])){return;}; return mb_substr($h,$n,1);}; return;
      };

      if(is_array($h))
      {
         if(is_string($n)){$r=array_search($h,$n); return (($r===false)?null:$r);};
         if(is_int($n)){$k=array_keys($h); return (isset($k[$n])?$k[$n]:null);}; return;
      };

      if(isTron($h))
      {
         if(is_string($n)){foreach($h as $k => $v){if($v===$n){return $k;};}; return;};
         if(is_int($n)){$k=keys($h); return (isset($k[$n])?$k[$n]:null);}; return;
      };
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: isin : check if haystack contains needle, case sensitive, works with types: numr,text,tool,list,tron .. returns bool -or null
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function isin($h,$n,$o=AUTO)
   {
      if((span($h)<1)||(span($n)<1)){return false;}; if(isNumr($h)){$h="$h";}; if(is_string($h)&&isNumr($n)){$n="$n";};
      if(is_string($h)&&is_string($n)){if((strpos($h,'Anon\\')===0)&&isTool($h)){$h=keys($h);}else{return (mb_strpos($h,$n)!==false);};};
      if(is_string($h)&&isTupl($n)){foreach($n as $i){$r=isin($h,$i); if($r){return true;}; return false;}};
      if(!isList($n)&&(isList($h)||isTron($h))){if($o===AUTO){$o=(isTupl($h)?VALS:KEYS);};$h=(($o===VALS)?vals($h):keys($h));
      return in_array($n,$h,true);}; if(isDeep($h)&&isDeep($n)){foreach($h as $hi){foreach($n as $ni)
      {if(mash($hi)===mash($ni)){return true;}};}; return false;};
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: (lpop/rpop) : shorthands for `array_shift` & `array_pop` .. `$n` is number of times
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function lpop(&$a,$n=1)
   {
      if(!is_array($a)||(span($a)<1)||($n<1)){return;}; if(!is_int($n)){$n=1;};
      $r=[]; do{$n--; $r[]=array_shift($a);}while($n>0); return ((count($r)===1)?$r[0]:$r);
   }

   function rpop(&$a,$n=1)
   {
      if(!is_array($a)||(span($a)<1)||($n<1)){return;}; if(!is_int($n)){$n=1;};
      $r=[]; do{$n--; $r[]=array_pop($a);}while($n>0); return ((count($r)===1)?$r[0]:$r);
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: (ladd/radd) : shorthands for `array_unshift` & `..the other thing`
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function ladd(&$a,$i){if(is_array($a)){$r=array_unshift($a,$i); return $r;};}
   function radd(&$a,$i){if(is_array($a)){$a[count($a)]=$i; return count($a);};}
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: dupe : returns a new (duplicate) of something - use with caution
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function dupe($x,$n=null)
   {
      $t=substr(type($x),0,2); if(!$n&&($t!='li')&&($t!='no')){return $x;};
      if($t=='li'){$r=[]; $k=null; $v=null; foreach($x as $k => $v){$r[$k]=dupe($v);}; return $r;};
      if($t=='no'){$r=(clone $x); return $r;}; if(isNumr($x)){$x="$x";};
      if(!isText($x)||($n<1)){return $x;}; $r=''; for($i=0; $i<=$n; $i++){$r.=$x;}; return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------


# func :: exists : IF TEXT -> returns false if $v not exists as $t ... IF LIST/TRON -> returns bool if $t exists as key in $v
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function exists($v,$t=null)
   {
      if(span($v)<1){return false;}; $n='Anon\\'; if(is_string($v))
      {
         if($t===null){$t=(isPath($v)?PATH:(isTool($v)?TOOL:(isFunc($v)?FUNC:null)));}; if(($t!==PATH)&&(strpos($v,$n)===false)){$v="$n$v";};
         if($t===PATH){$r=path($v); return (($r&&file_exists($r))?$r:false);}; if(isFunc($v)&&($t===FUNC)){return $v;};
         if(!isTool($v)||!isText($t,1)){return false;}; if($t===TOOL){return true;}; $k=keys($v,1); return in_array($t,$k);
      };
      if((!isList($v)&&!isTron($v))||(span($t)<1)){return false;}; $k=keys($v,1); return in_array($t,$k);
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: hashed : shorthand for `hash('sha256',var_export($v,true))` .. supports multiple args .. unwraps `''` before hashing
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function hashed()
   {
      $a=func_get_args(); $r=''; foreach($a as $v)
      {$v=(($v===null)?':NULL:':(($v===true)?':TRUE:':(($v===false)?':FALSE:':$v))); $r.=var_export($v,true);};
      if(wrapOf($r)==="''"){$r=unwrap($r);}; return hash('sha256',$r);
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: mash : shorthand for `var_export($v,true)` .. $pp = pretty-print
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function mash($v,$pp=null)
   {$r=var_export($v,true); if($pp){$r=swap($r,['::__set_state',"=> \n"],['','=> ']); $r=swap($r,'=>   ','=> ');}; return $r;}
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: diff : returns the difference .. like `array_diff` -but also works on numr,text,list,scal,tron .. scal,tron uses key-diff
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function diff()
   {
      $a=func_get_args(); if((span($a)===1)&&isTupl($a[0])){$a=$a[0];}; if(span($a)<2){return (isset($a[0])?$a[0]:null);};
      $r=lpop($a); $t=type($r); do
      {
         $x=lpop($a); if($t!==type($x)){fail('diff args type mismatch');}; if($t==='numr'){$r=(($r>$x)?($r-$x):($x-$r)); continue;};
         if($t==='text'){$r=((indx($r,$x)!==null)?swap($r,$x,''):swap($x,$r,'')); continue;}; if(isTupl($r)&&isTupl($x))
         {
            if(isFlat($x)&&isFlat($r)){$d=array_diff($x,$r); $r=((count($d)>0)?$d:array_diff($r,$x)); continue;}; $d=[]; $n=null;
            foreach($x as $xi){$n=mash($xi); foreach($r as $ri){if(mash($ri)===$n){$n=null;};}; if($n){$d[]=$xi;};};  unset($n,$ri,$xi);
            if(span($d)<1){foreach($r as $ri){$n=mash($ri); foreach($x as $xi){if(mash($xi)===$n){$n=null;};}; if($n){$d[]=$ri;};};};
            $r=$d; continue;
         };
         if(($t!=='list')&&($t!=='tron')){fail('invalid diff arg type');};
         $rk=keys($r); $xk=keys($x); $kd=array_diff($xk,$rk); if(count($kd)<1){$kd=array_diff($rk,$xk);}; if(count($kd)<1){continue;};
         $rd=dupe($r); $xd=dupe($x); $r=(($t==='list')?[]:tron());
         foreach($kd as $kn){if($t==='list'){$r[$kn]=(exists($rd,$kn)?$rd[$kn]:$xd[$kn]);}else{$r->$kn=(exists($rd,$kn)?$rd->$kn:$xd->$kn);};};
      }
      while(count($a)>0); if(is_array($r)){$r=array_values($r);};
      return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: char : get character from int -or- hex, returns utf8 character -or null if invalid
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function char($d)
   {
      if((!is_int($d)&&!is_string($d))||(is_string($d)&&((strlen($d)!==4)||!ctype_xdigit($d)))){return;}; if(!is_int($d)){$d=hexdec($d);};
      if(span($d)<4){return chr($d);}; return mb_convert_encoding("&#{$d};",'UTF-8','HTML-ENTITIES');
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: fuse : merges arrays or objects .. result data-type is the same as the first arg .. duplicate keys are replaced
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function fuse()
   {
      $a=func_get_args(); $c=count($a); if($c<2){return (isset($a[0])?$a[0]:null);};
      if(($c===2)&&(isTupl($a[0])||(is_array($a[0])&&(count($a[0])<1)))&&isText($a[1])){return implode($a[1],$a[0]);};
      $r=dupe((array_shift($a))); $t=type($r); if(($t!='list')&&($t!='tron')){return $r;}; foreach($a as $i)
      {
         $q=type($i); $x=span($i); if(($q!='list')&&($q!='tron')){continue;};
         foreach($i as $k =>$v){if(is_numeric("$k")){$k=$x; $x++;}; if($t=='list'){$r[$k]=$v; continue;}; $r->$k=$v;};
      };
      return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: infuse : create tron by fusing 2 tuples using the 1st as keys and 2nd as values
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function infuse($a,$b)
   {
      if(!isTupl($a)||!isTupl($b)||(count($a)!==count($b))){fail('expecting 2 tuples of the same length');}; $r=tron();
      if(count($a)<1){return $r;}; foreach($a as $i => $k){$r->$k=$b[$i];}; return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: stub : finds first occurence of $d in $t then splits there once, returns array[left,dlim,right] .. or null if invalid
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function stub($t,$d,$r=0)
   {
      if(isTupl($d)){$d=locate($t,$d);};
      if(!is_string($t)||!is_string($d)){return;}; $p=(!$r?mb_strpos($t,$d):mb_strrpos($t,$d));
      if($p!==false){return [mb_substr($t,0,$p),$d,mb_substr($t,($p+mb_strlen($d)))];};
   }

   function lstub($t,$d){return stub($t,$d);};  function rstub($t,$d){return stub($t,$d,1);}
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: padded : pad sides of string with string .. 2 args pads both sides with same string .. supports arrays of strings also
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function padded($x,$l,$r=null)
   {
      if(!is_string($x)&&!is_array($x)){fail('expecting 1st arg as :text: or :list:');}; if($r===null){$r=$l;};
      if(is_string($x)){return "{$l}{$v}{$r}";}; foreach($x as $k => $v){$x[$k]="{$l}{$v}{$r}";}; return $x;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: lock
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class lock
   {
      private static $max;
      private static $dir;


      static function init()
      {
         self::$max=((ini_get('max_execution_time')*1)-1); $p=path('/c0r3/var/temp/lock'); self::$dir=$p;
      }


      static function exists($p)
      {
         if(!is_string($p)){return;}; $d=self::$dir; $h=sha1($p); $p=path("$d/$h"); if(!file_exists($p)){return false;};
         $n=microtime(true); $d=explode("\n",file_get_contents($p)); $t=($d[0]*1); if(($n-$t)<self::$max){return true;};
         fail::hogging("`$p` has been locked too long");
      }


      static function create($p)
      {
         if(!is_string($p)){return;}; if(self::exists($p)){fail::waiting("`$p` is locked");};
         $d=self::$dir; $h=sha1($p); $p=path("$d/$h"); $n=microtime(true); $i=PROCHASH; $m=umask(); umask(0);
         file_put_contents("$d/$h","$n\n$i"); umask($m); return true;
      }


      static function awaits($p,$m=true)
      {
         if(!is_string($p)){return;}; while(self::exists($p)){wait(20);}; $r=false; if($m){$r=self::create($p);}; return $r;
      }


      static function remove($p)
      {
         if(!is_string($p)){return;}; $d=self::$dir; $h=sha1($p); $p=path("$d/$h"); if(!file_exists($p)){return true;};
         $d=explode("\n",file_get_contents($p)); if($d[1]===PROCHASH){unlink($p); return true;}; return false;
      }
   }

   lock::init();
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: temp
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class temp
   {
      static function exists($p)
      {
         expect::{'text:1'}($p); $d=target(PROCDATA); $l=$d->select([using=>'temp',fetch=>'*']);
         if(count($l)<1){return false;}; $t=msec(0,3); $x=false; foreach($l as $i)
         {$r=$i->purl; if(($t-$i->made)>=$i->life){self::remove($i->hash); continue;}; if($r===$p){$x=true;};};
         unset($d); return $x;
      }

      static function create($p,$s=59)
      {
         expect::{'text:1'}($p); if(!isNumr($s)||($s<0)){$s=0;}; $h=hashed($p); $d="/c0r3/var/temp/$h";
         if(self::exists($p)){return $d;}; path::make($d,FOLD);
         target(PROCDATA)->insert([using=>'temp',write=>[$p,$h,msec(0,3),$s]]); return $d;
      }

      static function remove($h)
      {
         expect::hash($h); $d="/c0r3/var/temp/$h"; path::void($d);
         $r=target(PROCDATA)->delete([using=>'temp',where=>["hash = $h"]]); return true;
      }
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: flog
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class flog
   {
      static function __callStatic($n,$a)
      {
         $i=(!isset($a[0])?0:(!isset($a[5])?5:null)); $x=null; $o=['USERINFO','dbugInfo','GUITOKEN','dbugPath','dishPath','DBUGDATA'];
         if($i!==null)
         {
            $x=[]; $y=vars('client'); foreach($y as $k => $v){if(in_array($k,$o)){continue;};$x[$k]=(strtoupper(type($v)).' '.span($v));};
            $x=encode::jso($x); if($i){$a[]=$x;}else{$a=[vars('permit')->name,BOOTTIME,SITEMODE,USERDEED,NAVIPATH,$x];};
         };
         target(LOGSDATA)->insert([using=>$n,write=>$a]); return true;
      }
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: todo
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function todo($v)
   {if(!isText($v,2)){fail('invalid todo specification');}; flog::todo($v);}
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: path
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class path
   {
      static function mini($d)
      {
         $r=path($d); if(!$r){return;}; $r=str_replace([COREPATH,ROOTPATH],'/',$r); $r=str_replace('//','/',$r);
         if($r!=='/'){$r=rtrim($r,'/');}; $u=defn('USERNAME'); if(!$u){return $r;};
         $q="/w0rk/user/$u/repo/"; if(isin($r,$q)){$r=str_replace($q,'',$r); $r=('/'.stub($r,'/')[2]);};
         return $r;
      }


      static function full($d){return path($d);}
      static function levl($d){$p=self::mini($d); if(!$d){return;}; $p=trim($d,'/'); return span($p,'/');}
      static function time($d){$p=path($d); if(!$p){return;}; if(!exists($p)){return;}; $r=filemtime($p); return $r;}


      static function info($d)
      {
         expect::{'text:1'}($d); $n=null; $h=HOSTNAME; if(path($d)){$d=crop($d); $d="file://{$h}{$d}";};
         if(isin($d,'::')){$s=stub($d,'::'); $d="{$s[0]}://{$h}{$s[2]}";}; $i=tron(expect::purl($d)); $p=$i->path; $q=$i->query;
         $r=tron(['plug'=>$n,'user'=>$n,'pass'=>$n,'host'=>$n,'path'=>$n,'levl'=>0,'stem'=>$n,'twig'=>$n,'leaf'=>$n,'type'=>$n,'vars'=>$n]);
         $r->plug=$i->scheme; $r->user=$i->user; $r->pass=$i->pass; $r->host=$i->host; $r->path=$p; $r->frag=$n; $s='/'; if($p)
         {$r->levl=span(trim($p,$s),$s); $r->stem=self::stem($p); $r->twig=self::twig($p); $r->leaf=self::leaf($p); $r->type=self::type($p);};
         if($q){parse_str($q,$v); $r->vars=tron($v);}; if($i->fragment){$r->frag=$i->fragment;}; $r->purl=$d; return $r;
      }


      static function stem($d)
      {
         $p=path($d); if(!$p){return;}; $p=trim(self::mini($p),'/'); $p=trim($p,'/'); $s=span($p,'/');
         if($p==='/'){return '/';}; if($s<1){return $p;}; $r=explode('/',$p)[0]; return $r;
      }

      static function twig($d)
      {
         $p=path($d); if(!$p){return;}; $p=trim(crop($p),'/'); $s=span($p,'/'); if($s<1){return '/';};
         if($s<2){$r=stub($p,'/')[0]; return "/$r";}; $b=self::leaf("/$p"); $r=str_replace("/$b",'',"/$p"); return $r;
      }

      static function leaf($d,$x=1)
      {
         $p=path($d); if(!$p||($p&&($d==='/'))){return;} $s=rstub(rtrim($p,'/'),'/'); $b=$s[2]; if($x||(strpos($b,'.')===false)){return $b;};
         $s=rstub($b,'.'); return $s[0];
      }

      static function type($d)
      {
         $p=path($d); if(!$p){return;};  if(is_dir($p)){return 'fldr';}; if(strpos($d,'.')===false){return (is_file($p)?'file':'none');};
         $x=rstub($d,'.')[2]; return (preg_match('/^[a-zA-Z0-9]{1,8}$/',$x)?$x:(is_file($p)?'file':'none'));
      }

      static function size($d,$o=null)
      {
         $p=path($d);  if(!$p){return;}; if(isFile($p)){return filesize($p);}; if(!isFold($p)){return;}; $rp=ROOTPATH; $cp=COREPATH;
         $h=(isin($p,$cp)?$cp:$rp); $t=trim(self::twig($d),'/'); $h="$h/$t"; $f=self::leaf($p); if(!file_exists("$h/$f")){return;};
         $r=exec::{"du -sb ./$f"}($h); $x=stub($r,[' ',"\t"]); if($x){$r=$x[0];}; if(isNumr($r,1)){return ($r*1);};
         fail("failed to get byte-size of: `$h/$f`");
      }

      static function mime($d)
      {
         if(!isWord($d)&&!isPath($d)){return;}; $x=(isWord($d)?$d:self::type($d));
         $m=conf('c0r3/mime')->$x; if(!$m){$m='unknown/undefined';}; return $m;
      }

      static function code($d)
      {$p=path($d); if(!$p){return;}; return (is_readable($p)?200:(file_exists($p)?403:404));}

      static function stat($d)
      {$c=self::code($d); if(!$c){return;}; return (($c===404)?'undefined':(($c===403)?'forbidden':'OK'));}

      static function goal($arg,$lev=0)
      {
         if($arg==='/'){$idx=self::indx(ROOTPATH); if($idx){$pth="/$idx";}else{$pth=conf('c0r3/boot')->HOMEPATH; if(!isPath($pth,R))
         {$pth=('/c0r3/doc/'.self::indx('/c0r3/doc'));}}; expect::{'path:R'}($pth);}
         else{$pth=path($arg); if(isFold($pth)){$idx=self::indx($pth); if($idx){$pth="$pth/$idx";};}; $pth=crop($pth);};
         $alt=conf('c0r3/path')->$pth; if(isPath($alt)){$pth=$alt;}; $rsl=tron(['path'=>$pth,'exec'=>null,'args'=>[]]);
         $prt=frag(trim($pth,'/'),'/'); $top=(span($prt)-1); $bfr=""; $idx=0; $tpe=0; foreach($prt as $lvl => $itm)
         {
            if($rsl->exec&&($idx!==$itm)){$rsl->args[]=$itm;continue;}; $bfr.="/$itm"; $pth=path($bfr);
            if(isFold($pth)){$idx=self::indx($pth); if($idx){$pth="$pth/$idx";};}elseif(!isFile($pth)&&isFile("$pth.php")){$pth="$pth.php";};
            $alt=conf('c0r3/path')->$pth; if(isPath($alt)){$pth=$alt;};
            $pth=crop($pth); $tpe=self::type($pth); if(($tpe==='php')&&($pth!=='/c0r3/aard.php')&&($lvl>=$lev)){$rsl->exec=$pth;};
         };
         if($rsl->path){$rsl->path=self::w0rk($rsl->path);}; if($rsl->exec){$rsl->exec=self::w0rk($rsl->exec);};
         return $rsl;
      }

      static function xarg($d,$NI=false,$NM=false)
      {
         if(!isPath($d)){return;}; $a=trim($d,'/'); if(!isin($a,'/')){return;}; $a=frag($a,'/'); $b=''; $f=null; $r=null; do
         {
            $i=lpop($a); $b.="/$i";
            $p=(isFile("$b/aard.php")?"$b/aard.php":(isFile("$b.php")?"$b.php":null)); if(!$p){continue;};
            $r=import($p,['_ARGS'=>$a],$NI); if(!isTool($r)&&!isTron($r)&&!isFunc($r)){continue;}; $f=(isset($a[0])?$a[0]:'init');
            if(isTool($r)&&isFunc("$r::$f")){$f="$r::$f";break;}elseif(isFunc($r)){$f=$r;}
            elseif(isTron($r)){$f=bore($r,"$b/$f"); if(isFunc($f)){break;}else{$r=null;$f=null;}};
         }
         while(count($a)); if(isset($a[0])){lpop($a);};
         return (!$f?null:tron(['func'=>$f,'args'=>$a]));
      }

      static function mode($d,$m=null)
      {
         $p=path($d); if(!$p){fail::arguments('expecting 1 arg as :path:');}; if(!file_exists($p)){return ND;}; $l=lock::exists($p);
         $r=is_readable($p); $w=is_writable($p); if($l){return ((!$r&&!$w)?NA:LP);}; if($r&&$w){return RW;}; return ($r?RO:WO);
      }

      static function flat($d)
      {
         expect::tron($d); $r=''; if($d->verb){$r.="$d->verb::";}; if($d->plug){$r.="$d->plug://";};
         if($d->user&&$d->pass){$r.="$d->user:$d->pass@";}elseif($d->user){$r.="$d->user@";}; if($d->host){$r.=$d->host;};
         if($d->path){$r.="/$d->path";}; return $r;
      }

      static function prep($p='/',$t=REPO,$o=AFTR,$l=null)
      {
         $p=expect::{'path:R,D'}($p); if($l===null){$l=0;}else{$l++;}; $lst=array_diff(scandir($p),['.','..']); $spn=count($lst);
         if(($o===REPO)&&($o===AFTR))
         {
            if($l<1){expect::{'path:R,D'}("$p/.git");}; $kf='.gitkeep';
            if($spn<1){self::make("$p/$kf","!",FILE);}elseif(isFile("$p/$kf")){unlink("$p/$kf");};
         };
         foreach($lst as $itm){if(isFold("$p/$itm")){self::prep("$p/$itm",$t,$o,$l);}};
      }

      static function scan($h,$o=TUPL,$x=FLAT,$y=null)
      {
         $h=expect::{'path:R'}($h); $r=(($o===TUPL)?[]:tron()); $t=self::type($h); if($t!=='fldr'){return file_get_contents($h);};
         $l=array_diff(scandir($h),['.','..']); $f=''; $s=null; if(isTupl($y,1)){$f=lpop($y); expect::{'text:2'}($f); $s=count($y);};
         foreach($l as $i)
         {
            if($i[0]==='.'){continue;}; $p="$h/$i"; $t=self::type($p); $c=((($t==='fldr')&&(!$y||($y&&($s>0))))?self::scan($p,$o,$x,$y):null);
            if($f&&((($f===FOLD)&&($t!=='fldr'))||(($f===FILE)&&!isFile($p))||(($f!==FOLD)&&($f!==FILE)&&!locate($i,$f)))){continue;};
            if($o===TUPL){$r[]="/$i"; if($c&&($x===DEEP)){foreach($c as $v){$r[]=swap("/$i/$v",'//','/');}}; continue;};
            $z=tron(['type'=>$t,'size'=>self::size($p)]); if($c){$z->kids=$c;}; $r->$i=$z;
         };
         return $r;
      }

      static function indx($d,$x=null)
      {
         $p=expect::{'path:R,D'}($d); $c=conf('c0r3/boot')->DIRINDEX; $l=[]; foreach($c->name as $cn){foreach($c->type as $ct){$l[]="$cn.$ct";}};
         $r=locate(array_diff(scandir($p),['.','..']),$l); if(!$r){return;}; if(!$x||!isin($r,'.')){return $r;}; $z=explode('.',$r)[1];
         return (($z===$x)?$r:null);
      }

      static function make($p,$d=null,$t=AUTO,$m=AUTO)
      {
         $p=expect::path($p); $a=self::mode($p); if(($d===null)&&($a!==ND)){return true;}; if(($d===FILE)||($d===FOLD)){$t=$d; $d=null;};
         if(($a!==ND)&&!strpos($a,'writ')){fail::permission("`$p` is $a");}; $h=self::twig($p); $q=self::mode($h); $n=($q===ND); $u=umask();
         if($q===ND){$l=self::leaf($p); self::make($h,FOLD);}elseif(($a===ND)&&!strpos($q,'writ')){fail::permission("`$h` is $q");};
         if($t===AUTO){$t=self::type($p);$t=((($t==='fldr')||($t==='none'))?FOLD:FILE);}; $f='could not create';
         if($m===AUTO){$m=(($t===FILE)?vars('chmods')->WF:vars('chmods')->WD);}; if(($t===FILE)&&!is_string($d)){$d=texted($d);};
         if($t===FILE){lock::awaits($p); umask(0); file_put_contents($p,$d); if($n){chmod($p,$m);}; umask($u); lock::remove($p); return true;};
         if(($t===FOLD)&&($a===ND)){lock::awaits($p); umask(0); mkdir($p); if($n){chmod($p,$m);}; umask($u); lock::remove($p);};
         if($t!==FOLD){fail::arguments('expecting type as :FILE: or :FOLD:');}; if(!isScal($d)&&!isTron($d)){return true;};
         foreach($d as $k => $v){self::make("$p/$k",$v);}; return true;
      }

      static function void($p)
      {
         $p=expect::path($p); if(($p===COREPATH)||($p===ROOTPATH)){fail("please don't do that"); return;}; $p=crop($p);
         $h=self::twig($p); $t=self::leaf($p); lock::awaits($p); $r=exec::{"rm -rf ./$t"}($h); lock::remove($p); return true;
      }

      static function copy($s,$d,$f=null)
      {
         expect::path($s); expect::path($d); $ss=path::stat($s); $dm=path::mode($d); if($ss!=='OK'){fail("`$s` is $ss");};
         if(($dm!==ND)&&($dm!==RW)&&($dm!==WO)){fail("`$d` is $dm");}; $sd=isFold($s); $dd=isFold($d); $sf=isFile($s); $df=isFile($d);
         if(($dm!==ND)&&$sd&&$df){fail('cannot copy folder into file');}; if($df&&!$f){fail("`$d` exists");}; //$dh=path::twig($d);
         if($sf){return self::make($d,self::scan($s),FILE);}; if($dm===ND){self::make($d,FOLD);}; $l=array_diff(scandir($s),['.','..']);
         $z=true; foreach($l as $i => $v){$r=self::copy("$s/$i","$d/$i",$f); if(!$r){$z=false;};}; return $z;
      }

      static function line($p,$b,$e=null)
      {
         expect::{'file:R'}($p); $d=path::scan($p); if(!isin($d,$b)||($e!==null)&&!isin($d,$e)){return;}; $d=frag($d,"\n");
         if($e===null){foreach($d as $x => $l){if(isin($l,$b)){return ($x+1);}}; return;}; $r=[]; $bx=0; $ex=0; foreach($d as $x => $l)
         {$x=($x+1); $bx=(isin($l,$b)?$x:$bx); $ex=(isin($l,$e)?$x:0); if($bx&&$ex){$r[]=[$bx,$ex]; $bx=0; $ex=0;};};
         return ((span($r)<1)?null:$r);
      }

      static function repo($p)
      {
         if(!isPath($p)){return;}; if(isRepo($p)){return $p;}; $x=trim($p,'/'); $l=explode('/',$x);
         $r=null; do{rpop($l); $p=implode('/',$l); if(isRepo("/$p")){$r="/$p";};}while((count($l)>0)&&($r===null)); if(!$r){return;};
         return $r;
      }

      static function w0rk($p)
      {
         if(!isPath($p)){fail('expecting :PATH:');}; if($p!=='/'){$p=rtrim($p,'/');}; if(!isin(USERTYPE,['worker','leader'])){return $p;};
         $r=self::repo($p); if(!$r){return $p;}; $n=sha1($r); $u=USERNAME; $h="/w0rk/user/$u/repo/$n"; $r=($h.$p); $r=rtrim($r,'/');
         if(!exists($r)){$r=$p;}; return $r;
      }
   };
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: durl : returns data-url from existing file-path .. returns null if invalid-path or not-a-readable-file
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function durl($d)
   {
      $p=path($d); if(!$p||!isFile($p)||!is_readable($p)){return;}; $v=path::scan($p);
      $m=path::mime($p); $r=base64_encode($v); return "data:$m;base64,$r";
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: (wrapping) : text functions for performing operations on first-and-last characters of a string if it's "wrapped"
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function isWrap($d,$b=1)
   {
      if(!is_string($d)||(strlen($d)<2)){return false;}; $r=(mb_substr($d,0,1).mb_substr($d,-1,1));
      if(in_array($r,['``','""',"''",'‷‴','[]','{}','()','<>','::','\\\\'])){return ($b?true:$r);};
   }

   function wrapOf($d){$r=isWrap($d,0); return ($r?$r:'');}
   function unwrap($d){if(!isWrap($d)){return $d;}; return mb_substr($d,1,(mb_strlen($d)-2));}
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: locate : returns first-matched `$n` in `$h` .. returns all if `$w=XACT` and all found .. or null if not-found/invalid .. supports `*`
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function locate($h,$n=null,$w=AUTO)
   {
      if(isNumr($h)){$h="$h";};  $hs=span($h); $ns=span($n);
      if(($hs<1)||($ns<1)){return;}; if(($n==='*')&&($w===AUTO)){return $h;}; $ht=type($h); $nt=type($n);

      if($ht=='text')
      {
         // if(($h==='Anon\\encode')&&($n==='jso')){dump(locate(keys($h),$n));};
         if((mb_strpos($h,'\\')!==false)&&(mb_strlen($h)<36)&&(mb_strpos($h,"\n")===false)&&class_exists($h,false))
         {return locate(keys($h),$n);};

         if(($nt=='text')||($nt=='numr'))
         {
            $n="$n"; $z=(($ns>1)?($n[0].mb_substr($n,-1,1)):''); $a=span($n,'*');
            if(($a<1)||($a>2)||($w!==AUTO)||!$z||(($z[0]!=='*')&&($z[1]!=='*'))){return((mb_strpos($h,$n)!==false)?$n:null);};
            $f=str_replace('*','',$n); if($z==='**'){return ((mb_strpos($h,$f)!==false)?$h:null);};
            $l=mb_strlen($f); if($z[0]=='*'){return ((mb_substr($h,(0-$l))===$f)?$h:null);}; return ((mb_substr($h,0,$l)===$f)?$h:null);
         };

         $f=0; $w=(($w===XACT)?0:1);
         if(isTupl($n))
         {
            $c=count($n); foreach($n as $v)
            {$vt=type($v); if((($vt=='text')||($vt=='numr'))&&(mb_strpos($h,"$v")!==false)){if($w){return $v;};$f++;};};
            return (($f<$c)?null:$n);
         };
         return;
      };

      if(($ht==='list')||($ht==='tron'))
      {
         $x=(($w===XACT)?1:0); if($x){$w=AUTO;}; $z=0;
         if($w===AUTO){$w=((($ht=='tron')||isScal($h))?KEYS:VALS);}elseif(($w!==KEYS)&&($w!==VALS)){return;};
         if(!isTupl($n)){$n=[$n];}; $d=(($w==KEYS)?keys($h):vals($h));
         foreach($n as $f){$r=(in_array($f,$d,true)?$f:null); if($r){if(!$x){return $f;}; $z++;};};
         return (($z<1)?null:$n);
      };
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: tron : Typical Runtime Object Norm .. return NULL on undefined keys .. supports `*` lookup .. collapses into JSON text
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class tron
   {
      function __construct($d=null,$p=null)
      {
         if(!isScal($d)&&!isTron($d)){return $this;};
         foreach($d as $k => $v){$k=trim($k); if($p===U){$k=unwrap($k);}; if(strlen($k)<1){continue;};
         if(isScal($v)||isTron($v)){$v=(new tron($v,$p));}; if(is_string($v)&&($p===V)){$v=parsed($v);}; $this->$k=$v;};
      }

      function __set($k,$v){if(!property_exists($this,$k)){$this->$k=$v; return;};}
      function &__get($x)
      {
         $n=$x; $r=null; $fx=strpos($n,'*'); $e=property_exists($this,$n); if(!$fx&&$e){return $this->$n;}; foreach($this as $k => $v)
         {
            if(strpos($k,'__')===0){continue;}; $fk=strpos($k,'*'); if(($fk===false)&&($fx===false)){continue;};
            if((($fk!==false)&&locate($x,$k))||(($fx!==false)&&is_string($v)&&locate($v,$x))){$r=$v; return $v;};
         };
         return $r;
      }

      function __call($k,$a){if(property_exists($this,$k)){return call_user_func_array($this->$k,$a);};}
      function __tostring(){return json_encode($this,JSON_UNESCAPED_SLASHES);}
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function tron($d=null,$p=null){return (new tron($d,$p));};
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: bore : refer to children of object/array by using folder-like-path-string .. if `$v` then `$p` is made and value is set inside `$o`
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function bore(&$o,$p,$v=null,$m=null)
   {
      if(!isTron($o)&&!isList($o)){fail::arguments('expecting type :tron: or :list:');}; expect::{'text:1'}($p);
      if((!isWord($p)&&!isNumr($p,1)&&!isPath($p))){fail::arguments('expecting :word: or :path:');};
      $p=trim($p,'/'); $r=&$o; $a=(isList($r)?1:0); $d=($v===VOID); $g=($v===null); $l=explode('/',$p); $z=(count($l)-1);
      foreach($l as $x => $k)
      {
         $f=locate($r,[$k,(RO.$k)]);if(!$f&&$g){return;}; if(($x<$z)&& $f){if($a){$r=&$r[$k];}else{$r=&$r->$k;};continue;}; # seek
         if(($x<$z)&&!$f&&!$g&&!$d){if($a){$r[$k]=[];$r=&$r[$k];}else{$r->$k=tron($m);$r=&$r->$k;};continue;}; if($x!==$z){continue;}; # drill
         if($f&&$g){return(($a)?$r[$k]:$r->$k);}; if(!$g&&!$d){if($a){$r[$k]=$v;}else{$r->$k=$v;};return;}; # get/set
         if($f&&$d){if($a){unset($r[$k]);}else{unset($r->$k);};}; # rip
      };
   };
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: gmod : get fileperms - as expected by `chmod`
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function gmod($d){$p=path($d); if($p){return fileperms($p);}; expect::path($d);};
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: vars : unique read-only global variables .. can only be set once during runtime
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class vars
   {
      private static $meta=[];
      static function exists($k){if(isText($k,1)){return array_key_exists($k,self::$meta);}; fail::arguments('expecting 1 arg as :text:');}
      static function select($k){if(self::exists($k)){return self::$meta[$k];};}
      static function create($d,$v=null)
      {
         if(is_string($d)&&($v!==null)){$d=[$d=>$v]; $v=null;}; if(!isScal($d)&&!isTron($d)){fail::arguments('expecting :scal: or :tron:');};
         foreach($d as $k => $v){if(self::exists($k)){fail("vars->$k is readonly");}; self::$meta[$k]=$v;};
      }
   };

   function vars($v)
   {
      if(is_string($v)){$v=trim($v);}; if(!isText($v,1)){return;}; $v=swap($v,'.','/'); if(!isin($v,'/')){return vars::select($v);};
      $p=stub($v,'/'); $r=vars::select($p[0]); return bore($r,$p[2]);
   };
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: ipv6 : convert given `$d` from ipv4 -or ipv6 to expanded ipv6 if `$d` is ipv4; else returns unchanged `$d`
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function ipv6($d)
   {
      expect('text:3')->from($d); $x=$d; $s=span($x,':'); if($s===7){return $x;}; if($s<1){$x="::$x";}; $c=rstub($x,':')[2]; $x=inet_pton($x);
      if(preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/',$c))
      {$r=[]; for($i=0;$i<8;$i++){$r[$i]=sprintf("%02x%02x",ord($x[$i*2]),ord($x[($i*2)+1]));}; return (implode(":",$r));};
      $x=unpack("H*hex",$x); $r=substr(preg_replace("/([A-f0-9]{4})/","$1:",$x['hex']),0,-1); return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: expose : extract strings between strings, returns list of extracted strings
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function expose($t,$b,$e,$u=null)
   {
      if(!isText($t)||!isText($b)||!isText($e)||(mb_strpos($t,$b)===false)||(mb_strpos($t,$e)===false)){return;};
      $r=[]; $m=mb_strlen($b); $n=mb_strlen($e);
      do
      {
         $a=indx($t,$b,0); $i=($a+$m); $z=indx($t,$e,$i); do{$i++; $z=indx($t,$e,$i);}while($u&&($n<2)&&$z&&($t[($z-1)]===$u));
         if(($a===null)||($z===null)){break;}; $z+=$n; $x=mb_substr($t,($a+$m),($z-$a));
         $r[]=mb_substr($x,0,mb_strpos($x,$e)); $t=mb_substr($t,$z); if($x===false){break;};
      }
      while($t); return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: impose : replace strings between strings
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function impose($t,$b,$e,$x)
   {
      expect::text($x); $l=expose($t,$b,$e); foreach($l as $i){$t=str_replace("{$b}{$i}{$e}","{$b}{$x}{$e}",$t);}; return $t;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: remove : delete contents with specification
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function remove($v,$b=null,$e=null)
   {
      expect::text($v); if(isText($b,1)&&isText($e,1)){$l=expose($v,$b,$e); foreach($l as $i){$f="{$b}{$i}{$e}";$v=str_replace($f,'',$v);};};
      return $v;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: parsed : turn plain text into implied value, returns value -OR- null if NULL/invalid
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function parsed($d,$v=null)
   {
      $x=''; if(is_string($d)){$x=trim($d);}; if(!isText($x,1)||($x==='NONE')||($x==='NULL')||($x==='VOID')){return;};
      if(isin(['TRUE','FALS','FALSE'],$x)){return (($x[0]==='T')?true:false);}; if(isNumr($d,true)){return ($d*1);}; $x=$d; $d=null;
      if(span($x)<2){return $x;}; if(locate($x,['data:','/',';base64,'],XACT)){$e=expose($x,'("','")'); if($e){$x=$e[0];}; $p=stub($x,',');
      $m=expose($p[0],':',';')[0]; $x=base64_decode($p[2]); return ['head'=>['Content-Type'=>$m,'Content-Length'=>span($x)],'body'=>$x];};
      $w=wrapOf($x); if(($w==='[]')||($w==='{}')){return decode::jso($x);}; if($w){$x=trim(unwrap($x));}; $s=strlen($x); if($s<1){return;};
      $l=locate(['=',':',','],$x); if(!$w&&$l){if($l==='='){$x=ltrim($x,'?');}elseif($l===':'){$x=str_replace([':',';'],['=','&']); $l='=';};
      if($x==='='){$r=parse_str($x);}else{$x=explode(',',$x);}; $r=[]; foreach($x as $xk => $xv){$r[$xk]=parsed($xv,$v);}; return $r;};
      if(!$w&&($s===2)&&($x[0]==='\\')){$x=$x[1]; return "\{$x}";}; if($w==='\\\\'){return char($x);};  if($w!=='()'){return $x;};
      if(wrapOf($x)==='``'){$y=unwrap($x); if(isPath($y)){$x=$y;}};
      if(isPath($x)){if(!exists($x)){return;}; $r=durl($x); if(!$r){$r=padded(keys(path::scan($x,TRON)),"$x/",'');}; return $r;};
      if(!verify($x,'/^[a-zA-Z0-9_`\&\?\:\(\)\|\.\s]{1,144}$/')){return "($x)";}; $o=frag('()&?:|'); $u=frag('₍₎﹠﹖﹕｜');
      $l=expose($x,'`','`'); if($l){foreach($l as $f){$r=swap($f,$o,$u);$x=swap($x,"`$f`","`$r`");}}; if(!isTron($v)){$v=tron($v);};
      unset($f,$r); $l=expose($x,'(',')'); if($l){foreach($l as $f){$r=parsed("($f)",$v); $x=swap($x,"($f)",$r);}};
      $vget=function($k,$v,$o,$u){$k=trim($k); if(wrapOf($k)==='``'){return swap(unwrap($k),$u,$o);}; $r=durl($k); if($r){return $r;};
      if(isWord($k)&&isUpperCase($k)){return defn($k);}; $k=swap($k,'.','/'); $r=bore($v,$k); if($r===null){$r=vars($k);};
      if($r===null){$r=parsed($k,$v);}; return $r;}; $r=null; $er='encapsulation required';
      $p=stub($x,'&'); if($p){if(isin($x,['?',':','|'])){fail($er);}; return ($vget($p[0],$v,$o,$u)&&$vget($p[2],$v,$o,$u));};
      $p=stub($x,'?'); if($p){$c=$vget($p[0],$v,$o,$u); $x=trim($p[2]); if(!isin($x,':')){fail('invalid terinary');};
      $p=stub($x,':'); $t=$vget($p[0],$v,$o,$u); $f=$vget($p[2],$v,$o,$u); return ($c?$t:$f);};
      if(!isin($x,'|')){return $vget($x,$v,$o,$u);}; $l=frag($x,'|'); unset($i);
      foreach($l as $i){if(isin($i,['&','?',':'])){fail($er);}; $r=$vget($i,$v,$o,$u); if(!isVoid($r)&&($r!==0)){return $r;};};
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: texted : turn value into plain text
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function texted($d,$t=true)
   {
      if(isText($d)){if($t){$d=trim($d);}; return $d;}; if($d===NULL){return 'null';}; if(isBool($d)){return ($d?'true':'false');};
      if(isNumr($d)){return "$d";}; if(isList($d)||isTron($d)){$r=encode::jso($d);}else{$r=print_r($d,true);}; if($t){$r=trim($r);};
      return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: relate : reference
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function relate($n,$d)
   {
      expect::{'text:1'}($n); expect::{'text:1'}($d);
      return target(PROCDATA)->ensure([using=>'refs',where=>"name = $n",claim=>[[$n,$d]]]);
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: target : plug driver interaction
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function target($d,$r=null)
   {
      expect::{'text:1'}($d); $h=HOSTNAME;
      // if(!$r){$r=target(PROCDATA,1)->select([using=>'refs',fetch=>'dest',where=>"name = $d"]);if(span($r)>0){$d=$r[0]->dest;}};
      $x=path::info($d); $o=$x->plug; $p=$x->path;
      if((($x->type==='git')&&isin($o,'http'))||(($o==='file')&&($x->type==='fldr')&&exists("$p/.git"))){$o='git';}; $c="Anon\\{$o}_plug";
      if(!isTool($c))
      {
         $p=path("/c0r3/lib/plug/$o.php"); $f=(!file_exists($p)?'undefined':(!is_readable($p)?'forbidden':(!is_file($p)?'not a file':null)));
         if($f){fail("plug adapter for `$o` is: $f");}; ob_start(); require($p); $t=ob_get_clean();
         if(!isTool($c)){fail("expecting class `$c` in: `$p`");};
      };
      $inst=(new $c($x)); return $inst;
   }

   defn('count fetch using alter write claim where group order limit parse shape purge');
   defn('NATIVE REMOTE');

   function within($d,$r=null){return target($d,$r);}

   function isRepo($p)
   {
      expect::path($p); if(!isFold($p)){return false;}; try{$r=exec::{"git branch"}($p);}catch(\Exception $e){return false;}; return true;
   };
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: conf : shorthand for configuration read .. saves result to vars
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function conf($x)
   {
      if(!isText($x,3)||(strpos($x,'/')===false)){fail('expecting :stem/table:');}; $x=trim($x,'/');
      if(vars::exists($x)){return vars::select($x);}; $y=explode('/',$x); $d=$y[0]; $t=$y[1]; $h=path("/$d");
      $l=['var/data/conf','conf','.auto/var/data/conf','.auto/conf']; $p=null;
      foreach($l as $i){if(isFile("$h/$i/base.sdb")||isFile("$h/$i/cols.php")){$p="$h/$i"; break;};};if(!$p){fail("no conf defined in `$x`");};
      $r=target("sqlite::$p")->select([using=>$t, fetch=>['name','valu'], shape=>'name:valu']); vars::create($x,$r); return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: encode/decode : encoding/decoding
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class encode
   {
      static function nbx($d,$b)
      {
         if($b===64){if(!isText($d)){$d=texted($d);}; return base64_encode($d);};
         if(!isNumr($d)||isin($d,'.')||($d<1)){fail::base_convert('expecting positive integer');};
         if(!is_int($b)||(($b%2)!==0)||($b>62)){fail::base_convert("invalid encoding base");};
         return gmp_strval(gmp_init($d,10),$b);
      }

      static function jso($d,$v=null)
      {return json_encode($d,JSON_UNESCAPED_SLASHES);}

      static function __callStatic($n,$a)
      {
         if(strlen($n)<1){fail::reference('invalid method name');}; if(!isset($a[0])){$a[0]=null;}; if(!isset($a[1])){$a[1]=null;};
         $f=(($n==='hex')?'b16':(($n==='json')?'jso':(($n==='cfg')?'vmp':$n))); if(locate(__CLASS__,$f)){return self::{$f}($a[0],$a[1]);};
         $b=null; if($f[0]==='b'){$b=substr($f,1); $b=(is_numeric($b)?($b*1):null); if(!is_int($b)){$b=null;};};
         if($b){return self::nbx($a[0],$b);}; $f=swap($f,' ',''); $l=frag($f,'->'); if(span($l)<2){fail::reference("invalid method `$f`");};
         $r=$a[0]; foreach($l as $i){$r=encode::{$i}($r,$a[1]);}; return $r;
      }
   }


   class decode
   {
      static function nbx($d,$b)
      {
         $v=(isNumr($d)?"$d":$d); if(!isText($v)){fail::arguments('expecting 1st arg as :text: or :numr:');};
         if(!is_int($b)||(($b%2)!==0)){fail::base("invalid encoding base");};
         if($b===16){return hex2bin($v);}; if($b===64){return base64_decode($v);}; return hex2bin(gmp_strval(gmp_init($v,$b),16));
      }

      static function jso($d,$v=null)
      {
         if(isPath($d)){$p=expect::{'path:R'}($d); $t=file_get_contents($p);}else{$t=$d;}; $o=wrapOf($t);
         $r=json_decode($t); if($o==='{}'){$r=tron($r);}; return $r;
      }

      static function __callStatic($n,$a)
      {
         if(strlen($n)<1){fail::reference('invalid method name');}; if(!isset($a[0])){$a[0]=null;}; if(!isset($a[1])){$a[1]=null;};
         $f=(($n==='hex')?'b16':(($n==='json')?'jso':(($n==='cfg')?'vmp':$n))); if(locate(__CLASS__,$f)){self::{$f}($a[0],$a[1]);};
         $b=null; if($f[0]==='b'){$b=substr($f,1); $b=(is_numeric($b)?($b*1):null); if(!is_int($b)){$b=null;};};
         if($b){return self::nbx($a[0],$b);}; fail::reference("invalid method `$f`");
      }
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: cookie :
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class cookie
   {
      static function create($n,$v=null,$e=0,$p='/')
      {
         if(isScal($n)||isTron($n)){$e=($v?$v:0); $p=($e?$e:'/'); unset($v); foreach($n as $k => $v){self::create($k,$v,$e,$p);}; return;};
         if(!isWord($n)){fail('expecting :word:');}; $v=(($v===null)?$v:encode::b64(encode::jso($v))); return setrawcookie($n,$v,$e,$p);
      }

      static function exists($n)
      {if(!isWord($n)){fail('expecting :word:');}; return isset($_COOKIE[$n]);}

      static function select()
      {
         $a=func_get_args(); $s=span($a); $r=tron(); foreach($a as $n)
         {
            if(!isWord($n)){fail('expecting :word:');}; if(!isset($_COOKIE[$n])){return;}; $v=str_replace(' ','+',$_COOKIE[$n]);
            $t=decode::b64($v); if($t!==false){$v=$t;}; $r->$n=decode::jso($v);
         };
         if($s===1){$r=vals($r)[0];}; return $r;
      }

      static function delete($n,$p='/')
      {if(isTupl($n)){foreach($n as $k){self::delete($k,$p);}; return;}; return self::create($n,null,-1,$p);}
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: dbug : gracefully handles fails and dumps, according to interface
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class dbug
   {
      static $meta=['stak'=>null,'path'=>null,'line'=>null];


      private static $code = array
      (
         0=>'Usage', 1=>'Fatal', 2=>'Warning', 4=>'Parse', 8=>'Notice', 16=>'Core', 32=>'Warning', 64=>'Compile',
         128=>'Warning', 256=>'Coding', 512=>'Warning', 1024=>'Notice',2048=>'Strict',4096=>'Recoverable',
         8192=>'Deprecated', 16384=>'Deprecated'
      );


      static function stak($n=null,$x=null)
      {
         $s=self::$meta['stak']; if(!$s){$e=(new \Exception); $s=$e->getTraceAsString();}; $s=explode("\n",$s); $b=[]; $r=[];
         foreach($s as $i)
         {
            if(!strpos($i,'.php(')){continue;}; $y=explode('.php(',$i); $p=crop($y[0]); $p=(explode(' ',$p)[1].'.php'); $y=$y[1];
            $y=explode('): ',$y); $l=($y[0]*1); $y=crop($y[1]); $y=explode('(',$y); $f=$y[0];
            if(($p=='$.php')||(in_array($f,['{closure}','call_user_func_array','dbug::stak']))){continue;};
            $b[]=json_decode(json_encode(['func'=>$f,'path'=>$p,'line'=>$l])); $y=null;
         };
         if(($n===null)&&($x===null)){return $b;}; $y=0;
         foreach($b as $i => $o){if(($i===$x)||($o->func===$n)){$y=1; continue;}; if($y){$r[]=$o;};}; if(count($r)>0){return $r;}; return $b;
      }


      static function name($d=0)
      {
         if(!is_int($d)){$d=0;}; if(isset(self::$code[$d])){return self::$code[$d];};
         $o=conf('c0r3/stat'); if(isset($o[$d])){return $o[$d];}; return self::$code[0]; if($n===null){$n=0;};
      }

      static function view($i,$rc=null,$rt=null)
      {
         if(!isTron($i)){$i=tron($i);}; $fc=INTRFACE; $i->INTRFACE=INTRFACE; $u=vars('permit'); $dp=substr(envi('DBUGPATH'),2);
         $i->USERINFO=$u; $d=json_encode($i,JSON_UNESCAPED_SLASHES);if(!headers_sent()){header_remove();};if(ob_get_level()){ob_end_clean();};
         if(!$rc){if($i->DBUGMODE==='fail'){$rc=500; $rt='Internal Server Error';}else{$rc=418; $rt="I'm a teapot";};};
         if(($fc==='DPI')&&(NAVIPATH===$dp)){$rc=200; $rt='OK';};
         header("HTTP/1.1 $rc $rt"); if($fc==='BOT'){done(0);}; if((($fc==='DPI')&&(NAVIPATH!==$dp))||($fc==='API')){echo $d; done(0);};
         $d=base64_encode($d); $h=file_get_contents(path($dp)); $h=str_replace('{:DBUGDATA:}',$d,$h); $b='<!--['; $e=']-->'; $l=expose($h,$b,$e);
         if($l){foreach($l as $p){$f="{$b}{$p}{$e}";$p=path(trim($p));if($p&&is_readable($p)){$h=str_replace($f,file_get_contents($p),$h);};};};
         header('Content-Type: text/html; charset=utf-8'); echo $h; done(0); exit;
      }

      static function fail($m,$n="0",$s=null,$y=null)
      {
         if(!$m){return;}; if(ob_get_level()){ob_get_clean();}; if(defined('HALT')||defined(':PROCEXIT:')){if(isset($_SERVER['FAIL']))
         {self::view($_SERVER['FAIL']);}; exit;}; define('HALT',1); ob_start(); $h=((is_numeric($n))?self::name(($n*1)):$n);
         $h=str_replace(['Error','error'],':',$h); $h=ucwords($h);
         $m=str_replace('call_user_func_array() expects parameter 1 to be a valid callback, ','',$m);
         if((strpos($m,'Missing argument ')===0)&&(strpos($m,', called in'))){$m=explode(', called in',$m)[0];}; $m="{$h}Error: $m";
         $x=self::stak($y,$s,1);  $f=self::$meta['path']; $l=self::$meta['line']; if(!$f||!$l){$f=$x[0]->path; $l=$x[0]->line;};
         $r=['DBUGMESG'=>crop($m), 'DBUGFILE'=>crop($f), 'DBUGLINE'=>$l, 'DBUGMODE'=>'fail', 'STACKLOG'=>$x]; $_SERVER['FAIL']=$r;
         self::view($r); exit;
      }


      static function time()
      {
         $t=microtime(); $p=explode(' ',$t); $p=sprintf('%d%03d',$p[1],($p[0]*1000)); return ($p/1000);
      }


      static function dump()
      {
         $a=func_get_args(); $x=self::stak(null,null,1); if(isset($x[0])){$f=$x[0]->path; $l=$x[0]->line;};
         $c=conf('c0r3/boot'); $m="\ndbug::dump   $f   ($l)\n\n"; $b=dupe('-',56); $e=dupe('=',56);
         foreach($a as $i){$t=type($i); $s=span($i); $d=trim(print_r($i,true));  $m.="\n$t ($s)\n$b\n$d\n$e\n\n";};
         $ts=round((envi('REQUEST_TIME_FLOAT')*1),3); $tf=self::time(); $td=round(($tf-$ts),3); $m.=("\nelapsed: $td");
         self::view(['DBUGMESG'=>crop($m), 'DBUGFILE'=>crop($f), 'DBUGLINE'=>$l, 'DBUGMODE'=>'dump', 'STACKLOG'=>$x]); exit;
      }


      static function make($p)
      {
         if(!is_string($p)||!$p||!is_dir($p)){fail('expecting existing folder path');}; $r=getcwd(); $c="$r/.auto";
         if(strpos($p,' ')!==false){fail::Reference("path-name contains space in: `$p`");};
         $o=[]; $l=array_diff(scandir($p),array('.','..')); foreach($l as $i)
         {
            if(strpos($i,' ')!==false){fail::Reference("path-name contains space in: `$i`");};
            if(is_dir("$p/$i")){$x=self::make("$p/$i");}else{$x=1;}; $k=str_replace([$c,$r],['$',''],"$p/$i"); $o[$k]=$x;
         };
         $o=json_decode(json_encode($o)); return $o;
      }


      // static function test($o)
      // {
      //    if(!is_object($o)){fail('expecting object');}; $r=getcwd(); $c="$r/.auto"; foreach($o as $k => $v)
      //    {
      //       $p=str_replace([$c,$r],['$',''],$k); $p=(($p[0]==='/')?"$r{$p}":(($p[0]==='$')?($c.substr($p,1)):$p));
      //       $w=(is_object($v)?'folder':'file'); if(!is_readable($p)){fail::Dependency("expecting `$p` as readable $w");};
      //       if(strpos($p,'/tmp')&&is_dir($p)&&!is_writable($p)){fail::Permission("expecting `$p` as writable folder");};
      //       if(is_dir($p)||!strpos($p,'.')){continue;}; $x=array_pop((explode('.',$p))); if($x!=='inf'){continue;};
      //       if(!is_writable($p)){fail::Permission("expecting `$p` as writable file");};
      //    };
      // }
      //
      //
      // static function init()
      // {
      //    $di=isee('inf/dbug.inf'); $pi=isee('inf/path.inf'); if(!$di||!$pi){fail('framework dependency issue');};
      //    $l=trim(file_get_contents($di)); if(!is_numeric($l)){$l=0;}; $l=($l*1); $c=time(); $d=($c-$l); if($d<3600){return;};
      //    if(!is_writable($di)||!is_writable($pi)){fail('invalid framework permission');}; $s=json_decode(trim(file_get_contents($pi)));
      //    if(!is_object($s)){$s=self::make(getcwd().'/.auto'); file_put_contents($pi,json_encode($s,JSON_UNESCAPED_SLASHES));}; self::test($s);
      // }
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class fail
   {
      static function __callStatic($n,$a)
      {
         $m=(isset($a[0])?$a[0]:null); $s=(isset($a[1])?$a[1]:null); $y=(isset($a[2])?$a[2]:'dbug::fail');
         if(!dbug::$meta['stak']){$e=(new \Exception); dbug::$meta['stak']=$e->getTraceAsString();}; dbug::fail($m,$n,$s,$y);
      }
   };

   function dump()
   {
      $a=func_get_args(); call_user_func_array(['Anon\\dbug','dump'],$a);
   };
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: done : exit gracefully
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function done($d=null)
   {
      if(defined(':PROCEXIT:')||($d===0)){define(':PROCEXIT:',1); if(ob_get_level()){flush();}; exit;}; define(':PROCEXIT:',1); $f="";
      if((strpos($d,'/')!==false)){if(ob_get_level()){ob_get_clean();};header("Location: $d");exit;};$n=NAVIPATH;$f="The `$n` request-process";
      if(strlen(trim((ob_get_contents().'')))<1){dbug::fail("$f resulted in a BSOD .. (blank screen of death)");};
      if(!vars('render')->stat){dbug::fail("$f shorted out");}else{exit;}; ob_end_flush(); flush(); exit;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: exec : run server command .. returns output -or null if invalid
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class exec
   {
      static function __callStatic($c,$a)
      {
         $un=USERNAME; $up="/w0rk/user/$un"; // TODO security check
         if(!isset($a[0])){$a[0]=$up;}; $p=$a[0]; $v=(isset($a[1])?$a[1]:null);$i=(isset($a[2])?$a[2]:'');expect::text($i);$p=expect::path($p);
         if(!isNull($v)){expect::scal($v);}; $q=[0=>["pipe","r"], 1=>["pipe","w"], 2=>["pipe","w"]]; $r=proc_open($c,$q,$x,$p,$v);
         if(!is_resource($r)){return;}; if($i&&($i!==NOFAIL)){wait(1000); fwrite($x[0],$i);}; fclose($x[0]);
         $o=trim(stream_get_contents($x[1])); fclose($x[1]); $e=trim(stream_get_contents($x[2])); fclose($x[2]); $z=trim(proc_close($r));
         if($z){$z=(($e&&$o)?"$e ..\n$o":($e?$e:$o));}; if(!$z){return $o;}; throw new \Exception("$z");
      }
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: ping : returns object of ping info, or null if unreachable
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function ping($h)
   {
      expect::{'text:5'}($h); try{$l=exec::{"ping -W 3 -c 1 $h"}();}catch(\Exception $e){return;};
      $l=trim(stub(stub($l,"\n")[2],"\n\n")[0]); $p=stub($l,'bytes from '); if(!$p){return;}; $p=stub($p[2],': '); $a=trim($p[0]);
      if(isin($a,'(')){$a=expose($a,'(',')')[0];}; $t=trim(stub(stub($p[2],'time=')[2],' ')[0]); $r=tron(['addr'=>$a,'time'=>($t*1)]);
      return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: online : check if domain is available .. if none is given then it is implied if `self` is online
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function online($h=null)
   {
      if($h===null){$h='example.com';}; $r=ping($h); return ($r?true:false);
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: permit
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function permit($v=null)
   {
      expect::{'scal tron'}($v); if(!isTool('permit')){import('/c0r3/lib/core/permit.php');};
      foreach($v as $k => $v){$k=unwrap($k); return permit::{$k}($v);};
   }

   defn('auth mode role user rank rate');
# ---------------------------------------------------------------------------------------------------------------------------------------------



# clan :: module : object
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class module
   {
      function __construct($d=null)
      {
         if(!isScal($d)){return $this;}; foreach($d as $k => $v)
         {$k=trim($k); if(strlen($k)<1){continue;}; if(isScal($v)){$v=tron($v);}; $this->$k=$v;};
      }

      function __get($k){return null;}
      function __call($k,$a){if(property_exists($this,$k)){return call_user_func_array($this->$k,$a);};}
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: import
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function import($d,$v=null,$NOINIT=false)
   {
      if(isText($d,5)&&(substr($d,0,5)==='Anon\\')){$d=substr($d,5);}; $p=expect('path:R')->from($d); $q=$d; $javu=0;
      if(isPath($q))
      {
         $javu=sha1($q); $deja=vars($javu); if($deja){return $deja;}
      };
      $w=isWord($d); if($w&&isTool($d)){return "Anon\\$d";}; $x=path::type($p); if($x==='fldr')
      {$i=path::indx($p); if($i){$p="$p/$i";}elseif(exists("$p/.auto")){$p=expect::{'path:R,F'}("$p/.auto/aard.php");}; $x=path::type($p);};

      if($x==='php')
      {
         if(!isTron($v)){$v=tron($v);}; $x=call_user_func_array(function($_PATH,$_VARS)
         {
            foreach($_VARS as $k => $v){$$k=$v;}; unset($k,$v); ob_start(); require($_PATH); $l=get_defined_vars(); $r=trim(ob_get_clean());
            foreach($l as $k => $v){if(($k=='_PATH')||($k=='_VARS')||property_exists($_VARS,$k)){unset($l[$k]);};}; return ['V'=>$l,'T'=>$r];
         },[$p,$v]);
         if($x['T']){return $x['T'];}; $c="Anon\\$d"; if(!isTool($c)){$c=null;};
         if(isset($x['V']['export'])){$r=$x['V']['export']; if(isTool($r)&&!$c){$c="Anon\\$r"; unset($x['V']['export']);}else{return $r;};};
         if(!$c){$c=path::twig($p); if($c!='/'){$c=path::leaf($c);}else{$c=path::leaf($p,0);}; if(isTool($c)){$c="Anon\\$c";}else{$c=null;};};
         if($c){if(isin($c,'meta')&&!isTron($c::$meta)){$c::$meta=tron();};if(isin($c,'init')&&isFunc("$c::init"))
         {$p=NAVIPATH; $n=swap($c,'Anon\\',''); $a=[]; if(isin(trim($p,'/'),"$n/")){$g=path::goal($p);$a=$g->args;};
         if(!$NOINIT){$r=call("$n::init",$a);if($r){$c=$r;};};};
         vars::create(["$javu"=>$c]);
         return $c;};
         if(span($x['V'])>0){return (new module($x['V']));}; return true;
      };

      $r=path::scan($p); if($x==='fldr'){return $r;};

      if(isin(['md','htm','html','xml'],$x))
      {
         unset($i); $y=tron(['css'=>'style','es6'=>'script','jsm'=>'script','jsc'=>'script','js'=>'script']);
         $l=expose($r,'<!--[ ',' ]-->'); if(!$l){$l=[];}; foreach($l as $i)
         {
            $fnd="<!--[ $i ]-->"; $rpl=''; $lst=explode(',',$i); foreach($lst as $itm)
            {
               $p=path(trim($itm)); if(!$p){continue;}; $t=path::type($p); $n=$y->$t; $z=import($p,$v);
               if($n){$rpl.="<$n>$z</$n>";}else{$rpl.=$z;};
            };
            $r=str_replace($fnd,$rpl,$r);
         };
      };

      $b='{:'; $e=':}'; unset($i); if(isin($r,$b)&&isin($r,$e))
      {
         $l=expose($r,$b,$e); foreach($l as $i){$z=texted(parsed($i,$v)); $r=swap($r,"{$b}{$i}{$e}",$z);};
      };
      if(isin(['jso','json'],$x)){return decode::{$x}($r,$v);};

      return $r;
   };
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: requires
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function requires($_PATH)
   {
      if(isWord($_PATH)){if(!extension_loaded($_PATH))
      {fail("the `$_PATH` extension is required; make sure it is installed and configured, or contact your hosting provider");}; return true;};

      $_PATH=path($_PATH); if(!$_PATH||!is_readable($_PATH)||!is_file($_PATH)||(path::type($_PATH)!=='php'))
      {fail('expecting readable php file-path');}; ob_start(); require_once "$_PATH"; $r=trim(ob_get_clean());
      if(!$r){$r=true;}; return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# tool :: render : output
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class render
   {
      static function header($d)
      {
         if(isPath($d))
         {
            $c=path::code($d); if($c!==200){self::status($c); return;}; $t=path::type($d);
            $m=((($t==='fldr')||($t==='none'))?USERMIME:conf('c0r3/mime')->$t); $s=path::size($d);
            $d=['Content-Type'=>$m,'Content-Length'=>$s,'Content-Range'=>0,'Render-Type'=>$t];
         };

         if(defined('HALT')||headers_sent()||!isScal($d)||vars('render')->body){return;}; if(ob_get_level()){ob_get_clean();};
         if(!isset($d['Status'])){$d['Status']=200;};$c=$d['Status'];$m=conf('c0r3/stat')->$c;if(!$m){fail::reference('invalid HTTP status-code');};
         if(!vars('render')->stat){vars('render')->stat=$c; header("HTTP/1.1 $c $m");}; unset($d['Status']);
         if(APIFACED){$d['Access-Control-Allow-Origin']=CORSFROM;}; if(GUIFACED){$d['X-Frame-Options']='SAMEORIGIN';};
         foreach($d as $k => $v)
         {
            if($k==='Content-Type'){vars('render')->mime=$v;}; if($k==='Content-Length'){vars('render')->size=$v;};
            header("$k: $v");
         };
         return true;
      }


      static function status($c,$m=DONE)
      {
         expect::numr($c); $t=conf('c0r3/stat')->$c; if(!$t){fail::reference('invalid status code');};
         if($m===null){self::header(['Status'=>$c]); return;};
         if(!GUIFACED){self::header(['Status'=>$c]); self::header(['Content-Type'=>USERMIME,'Content-Length'=>0]); done(0);};

         if(GUIFACED)
         {
            // $r=path::scan(STATPATH); $v=encode::b64(encode::json(['code'=>$c,'text'=>$t])); $r=swap($r,'{:HOSTVARS:}',);
            $r=path::scan(STATPATH); $v=encode::{'json -> b64'}(['code'=>$c,'text'=>$t]); $r=swap($r,'{:HOSTVARS:}',$v);
            self::header(['Content-Type'=>'text/html','Content-Length'=>span($r)]); echo $r; done(0);
         };

         if(APIFACED)
         {
            $x=stub(USERMIME,'/')[1]; $r=encode::{$x}(tron(['data'=>$m])); $s=span($r);
            self::header(['Content-Type'=>USERMIME,'Content-Length'=>$s]); echo $r; done(0);
         };
      }


      static function parsed($d,$v=null)
      {
         $p=expect::{'path:R'}($d); $a=crop($p); $x=path::type($a); if(!isTron($v)){$v=tron($v);}; $dp=DBUGPATH;

         if($a===$dp)
         {$i=cookie::select('dbugInfo'); if(!isTron($i)){$i=tron();}; if(!$i->DBUGMODE){$i->DBUGMODE='fail';}; dbug::view($i);}

         if(APIFACED)
         {
            if($d==$dp){self::status(503,DONE);}; if($d==STATPATH){self::status($c,DONE);};
            self::status(503,DONE); return; // TODO - render parsed files to API !!
         };

         if(DPIFACED)
         {
            if((($d===$dp)||($d===STATPATH))&&!isin(['html','htm','php'],path::type(NAVIPATH))){self::status((($d==$dp)?500:$c),DONE);};
            $l=['html','htm','xml','php','cfg','txt','fldr','md','js','jsc','ejs','jsm','json','jso'];
            if(isin($l,$x)){$r=import($p,$v);}else{$r=$p;}; self::direct($r,$x); return;
         };

         if(GUIFACED)
         {
            $r=path::scan(AUTOPATH); $t=cookie::select('GUITOKEN'); if(!$t){$t=vars('procID');}; $i=cookie::select('dbugInfo');
            if(!isTron($i)){$i=tron();}; $i=fuse($i,['SITEMODE'=>SITEMODE]); $u=vars('permit'); $q=envi('QUERY_STRING');
            cookie::create(['dbugPath'=>DBUGPATH,'dishPath'=>(rtrim(path::mini($p),'.php')."?$q"),'dbugInfo'=>$i,'USERINFO'=>$u]);
            cookie::create(['TETHERID'=>TETHERID]);
            self::direct($r,'html'); return;
         };

         if(BOTFACED)
         {
            if($d==DBUGPATH){self::status(503,DONE);}; if($d==STATPATH){self::status($c,DONE);};
            self::status(503,DONE); return; // TODO !!
            // $r=import::{'$/htm/bots.htm'}($v);
         };

         if(SCIFACED){echo "render::parsed(`$a`); ... why?\n"; done(0);};
      }


      static function direct($d,$x=null)
      {
         if(isPath($d)){$p=expect::{'path:R,F'}($d); if(!vars('render')->stat){self::header($p);}; readfile($p); done(0);}

         if($x==='php'){$x=null;}; if($x){$r=$d;}
         else
         {
            $r=texted((isText($d)?trim($d):$d),1);
            $tl=['</head','</body','</span','</div','</a','</i','</b','</h1','</h2','</h3','<br','<img','<link','</script','</style'];
            $w=wrapOf($r); if(locate(['{}','[]'],$w)){$x='json';}elseif(($w==='<>')&&strpos($r,'</'))
            {$x=(locate($r,$tl)?'html':(locate($r,['<svg','</svg>'],XACT)?'svg':'xml'));}else{$x='txt';};
         }

         $m=conf('c0r3/mime')->$x; $s=strlen($r); if(!$m){fail::reference("invalid mime-type");};
         self::header(['Content-Type'=>$m,'Content-Length'=>$s,'Content-Range'=>0,'Render-Type'=>$x]); echo $r; done(0);
      }
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# defn :: words/vars : based on config, interface, request .. debug input for naming conflicts
# ---------------------------------------------------------------------------------------------------------------------------------------------
   $l=['GET'=>$_GET,'POST'=>$_POST,'FILES'=>$_FILES,'UPLOAD'=>[]]; $d=tron(); $s=file_get_contents('php://input');
   if(wrapOf($s)==='{}'){$s=decode::jso($s); $l['UPLOAD']=(isTron($s)?$s:['STREAM'=>$s]);}; foreach($l as $x => $i)
   {foreach($i as $k => $v){if(!property_exists($d,$k)){$d->$k=(isText($v)?parsed($v):$v);}else{fail("duplicate key `$k` in $x");};};};
   unset($l,$i,$k,$v);

   vars::create
   ([
      'header'=>tron(swap($_SERVER,'HTTP_','')),'client'=>$d,'stream'=>$s,'procID'=>(ipv6(USERADDR).'|'.getmypid().'|'.BOOTTIME),
      'render'=>tron(),
      'chmods'=>tron(['RF'=>gmod(__file__),'WF'=>gmod('/c0r3/var/info/dbug.inf'),'RD'=>gmod(COREPATH),'WD'=>gmod('/c0r3/var')]),
   ]);

   define('PROCHASH',hashed(vars('procID'))); $tid=cookie::select('TETHERID'); if(!$tid){$tid=PROCHASH;}; define('TETHERID',$tid); unset($tid);
   $u=cookie::select('USERINFO'); $a=AUTHDATA; $q=((envi('INTRFACE')==='BOT')?'webcrawler':'anonymous'); $hack=0;
   $fu=[using=>"user",fetch=>["name","mail","pass","rank"],where=>"name = '$q'"];
   if($u&&!$u->hash){$hack=1;}elseif($u)
   {
      $ch=$u->hash; $fu[where]="mail = '$u->mail'"; $u=target($a)->select($fu); if(!isset($u[0])){$hack=1;}
      else{$u=$u[0]; $dh=hashed($u); if($ch!==$dh){$hack=1;}else{$u->hash=$dh;}; unset($u->pass);};
   };
   if($hack){cookie::delete('USERINFO'); echo('insert 50c and try again'); done(0);}; $fu[where]="name = '$q'";
   if(!$u){$u=target($a)->select($fu)[0]; $u->hash=hashed($u); unset($u->pass);}; unset($ud,$ch,$dh,$hack);

   $u->role=target("sqlite::/w0rk/user/$u->name/data")->select([using=>"clan",fetch=>"role",shape=>"[role]"]);

   vars::create
   ([
      'permit'=>$u,
   ]);


   define('USERNAME',$u->name); define('USERMAIL',$u->mail); define('USERTYPE',implode(' ',$u->role)); $i=envi('INTRFACE');
   unset($d,$s,$u); $c=conf('c0r3/boot'); foreach($c as $k => $v){if(($k==='INTRFACE')||isList($v)||isTron($v)){continue;}; define($k,$v);};
   $p=vars('permit')->path; if($p&&$p->face&&($r>=$p->rank)&&(!$p->mode?1:(SITEMODE===$p->mode))){$i=$p->face;$_SERVER['INTERFACE']=$i;};
   $l=frag('API BOT GUI SCI DPI TPI',' '); foreach($l as $z){define("{$z}FACED",(($z==$i)?true:false));};define('INTRFACE',$i);unset($l,$i,$z);
   $l=['DEVL','LIVE','DEMO','DOWN']; foreach($l as $i){define("{$i}MODE",(($i==SITEMODE)?true:false));}; unset($l,$i,$c,$r,$k,$v);
   define('SEIFACED',((APIFACED&&(envi('ACCEPT')==='text/event-stream'))?true:false));
# ---------------------------------------------------------------------------------------------------------------------------------------------



# defn :: event : handlers
# ---------------------------------------------------------------------------------------------------------------------------------------------
   set_exception_handler(function($e)
   {
      dbug::$meta['stak']=$e->getTraceAsString(); dbug::$meta['path']=$e->getFile(); dbug::$meta['line']=$e->getLine();
      if(ob_get_level()){ob_get_clean();}; dbug::fail($e->getMessage(),$e->getCode()); exit;
   });

   set_error_handler(function()
   {
      if(isset($_SERVER['nofail'])&&$_SERVER['nofail']){return;};
      $c=vars('permit')->path; if($c&&($c->face==='TPI')&&($c->dbug!==true)){return;}; $b='';
      if(ob_get_level()){$b=("\n\n".ob_get_clean());};
      $e=func_get_args();
      $x=(new \Exception); dbug::$meta['stak']=$x->getTraceAsString();
      dbug::$meta['path']=$e[2]; dbug::$meta['line']=$e[3]; dbug::fail(($e[1].$b),$e[0]); exit;
   });

   register_shutdown_function(function()
   {
      if((defn('INTRFACE')==='TPI')||defined('HALT')||defined(':PROCEXIT:')){flush(); exit;};
      $e=error_get_last(); if($e===null){done();};
      $x=(new \Exception); dbug::$meta['stak']=$x->getTraceAsString(); dbug::$meta['path']=$e['file']; dbug::$meta['line']=$e['line'];
      if(ob_get_level()){ob_get_clean();}; dbug::fail($e['message'],$e['type']); exit;
   });

   spl_autoload_register(function($n){import($n);});
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: User : shorthand
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function User($u,$o=null)
   {
      if(!isWord($u)){fail('invalid username');}; if(($o!==null)&&(!isWord($o))){fail('invalid property-name');}; $ad=AUTHDATA;
      if($u===USERNAME){$r=vars('permit'); if($o===null){return $r;}; return $r->$o;};
      $r=target($ad)->select([using=>"user",fetch=>["name","mail","pass","rank"],where=>"name = '$u'"]); if(span($r)<1){return;}; $r=$r[0];
      $p="/w0rk/user/$u/data"; if(!exists($p)){fail("user folder for `$u` is missing");};
      $r->role=target("sqlite::$p")->select([using=>"clan",fetch=>"role",shape=>"[role]"]);
      if($o===null){return $r;}; return $r->$o;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: random : creates random string from ALPHABET .. $s is the char-span
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function random($s=null)
   {
      if(!is_int($s)){$s=6;}; $r=str_shuffle(ALPHABET); $r=substr($r,0,$s); return $r;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: reckon : assert on property values by using string as expression .. for use in `where` crud-filters that don't use database
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function reckon($expr,$vars)
   {
      expect::text($expr); expect::tron($vars); $oper=padded((explode(' ',EXPROPER)),' '); $p=stub($expr,$oper);
      if(!$p){fail("invalid expression `$expr`");}; $l=trim($p[0]); $l=$vars->$l; $o=trim($p[1]); $r=$p[2]; $r=dval($r);
      if($o==='!='){return ($l!==$r);}; if($o==='<='){return ($l<=$r);}; if($o==='>='){return ($l>=$r);}; if($o==='='){return ($l===$r);};
      if($o==='<'){return ($l<$r);}; if($o==='>'){return ($l>$r);}; if(!isin($o,'~')||!isin($r,'*')){return;}; $f=locate($l,$r);
      if($o==='~'){return ($f?true:false);}; return (!$f?true:false);
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



# func :: filter : whatever
# ---------------------------------------------------------------------------------------------------------------------------------------------
   function filter($v,$f)
   {
      $fnc=0; $tst=0; $loc=0; if(isFunc($f)){$fnc=1;}elseif(isText($f)){if(wrapOf($f)==='//'){$tst=1;}elseif(isin($f,'*')){$loc=1;}};
      if(!$fnc&&!$tst&&!$loc){return $v;};

      if(isList($v))
      {
         $r=[]; foreach($v as $i)
         {
            if($fnc){$i=call($f,$i); if($i!==null){$r[]=$i;}; continue;};
            if($tst){$x=test($i,$f); if($x){$r[]=$i;}; continue;};
            if($loc){$n=0; if($f[0]==='!'){$n=1; $f=substr($f,1);}; $x=locate($i,$f); if((!$n&&$x)||($n&&!$x)){$r[]=$i;}; continue;};
         };
         return $r;
      };

      return $v;
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------




# tool :: emit : events
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class emit
   {
      static function __callStatic($n,$a)
      {
         ladd($a,$n); $r=call('events::signal',$a); return $r;
      }
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------
