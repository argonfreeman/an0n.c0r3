<?
namespace Anon;
if(!isset($tool)||!isTron($tool)){$tool=tron();};
?>

<table id="devlTool{:TOOLNAME:}" class="devlToolPanl">
   <tr>
      <td class="devlSideHead">
         <div class="holder">
            <?=$tool->sideHead?>
         </div>
      </td>
      <td class="devlVertDlim"><div></div></td>
      <td class="devlMainHead">
         <div class="holder">
            <?=$tool->mainHead?>
         </div>
      </td>
   </tr>
   <tr>
      <td class="devlHorzLine"><div></div></td>
      <td class="devlVertDlim"><div></div></td>
      <td class="devlHorzLine"><div></div></td>
   </tr>
   <tr>
      <td class="devlSideView">
         <div class="devlSidePanl">
            <?=$tool->sideView?>
         </div>
      </td>
      <td class="devlVertDlim"><div></div></td>
      <td class="devlMainView">
         <div class="devlMainPanl">
            <?=$tool->mainView?>
         </div>
      </td>
   </tr>
   <tr>
      <td class="devlHorzLine"><div></div></td>
      <td class="devlVertDlim"><div></div></td>
      <td class="devlHorzLine"><div></div></td>
   </tr>
   <tr>
      <td class="devlSideFoot">
         <?=$tool->sideFoot?>
      </td>
      <td class="devlVertDlim"><div></div></td>
      <td class="devlMainFoot">
         <?=$tool->mainFoot?>
      </td>
   </tr>
</table>

