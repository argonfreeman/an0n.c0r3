if(!window.dump)
{
   window.dump=function(){console.log.apply(console,([].slice.call(arguments)));};
};



if((typeof window.Select)!='function')
{
   window.Select=function(d,h)
   {
      if(!h){h=document}; var f,r,a,n,s; a=[].slice.call(arguments); s=a.length; f='querySelectorAll'; r=[]; a.forEach((v)=>
      {
         n=h[f](':scope '+v); if((n.length<1)&&(v[0]=='#')){n=h[f](':scope [name='+v.substr(1)+']')};
         if(n.length<1){return r}; ([].slice.call(n)).forEach((i)=>{r.push(i)});
      });
      return r;
   };
};



function xurl(p,d,f)
{
   var x=(new XMLHttpRequest()); x.open('POST',('//'+location.host+p)); x.setRequestHeader("Accept","application/json");
   x.addEventListener('loadend',function(e){if(this.status!==200){repl.say(this.status+' - '+this.statusText);return};f(this.response);});
   x.send(JSON.stringify(d));
};



function xcmd(c,a,f)
{
   if(a.callee){a=[].slice.call(a)};
   xurl('/c0r3/lib/repl/exec',{pwd:repl.vars.pwd,cmd:c,arg:a},function(r){if(r==='huh?'){repl.say(r);}else{f(r)}})
};

function cli2args(s, r,b,q)
{
   r=[]; b=''; q=0; s=s.trim(); if(s.length<1){return r}; s.split('').forEach(function(c)
   {if((c=='"')||(c=="''")||(c=="``")){q=(q?0:1);return}; if(q||(!q&&(c!=' '))){b+=c;return}; if(b!=''){r.push(b);b=''};});
   r.push(b); return r;
};


window.repl = // object
{
   vars:
   {
      data:{}, pwd:'/', autoUser:{name:'anonymous',mail:'anon@example.dom',role:['surfer'],rank:null},
   },

   ready:function()
   {
      let p,c; p=Select('#replProc'); c=Select('.replProm').pop();
      if(!c){p.innerHTML='<pre class="replProm"></pre>'; c=Select('.replProm').pop()};
      c.innerHTML=('['+this.vars.user.name+' '+this.vars.pwd+']$');
   },

   say:function(x){this.echo(('<span style="color:#444">'+escapeHtml(x)+'</span>'));},
   pre:function(x){this.echo(Select('#replProc').innerHTML+'&nbsp;'+x);},

   client:function()
   {
      let a=([].slice.call(arguments)); a=a.join(' '); if(a.length<1){this.say('huh?'); return};
      let r=eval(a); repl.say(r);
   },

   server:function()
   {
      let a=([].slice.call(arguments)); a=a.join(' '); if(a.length<1){this.say('huh?'); return};
      xcmd('run',a,(r)=>{repl.say(r);});
   },

   exec:function(x,c,a)
   {
      this.pre(x); x=x.split('\n')[0]; a=cli2args(x); c=a.shift(); if(c.length<1){return;};

      if((typeof this[c])!='function'){this.echo(c+' <span style="color:#444">.. huh?</span>'); return;}; let ur=this.vars.user.role;
      if
      (
         (((ur.indexOf('leader')<0)&&(ur.indexOf('geeker')<0))||(this.vars.user.rank<1))
         &&(['su','whoami','whatami','clear','exit','passwd'].indexOf(c)<0)
      )
      {this.say("access denied"); return}; this[c].apply(this,a);
   },


   echo:function(x)
   {
      // Select('#replFlog')[0].innerHTML += (Select('#replProc')[0].innerHTML+'&nbsp;'+x+'<br>');
      // x=x.split('\n').join('<br>');
      if((x.length>3)&&(x.substr(0,3)=='.. ')){this.say(x.substr(3)); return};
      let n=Select('#replFlog'); n.innerHTML += (x+'\n'); n.style.display='block';
      Select('#replView').scrollTop = Select('#replView').scrollHeight;
      Select('#replFeed').focus();
   },


   whoami:function()
   {
      this.echo(DATA.USERINFO.name);
   },


   whatami:function()
   {
      this.echo(DATA.USERINFO.role.join(' '));
   },


   clear:function()
   {
      delete this.vars.data; this.vars.data={}; let n=Select('#replFlog');
      if(n){n.innerHTML=''; n.style.display='none';};
      Select('#replFeed').focus();
   },

   reboot:function()
   {
      repl.say('refreshing ...');
      setTimeout(function(){window.parent.location.reload(true);},750);
   },

   su:function(x, self)
   {
      repl.ready(); var n=Select('#replFeed'); var p=Select('.replProm').pop();

      if(!this.vars.data.user&&!this.vars.data.pass&&!x)
      {n.trgt='su'; p.innerHTML='username:'; return;};

      if(!this.vars.data.user&&!this.vars.data.pass&&x)
      {this.vars.data.user=x; n.trgt='su'; p.innerHTML='password:'; n.type="password"; return;};

      if(this.vars.data.user&&!this.vars.data.pass&&x)
      {
         this.vars.data.pass=x; repl.ready(); n.trgt=undefined; n.type="text";
         xcmd('su',this.vars.data,(r)=>
         {
            let f,b; if(r==':DENY:'){repl.echo('access denied'); repl.ready(); return;};
            try{r=JSON.parse(r); if(!r||!r.name||!r.mail||!r.role||!r.role[0]||isNaN((r.rank*1))||!r.hash){f=1}}catch(e){f=1;};
            if(f){repl.echo('the authorization process is broken'); repl.ready(); return;};
            DATA.USERINFO=r; repl.vars.user=r;  repl.ready(); if(window.w0rkPanl){w0rkPanl.show()};
            if(((r.role.indexOf('hacker')>-1)||(r.role.indexOf('stager')>-1))&&window.dbug&&dbug.prep&&dbug.prev){dbug.prep(dbug.prev);};
            repl.say('logged in'); repl.reboot();
         });
         delete this.vars.data; this.vars.data={};
      }
   },


   passwd:function(x, self)
   {
      repl.ready(); var n=Select('#replFeed'); var p=Select('.replProm').pop();

      if(!this.vars.data.oldp&&!this.vars.data.newp&&!x)
      {repl.say('changing password...'); n.trgt='passwd';  p.innerHTML='old password:'; n.type="password";  return;};

      if(!this.vars.data.oldp&&!this.vars.data.newp&&x)
      {this.vars.data.oldp=x; n.trgt='passwd'; p.innerHTML='new password:'; n.type="password"; return;};

      if(this.vars.data.oldp&&!this.vars.data.newp&&x)
      {
         this.vars.data.newp=x; repl.ready(); n.trgt=undefined; n.type="text";
         xcmd('passwd',this.vars.data,(r)=>
         {
            if(r!='OK'){repl.echo(r); repl.ready(); return;}; repl.say(r); cookie.delete('USERINFO');
            repl.exit();
         });
         delete this.vars.data; this.vars.data={};
      }
   },


   pwd:function(){this.echo(this.vars.pwd);},


   cd:function(p)
   {
      let cd=this.vars.pwd; if(!p||(p==='/')||(p==='.')||((cd==='/')&&(p==='..'))){this.vars.pwd='/'; repl.ready(); return};
      xcmd('cd',arguments,(r)=>{if(!isPath(r)){repl.say(r);return}; this.vars.pwd=r; repl.ready()});
   },


   ls:function()
   {xcmd('ls',arguments,(r)=>{repl.echo(r);});},


   git:function()
   {
      let a=([].slice.call(arguments)).join(' ');
      xcmd('git',a,(r)=>{repl.echo(r);});
   },

   touch:function(p,m)
   {
      let h=this.vars.pwd; if(!isText(p)||(p.length<1)||(p=='/')||(p=='.')||(p=='..')||(p==h)){repl.say('?'); return;};
      if((p.length>2)&&((p[0]+p[1])=='./')){p=(h+'/'+p.substr(2))}else if(p[0]!='/'){p=(h+'/'+p)}; if(!m){m=''};
      xcmd('touch',(p+' '+m),(r)=>{repl.echo(r);});
   },

   mkdir:function(p,m)
   {
      let h=this.vars.pwd; if(!isText(p)||(p.length<1)||(p=='/')||(p=='.')||(p=='..')||(p==h)){repl.say('?'); return;};
      if((p.length>2)&&((p[0]+p[1])=='./')){p=(h+'/'+p.substr(2))}else if(p[0]!='/'){p=(h+'/'+p)}; if(!m){m=''};
      xcmd('mkdir',(p+' '+m),(r)=>{repl.echo(r);});
   },

   within:function()
   {
      let l,s,p,c,a,f; l=([].slice.call(arguments)); p=l.shift(); c=l.shift(); s=l.length; a={purl:p,crud:c,args:null}; f=0;
      if(!p||!c){repl.say('invalid arguments');return}; if((s===1)&&((l[0].indexOf('::')>0)||(l[0].indexOf(':')<1))){a.args=l[0];}
      else if(s>0){a.args={}; l.forEach((i)=>{i=i.split(':'); if(!i[1]){f=1}; if(f){return}; a.args[i[0]]=i[1]});};
      if(f){repl.say('invalid arguments');return}; xcmd('within',a,(r)=>{repl.echo(r);});
   },

   exit:function()
   {
      cookie.delete('USERINFO'); repl.say('logged out'); repl.reboot();
   },

   useradd:function()
   {
      let a=([].slice.call(arguments)); repl.mkusr.apply(this,a);
   },

   mkusr:function()
   {
      let a=([].slice.call(arguments)).join(' '); a=a.split(' -m').join('').split(' -G').join('').trim().split(' ');
      if(a.length<3){repl.say('invalid arguments'); return};
      xcmd('mkusr',a,(r)=>{repl.say(r);});
   },

   lsusr:function()
   {
      let a=([].slice.call(arguments));
      xcmd('lsusr',a,(r)=>{repl.say(r);});
   },

   rmusr:function()
   {
      let a=([].slice.call(arguments));
      xcmd('rmusr',a,(r)=>{repl.say(r);});
   },

   mkgrp:function()
   {
      let a=([].slice.call(arguments));
      xcmd('mkgrp',a,(r)=>{repl.say(r);});
   },

   lsgrp:function()
   {
      let a=([].slice.call(arguments));
      xcmd('lsgrp',a,(r)=>{repl.say(r);});
   },

   rmgrp:function()
   {
      let a=([].slice.call(arguments));
      xcmd('rmgrp',a,(r)=>{repl.say(r);});
   },

   config:function()
   {
      let a=([].slice.call(arguments));
      xcmd('config',a,(r)=>{repl.say(r);});
   },

   mail:function()
   {
      let a=([].slice.call(arguments));
      xcmd('mail',a,(r)=>{repl.say(r);});
   },

   invite:function()
   {
      let a=([].slice.call(arguments));
      xcmd('invite',a,(r)=>{repl.say(r);});
   },

   banish:function()
   {
      let a=([].slice.call(arguments));
      xcmd('banish',a,(r)=>{repl.say(r);});
   },
};






repl.vars.user=repl.vars.autoUser;

Select('#replFeed').style.display='block';
Select('#replFeed').addEventListener('keydown',function(event)
{
   let k=event.keyCode; if(!this.hist){this.hist={indx:0,list:['']};};

   if(k==13)
   {
      let v=this.value.trim(); let i=this.hist.list.length;
      let t=(this.trgt||'exec'); if(v&&!this.trgt){this.hist.list[i]=v; this.hist.indx=(i+1);};
      repl[t](v); this.value=''; return;
   };

   if((k==38)||(k==40))
   {
      let h,i,l,s; h=this.hist; i=this.hist.indx; l=this.hist.list; i=((k==38)?(i-1):(i+1)); if(!l[i]){return;};
      this.value=l[i]; this.hist.indx=i; s=this; setTimeout(function(){s.selectionStart=s.selectionEnd=(s.value.length+1);},0); return;
   };
});



if(!window.DATA)
{
   window.DATA={USERINFO:repl.vars.autoUser};
   if(window.cookie){DATA.USERINFO=cookie.select('USERINFO');};
}


(function()
{
   var tmi=setInterval(function()
   {
      if(!window.DATA||!DATA.USERINFO){return}; clearInterval(tmi); clearInterval(tmo);
      repl.vars.user=DATA.USERINFO; //repl.echo('Hello '+repl.vars.user.name);
      repl.ready();
      repl.clear();
   },50);

   var tmo=setTimeout(function(){clearInterval(tmi); clearInterval(tmo);},5000);
}());
