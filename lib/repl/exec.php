<?
namespace Anon;



class repl
{
   static $meta;

   static function init()
   {
      $f=vars('client')->cmd; if(!$f||!isin(NAVIPATH,'repl/exec')){return;};
      if(!isin(['su','passwd'],$f)){permit([role=>['leader','geeker'],rank=>1]);};
      $a=vars('client')->arg; self::$meta->pwd=vars('client')->pwd; if(!self::$meta->pwd){self::$meta->pwd='/';};

      if(($f!=='su')&&($f!=='config'))
      {
         $m=MAILFROM; if(!$m){emit('config MAILFROM is undefined');}; if(!isin($m,'mail://')){emit('invalid MAILFROM URL');};
      };

      $r=call("Anon\\repl::$f",$a); ob_end_clean(); emit($r);
   }

   static function run()
   {
      $a=vars('client')->arg; $r='OK'; eval("namespace Anon;\n".'$r'."=$a"); return $r;
   }

   static function su($a)
   {
      $r=permit([auth=>['user'=>$a->user,'pass'=>$a->pass]]); if(!$r){return DENY;};
      cookie::create('USERINFO',$r); render::direct($r);
   }

   static function cd($p)
   {
      $cd=self::$meta->pwd; if(!$p||($p==='/')||($p==='.')||(($cd==='/')&&($p==='..'))){return '/';};
      if($p==='..'){$xd=trim($cd,'/'); if(!isin($xd,'/')){return '/';}; $p=frag($cd,'/'); rpop($p); $cd=fuse($p,'/'); return "/$cd";};
      $p=trim(trim($p,'.'),'/'); $tp=('/'.trim("$cd/$p",'/')); $pc=path::code($tp);
      if($pc!==200){$w=(($pc===404)?'undefined':'forbidden'); return "`$tp` is $w";}; $tp=swap($tp,'..',''); $tp=('/'.trim($tp,'/'));
      return $tp;
   }

   static function ls($o=null,$p=null)
   {
      $cd=self::$meta->pwd; if($o&&(isPath($o)||isWord($o))){$x=$p; $p=$o; $o=$x;}; if(span($p)<1){$p='';}; $p=trim(trim($p,'.'),'/');
      $tp=('/'.trim("$cd/$p",'/')); $pc=path::code($tp); if($pc!==200){$w=(($pc===404)?'undefined':'forbidden'); return "`$tp` is $w";};
      if(!$o||($o[0]!=='-')||!isin($o,'a')){$l=path::scan($tp); $r=keys($l); $r=implode($r,"\n"); return $r;};
   }

   static function git($a)
   {
      if(!isTool('repo')){import('/c0r3/lib/repo/aard.php');}; $p=self::$meta->pwd;
      $r=repo::adjure($a,$p); return $r;
   }

   static function touch($a)
   {
      $a=frag($a,' '); if(!isset($a[1])){$a[1]='';}; $p=$a[0]; $m=strtoupper(trim(trim($a[1]),'-'));
      $m=((($m==='R')||($m==='RO'))?RF:((($m==='W')||($m==='RW'))?WF:null));
      $r=path::make($p,'',FILE,$m); return ($r?OK:FAIL);
   }

   static function mkdir($a)
   {
      $a=frag($a,' '); if(!isset($a[1])){$a[1]='';}; $p=$a[0]; $m=strtoupper(trim(trim($a[1]),'-'));
      $m=((($m==='R')||($m==='RO'))?RF:((($m==='W')||($m==='RW'))?WF:null));
      $r=path::make($p,null,FOLD,$m); return ($r?OK:FAIL);
   }

   static function within($a)
   {
      $p=$a->purl; $c=$a->crud; $a=$a->args; if(isWord($p)&&isUpperCase($p)){$p=defn($p);}; try{$r=target($p)->$c($a);}
      catch(Exception $e){$m=$e->getMessage(); $p=$e->getFile(); $l=$e->getLine(); $r=crop("$m   $p   ($l)");};
      return $r;
   }

   static function mkusr($u,$m,$g)
   {
      if(!locate(User(USERNAME,'role'),['ganger','leader'])){return 'permission denied';}; $f='invalid group-name';
      if(!isWord($u)){return 'invalid username';}; if(!isMail($m)){return 'invalid email';}; if(!isText($g)){return $f;};
      $l=target(AUTHDATA)->select([using=>'role',fetch=>'name',shape=>'[name]']); $g=frag($g,','); if(!locate($l,$g,XACT)){return $f;};
      $r=target(AUTHDATA)->select([using=>'user',fetch=>['mail'],where=>"name = '$u'"]); if(span($r)>0){return "`$u` exists";};
      $r=target(AUTHDATA)->select([using=>'user',fetch=>['name'],where=>"mail = '$m'"]); if(span($r)>0){return "`$m` exists";};

      $p=random(); $b=import('/w0rk/user/noob.md',['username'=>$u,'password'=>$p,'clanlist'=>fuse($g,' ')]);
      $h=frag($b,"\n")[0]; $h=stub($h,'# '); if(!$h){fail("`/w0rk/user/noob.md` needs a heading");}; $h=$h[2];
      requires('/c0r3/lib/edit/Parsedown.php'); $pd=(new \Parsedown()); $pd->setBreaksEnabled(true); $b=$pd->text($b);
      $b=import('c0r3/lib/mail/page.htm',['html'=>$b]); if(!online()){return 'server internet connection issue';};

      $r=within(MAILFROM)->insert
      ([
         'destAddy' => $m,
         'mesgHead' => $h,
         'mesgBody' => $b,
      ]);

      if($r->fail)
      {
         $f=$r->fail; $m="Cannot send mail\n.."; if(isin($f,'SMTP connect() failed'))
         {$i=path::info(MAILFROM); $u="$i->user@$i->host"; return "$m Make sure `$u` allows API access, check its inbox; see Anon manual";};
         return "$m $f\n.. make sure the mailbox exists";
      };

      $r=null;

      $r=target(AUTHDATA)->insert([using=>'user',write=>[$u,hashed($p),$m,0]]);
      $d="/w0rk/user/$u/data"; path::copy('/c0r3/lib/data/user.php',"$d/cols.php"); $c=target("sqlite::$d"); $c->vivify();
      foreach($g as $i){$c->insert([using=>'clan',write=>[$i,0]]);}; $c->pacify();
      return 'OK';
   }

   static function lsusr($c=null,$w=null)
   {
      $r=target(AUTHDATA)->select([using=>'user',fetch=>['name','mail','rank'],where=>['name != master','rank > 0'],shape=>'[name mail rank]']);
      $r=fuse($r,"\n");
      return $r;
   }

   static function passwd($v)
   {
      $u=USERNAME; $r=permit([auth=>['user'=>$u,'pass'=>$v->oldp]]); if(!$r){return DENY;}; $p=hashed($v->newp);
      $r=target(AUTHDATA)->update([using=>'user',where=>"name = $u",write=>['pass'=>$p]]); if($r->done===1){return 'OK';};
      return DENY;
   }

   static function rmusr($u,$f=null)
   {
      if(!isWord($u)){return 'invalid username';}; $cu=User(USERNAME); $tu=User($u); if(!isin($cu->role,'leader')){return 'permission denied';};
      if(!$tu){return "user `$u` is undefined";};
      if(isin($tu->role,'leader')&&(USERNAME!=='master')){return 'only `master` can delete leader users';};
      $r=target(AUTHDATA)->delete([using=>'user',where=>"name = $u"]); path::void("/w0rk/user/$u");
      return 'OK';
   }

   static function mkgrp($g)
   {
      if(!isWord($g)){return 'invalid group-name';}; $l=target(AUTHDATA)->select([using=>'role',fetch=>'name',shape=>'[name]']);
      if(isin($l,$g)){return "group `$g` already exisis";}; target(AUTHDATA)->insert([using=>'role',write=>[$g]]);
      return 'OK';
   }

   static function lsgrp()
   {
      $r=target(AUTHDATA)->select([using=>'role',fetch=>'name',shape=>'[name]']); $r=fuse($r,"\n");
      return $r;
   }

   static function rmgrp($g)
   {
      if(!isWord($g)){return 'invalid group-name';}; $k='peeker surfer backer worker sorter dealer minder drawer geeker ganger leader';
      if(isin($k,$g)){return 'permission denied';}; $r=target(AUTHDATA)->delete([using=>'role',where=>"name = $g"]);
      return 'OK';
   }

   static function config($t,$k=null,$v=null,$m=null)
   {
      if(!isWord($t)){return 'expecting 1st arg as word';}; $db=within('sqlite::/c0r3/var/data/conf'); $x=$db->exists($t);

      if(!$x&&(($k===null)||($v===null))){return "conf table `$t` is undefined";};
      if($k===null){$r=$db->select([using=>$t,fetch=>'*',parse=>false,shape=>'[name valu]']); $r=fuse($r,"\n"); return $r;};

      if(!isWord($k)){return 'expecting 2nd arg as word';};
      if($v===null){$r=$db->select([using=>$t,fetch=>'*',parse=>false,where=>"name = $k",shape=>'[valu]']); return ((span($r)>0)?$r[0]:null);};

      if(!$db->exists($t))
      {
         $db->create
         ([
            $t=>
            [
               'name' => ['base'=>'TEXT NOT NULL UNIQUE', 'test'=>':numr:', 'auto'=>null],
               'valu' => ['base'=>'TEXT NOT NULL', 'test'=>':text:', 'auto'=>null],
               'meta' => ['base'=>'TEXT NOT NULL', 'test'=>':bool:', 'auto'=>false],
            ]
         ]);
      };
      $sr=$db->select([using=>$t,fetch=>'*',parse=>false,where=>"name = $k"]);
      if(span($sr)<1){$db->insert([using=>$t,write=>[$k,$v]]);}
      else{$db->update([using=>$t,where=>"name = $k",write=>['valu'=>$v]]);};
      $db->pacify(); return 'OK';
   }

   static function mail($a,$h,$b,$f=null)
   {
      if(!online()){return 'server internet connection issue';};
      if(!$m){return 'config MAILFROM is undefined';}; if(!isin($m,'mail://')){return 'invalid MAILFROM URL';};

      $r=within(MAILFROM)->insert
      ([
         'destAddy' => frag($a,','),
         'mesgHead' => $h,
         'mesgBody' => $b,
         'attached' => $f,
      ]);

      return ($r->fail?$r->fail:'OK');
   }

   static function invite($u,$g)
   {
      $gl=frag($g,','); $ug=User($u,'role'); if(!$ug){emit("user `$u` is undefined");}; $c=target("sqlite::/w0rk/user/$u/data"); $c->vivify();
      $rg=target('sqlite::/c0r3/var/data/auth')->select([using=>'role',fetch=>'name',shape=>'[name]']); foreach($gl as $gn)
      {
         if(!isin($rg,$gn)){emit("clan `$gn` is undefined");}; if(isin($ug,$gn)){continue;};
         if(USERNAME!=='master')
         {
            if($u===USERNAME){emit('it is not polite to impose');};
            if(!isin(USERTYPE,$gn)){emit('you cannot invite somebody to a clan you don\'t belong to');};
            if(!isin(USERTYPE,['ganger','leader'])){emit('only gangers and leaders can invite');};
            if(($gn==='ganger')&&!isin(USERTYPE,'leader')){emit('only leaders can invite gangers');};
            if($gn==='leader'){emit('only `master` can invite leaders');};
         };
         $c->insert([using=>'clan',write=>[$gn,0]]);
      };
      $c->pacify(); return 'OK';
   }

   static function banish($u,$g,$FU=null)
   {
      $gl=frag($g,','); $ug=User($u,'role'); if(!$ug){emit("user `$u` is undefined");}; $c=target("sqlite::/w0rk/user/$u/data"); $c->vivify();
      $rg=target('sqlite::/c0r3/var/data/auth')->select([using=>'role',fetch=>'name',shape=>'[name]']); foreach($gl as $gn)
      {
         if(!isin($rg,$gn)){emit("clan `$gn` is undefined");}; if(!isin($ug,$gn)){continue;};
         if(USERNAME!=='master')
         {
            if(($u===USERNAME)&&($FU===null)){emit('how dark .. i like that! .. append "eat sh!t bitchez" .. i won\'t tell if you don\'t');};
            if(!isin(USERTYPE,$gn)){emit('you cannot banish somebody from a clan you don\'t belong to');};
            if(!isin(USERTYPE,['ganger','leader'])){emit('only gangers and leaders can banish');};
            if(($gn==='ganger')&&!isin(USERTYPE,'leader')){emit('only leaders can banish gangers');};
            if($gn==='leader'){emit('only `master` can banish leaders');};
         };
         $c->delete([using=>'clan',where=>"role = $gn"]);
      };
      $c->pacify(); return 'OK';
   }



   static function __callStatic($n,$a)
   {
      return 'huh?';
   }
}
