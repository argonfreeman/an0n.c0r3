<?
namespace Anon;


# tool :: sqlite_plug : embedded database abstraction
# ---------------------------------------------------------------------------------------------------------------------------------------------
   class sqlite_plug
   {
      private $mean=null;
      private $link=null;


      function __construct($x)
      {
         $p=$x->path; $t=path::type($p); if($t==='none'){$x->path="$p.sdb";}elseif($t==='fldr'){$x->path="$p/base.sdb";}; $p=$x->path;
         $this->mean=$x; $h=path::twig($p); $b=path::leaf($p); $this->mean->twig=$h; $this->mean->base=$b; $this->parsed=true;
         $td=expect('path:R,F')->from("$h/cols.php"); $this->{':defn:'}=import($td);
         // $td=expect("$h/cols.php")->as(['path'=>[R,F]]); $this->{':defn:'}=import($td);
      }


      function __destruct()
      {
         $this->pacify();
      }


      function __call($n,$a)
      {
         return call($this->$n,$a);
      }


      function vivify()
      {
         if($this->link){return $this->link;}; $p=path($this->mean->path); if(!isFile($p)||(path::size($p)<1)){$this->create();};
         $this->link=(new \SQLite3($p, SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE));
         $this->link->busyTimeout(6); $this->link->enableExceptions(true);
         return $this->link;
      }


      function pacify()
      {
         if($this->link){$this->link->close(); $this->link=null; return true;};
      }


      function adjure($q,$b=[],$l=null)
      {
         if(is_string($q)){$q=trim($q); $q=trim($q,';'); $q.=';';}; if(!isText($q,10)){fail('invalid SQL');}; $a=strtoupper(stub($q,' ')[0]);
         $l=(!$this->link?0:1); $c=$this->vivify();

         if(locate(['SELECT','INSERT','UPDATE','DELETE','PRAGMA'],$a))
         {
            try{$x=$c->prepare($q);}catch(\Exception $f)
            {
               $m=$f->getMessage(); if(isin($m,'Unable to prepare statement: 5, database is locked'))
               {$p=$this->mean->path; $m="database `$p` is locked";}; fail::sharing("$m\n\nQUERY:\n$q");
            };

            foreach($b as $k => $v){$x->bindValue($k,$v);}; do
            {
               $r=$x->execute(); $f=($c->lastErrorCode()?lowerCase($c->lastErrorMsg()):null); if($f){if(isin($f,'lock')){wait(1);}else
               {$p=$this->mean->path;$b=$this->mean->base;$t=$this->mean->tabl; fail("$f ..\nPATH: `$p`\nBASE: `$b`\nEXEC: `$q`"); $f=null;};};
            }while($f);

            if($a==='SELECT')
            {
               $n=$r->numColumns(); if($n<1){$this->pacify(); return [];}; $p=($this->parsed?V:null);
               $z=[]; while($i=$r->fetchArray(SQLITE3_ASSOC)){$z[]=tron($i,$p);}; if(!$l){$this->pacify();}; return $z;
            };
            if($a==='INSERT'){$z=tron(['deed'=>$a,'done'=>$c->changes(),'last'=>$c->lastInsertRowID()]);if(!$l){$this->pacify();}; return $z;};
            if(($a==='UPDATE')||($a==='DELETE')){$z=tron(['deed'=>$a,'done'=>$c->changes()]); if(!$l){$this->pacify();}; return $z;};
            if($a==='PRAGMA'){$z=[]; while($i=$r->fetchArray(SQLITE3_ASSOC)){$z[]=tron($i,V);}; if(!$l){$this->pacify();}; return $z;};
         };

         if(!$l){$this->pacify();}; return true;
      }


      function create($h=null)
      {
         $tf='invalid table definition'; $cf='invalid column definition'; $d=path($this->mean->path);
         $l=(new \SQLite3($d, SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE));
         $this->link=$l; if(isScal($h)){$h=tron($h);}; if(isTron($h)){$td=$h;}else
         {
            if(!$h){$h=path(path::twig($d));}; expect('path:D,W')->from($h);
            $td=$this->{':defn:'}; $tr=(isFile("$h/rows.php")?import("$h/rows.php"):tron());
            expect('tron')->from($td); expect('tron')->from($tr); if(span($td)<1){return;};
         };

         foreach($td as $tn => $tc)
         {
            if(!isTron($tc)||(span($tc)<1)){fail($tf);}; $q="CREATE TABLE $tn "; $q.='('; foreach($tc as $cn => $cd)
            {if(!isWord($cn)||!isTron($cd)||!isText($cd->base)){fail($cf);}; $q.="$cn $cd->base, ";}; $q=(rtrim($q,', ').");\n"); $l->exec($q);
            if(!isin($tr,$tn)||!isTupl($tr->$tn)||(span($tr->$tn<1))){continue;}; if(!isin($this,":$tn:")){$this->{":$tn:"}=keys($tc);};
            foreach($tr->$tn as $i){$this->insert($i,$tn);};
         };
         $this->pacify(); wait(50); chmod($d,0777); wait(50); return true;
      }


      function descry($x='*')
      {
         if(isWord($x))
         {
            $this->mean->tabl=$x;
            $y=(!$this->link?1:0); $this->vivify(); $i=$this->adjure("PRAGMA table_info('$x')"); if(count($i)<1){$this->pacify(); return;};
            $n=$this->adjure("SELECT COUNT(rowid) AS 'rows' FROM $x"); $n=$n[0]->rows; $c=tron();
            $l=$this->adjure("SELECT rowid AS 'last' FROM $x ORDER BY rowid DESC LIMIT 1"); $l=((count($l)>0)?$l[0]->last:0);
            foreach($i as $k => $v)
            {
               $c->{$v->name}=tron
               ([
                  'name'=>$v->name,'type'=>$v->type,'pkey'=>($v->pk?true:false),
                  'ordr'=>$v->cid,'dflt'=>$v->dflt_value,'null'=>($v->notnull?false:true)
               ]);
            };

            if($y){$this->pacify();}; return tron(['name'=>$x,'cols'=>$c,'rows'=>$n,'lrid'=>$l]);
         };

         if($x==='*')
         {
            $this->vivify(); $l=$this->adjure("SELECT name AS 'table' FROM sqlite_master WHERE type='table'");
            $r=tron(); foreach($l as $i){$r->{$i->table}=$this->descry($i->table);}; $this->pacify(); return $r;
         }
      }


      function exists()
      {
         $a=func_get_args(); if(isset($a[0])&&isTupl($a[0])){$a=$a[0];}; $s=span($a); $f=[]; $p=path($this->mean->path);
         $e=isFile($p); if(!$e){return false;}; if($s<1){return true;}; if(($s>0)&&(filesize($p)<2)){return false;};
         $this->vivify(); $e=0; foreach($a as $i)
         {$i=explode(':',$i); $t=$i[0]; $c=(isset($i[1])?$i[1]:0); $d=$this->descry($t); if(!$d){continue;}; $e+=(!$c?1:($d->cols->$c?1:0));};
         $this->pacify(); return ($s===$e);
      }


      function insert($x,$t=null)
      {
         if(!$this->link||!$t)
         {
            if(isScal($x)){$x=tron($x,U);}; if(!isTron($x)){fail('expecting :scal: or :tron:');};
            if(!isWord($x->using)){fail('expecting `using` as :word:');}; $t=$x->using; if(!$this->link){$this->vivify();};
            $d=$this->descry($t); if(!$d){fail("table `$t` is undefined");}; $l=keys($d->cols); $this->{":$t:"}=$l; $w=$x->write;
            $z=tron(['done'=>0,'last'=>0]); if(span($w)<1){return $z;}; expect::{'tupl tron scal'}($w);
            if(isTupl($w)&&!isTupl($w[0])&&!isScal($w[0])&&!isTron($w[0])){$w=[$w];};
            if(!isTupl($w)){$w=[$w];}; $z=tron(['deed'=>'INSERT','done'=>0,'last'=>0]); $this->mean->tabl=$x->using;
            foreach($w as $i){$r=$this->insert($i,$x->using); $z->done++; $z->last=$r->last;}; $this->pacify(); return $z;
         }

         if(isTupl($x))
         {
            $c=$this->{":$t:"}; $y=tron(); foreach($c as $k => $v){$y->$v=(isset($x[$k])?$x[$k]:'');}; $x=$y;
         };

         $ref=[]; $k=keys($x); $v=vals($x); $c=implode($k,', '); $sql="INSERT INTO $t ($c) VALUES"; foreach($v as $vk => $vv)
         {$n=($k[$vk]); if(!isText($vv)&&!isNumr($vv)){$vv=texted($vv);}; $vr=":{$n}_write"; $ref[$vr]=$vv; $v[$vk]=$vr;};
         $v=implode($v,', '); $sql.=" ($v)"; $r=$this->adjure($sql,$ref); if($r){return $r;}; return tron(['done'=>0,'last'=>0]);
      }


      function select($x='*')
      {
         if($x==='*'){return $this->descry();}; $ref=[]; $q=(isScal($x)?tron($x,U):$x); $x=null; $alt=[]; $tbl='';
         if(!isTron($q)){fail('expecting :scal: or :tron:');}; $sql='SELECT '; $opr=padded((explode(' ',EXPROPER)),' ');
         if($q->count&&is_string($q->count)){$q->count=[$q->count];}; if($q->fetch&&is_string($q->fetch)){$q->fetch=[$q->fetch];};
         if($q->using&&is_string($q->using)){$q->using=[$q->using];}; if($q->where&&is_string($q->where)){$q->where=[$q->where];};

         if(isin($q,'parse')){if(!$q->parse){$this->parsed=false;}};

         if($q->count){$x=$q->count; foreach($x as $k => $v){$x[$k]=swap($v,':',' AS ');}; $sql.=implode($x,', '); unset($x,$k,$v);};

         if($q->alter)
         {
            $a=$q->alter; if(isText($a)){$a=[$a];}; if(!isTupl($a)){fail('expecting `alter` as :text: or :tupl:');};
            foreach($a as $i){if(!locate($i,[':','@','.'],XACT)){fail('invalid alter expression');}; $i=explode(':',$i); $alt[$i[0]]=$i[1];};
         };

         if($q->fetch)
         {
            $x=$q->fetch; foreach($x as $k => $v)
            {
               if(locate($v,[':','@','.'],XACT)){$v=explode(':',$v); $alt[(isset($v[2])?$v[2]:$v[0])]=$v[1];unset($v[1]);$v=implode($v,':');};
               $x[$k]=swap($v,':',' AS ');
            };
            $sql.=implode($x,', '); unset($x,$k,$v);
         };

         if($q->using)
         {
            $x=$q->using; $tbl=implode($x,', '); $this->mean->tabl=$tbl; foreach($x as $k => $v){$x[$k]=swap($v,':',' AS ');};
            $sql.=" FROM $tbl"; unset($x,$k,$v);
         };

         if($q->where)
         {
            foreach($q->where as $k => $v)
            {
               $o=stub($v,$opr); if(!$o||(strlen($o[0])<1)||(strlen($o[2])<1)){fail('invalid `where` expression');}; $l=trim($o[0]);
               $r=trim($o[2]); $o=$o[1]; if(($o===' ~ ')&&(($r[0]==='*')||(substr($r,-1,1)==='*'))){$o=' LIKE ';$r=swap($r,'*','%');};
               $x=":{$l}_where"; $ref["$x"]=unwrap($r); $q->where[$k]="{$l}{$o}{$x}";
            };
            $sql.=(' WHERE '.implode($q->where,' AND ')); unset($x,$k,$v);
         };

         if($q->group){$x=$q->group; $sql.=" GROUP BY $x";};
         if($q->order){$x=$q->order; $p=explode(':',$x); $p[1]=((isset($p[1])&&($p[1]==='ASC'))?'ASC':'DESC'); $sql.=" ORDER BY $p[0] $p[1]";};
         if($q->limit){$x=$q->limit; $sql.=" LIMIT $x";}; $sql.=";"; $this->vivify();

         $r=$this->adjure($sql,$ref); unset($i,$t,$c,$f,$a,$k,$v,$x,$y,$z,$sql); if(count($r)<1){$this->pacify(); return $r;};
         if(count($alt)>0)
         {
            foreach($r as $k => $v)
            {
               foreach($alt as $a => $i)
               {
                  if(!strpos($i,'@')||!strpos($i,'.')||($v->$a===null)){continue;}; $i=explode('@',$i); $f=$i[0]; $i=explode('.',$i[1]);
                  $t=$i[0]; $c=$i[1]; if(!isWord($f)||!isWord($t)||!isWord($c)){fail('invalid sub-query');}; $x=$v->$a; if(!isTupl($x)){$x=[$x];};
                  $z=[]; foreach($x as $y)
                  {
                     if(!isNumr($y)){$y=("'".texted($y)."'");}; $sql="SELECT $f FROM $t where $c = $y LIMIT 1"; $rsl=$this->adjure($sql);
                     if(count($rsl)<1){continue;}; $z[]=$rsl[0]->$f;
                  };
                  if(!isTupl($v->$a)){$z=implode($z);}; $r[$k]->$a=$z;
               };
            };
         };

         $this->pacify();

         if($q->shape)
         {
            unset($k,$v,$z,$i); $x=$q->shape;

            if(is_string($x)&&strpos($x,':'))
            {
               $x=explode(':',$x); $k=trim($x[0]); $v=trim($x[1]); $z=tron(); $dbp=$this->mean->path; $dbn=$this->mean->base;
               if(span($v)<1){fail('invalid `shape` argument');}; $f="is undefined in table: `$tbl` .. in dbase: `$dbn` ($dbp)";
               if(!isWord($v)){if($v==='*'){$v=keys($r[0]);}else{$v=frag(swap($v,[', ',','],' '),' ');};};
               foreach($r as $i)
               {
                  if(!exists($i,$k)){fail("column `$k` $f");};
                  if(isWord($v)){$z->{$i->$k}=$i->$v; continue;}; $z->{$i->$k}=tron(); $c=null;
                  foreach($v as $c){if(!exists($i,$c)){fail("column `$c` $f");}; $z->{$i->$k}->$c=$i->$c;};
               };
               return $z;
            };

            if(is_string($x)&&(wrapOf($x)==='[]'))
            {
               $c=unwrap($x); $k=keys($r[0]); $z=[];
               if(isin($k,$c))
               {
                  foreach($r as $i){$z[]=$i->$c;};
                  return $z;
               };

               if(isin($c,' '))
               {
                  unset($l,$q); $l=frag($c,' ');
                  foreach($r as $i){$t=''; foreach($l as $q){$t.=($i->$q." ");}; $z[]=trim($t);};
                  return $z;
               };

               fail('invalid `shape` argument');
            };
         };

         return $r;
      }


      function update($x)
      {

         $q=(isScal($x)?tron($x,U):$x); $x=null; $alt=[]; $ref=[];
         if(!isTron($q)){fail('expecting :scal: or :tron:');}; $sql='UPDATE '; $opr=padded((explode(' ',EXPROPER)),' ');
         if(!$q->using){fail('expecting `using` as table reference');}; if(!isTupl($q->using)){$q->using=[$q->using];};
         if($q->where&&is_string($q->where)){$q->where=[$q->where];};
         if(!isScal($q->write)&&!isTron($q->write)){fail('expecting `write` as :scal: or :tron:');};

         $x=$q->using; foreach($x as $k => $v){if(!isWord($v)){fail('expecting `using` as :text-list:');}; $x[$k]=swap($v,':',' AS ');};
         $sql.=implode($x,', '); $sql.=' SET '; $z=[]; $this->mean->tabl=implode($x,', '); unset($x,$k,$v);
         foreach($q->write as $k => $v){if(!isText($v)&&!isNumr($v)){$v=texted($v);}; $ref[":{$k}_write"]=$v; $z[]="$k = :{$k}_write";};
         $sql.=implode($z,', ');

         if($q->where)
         {
            foreach($q->where as $k => $v)
            {
               $o=stub($v,$opr); if(!$o||(strlen($o[0])<1)||(strlen($o[2])<1)){fail('invalid `where` expression');}; $l=trim($o[0]);
               $r=trim($o[2]); $o=$o[1]; if(($o===' ~ ')&&(($r[0]==='*')||(substr($r,-1,1)==='*'))){$o=' LIKE ';$r=swap($r,'*','%');};
               $x=":{$l}_where"; $ref["$x"]=unwrap($r); $q->where[$k]="{$l}{$o}{$x}";
            };
            $sql.=(' WHERE '.implode($q->where,' AND ')); unset($x,$k,$v);
         };

         if($q->limit){$x=$q->limit; $sql.=" LIMIT $x";}; $sql.=";";
         $r=$this->adjure($sql,$ref); $this->pacify(); return $r;
      }


      function delete($x)
      {
         $q=(isScal($x)?tron($x,U):$x); if(!isTron($q)){fail('expecting :scal: or :tron:');}; $x=null; $alt=[]; $ref=[];
         if($q->purge)
         {
            $t=$q->purge; expect::word($t); $r=$this->adjure("DELETE FROM $t;");
            // $this->adjure("DELETE FROM SQLITE_SEQUENCE WHERE name='$t';");
            $this->pacify(); return $r;
         };
         $sql='DELETE FROM '; $opr=padded((explode(' ',EXPROPER)),' ');
         if(!$q->using){fail('expecting `using` as table reference');}; if(!isTupl($q->using)){$q->using=[$q->using];};
         if($q->where&&is_string($q->where)){$q->where=[$q->where];};

         $x=$q->using; foreach($x as $k => $v){if(!isWord($v)){fail('expecting `using` as :text-list:');}; $x[$k]=swap($v,':',' AS ');};
         $sql.=implode($x,', '); $this->mean->tabl=implode($x,', '); unset($x,$k,$v);

         if($q->where)
         {
            foreach($q->where as $k => $v)
            {
               $o=stub($v,$opr); if(!$o||(strlen($o[0])<1)||(strlen($o[2])<1)){fail('invalid `where` expression');}; $l=trim($o[0]);
               $r=trim($o[2]); $o=$o[1]; if(($o===' ~ ')&&(($r[0]==='*')||(substr($r,-1,1)==='*'))){$o=' LIKE ';$r=swap($r,'*','%');};
               $x=":{$l}_where"; $ref["$x"]=unwrap($r); $q->where[$k]="{$l}{$o}{$x}";
            };
            $sql.=(' WHERE '.implode($q->where,' AND ')); unset($x,$k,$v);
         };

         if($q->limit){$x=$q->limit; $sql.=" LIMIT $x";}; $sql.=";";
         $r=$this->adjure($sql,$ref); $this->pacify(); return $r;
      }


      function verify($x)
      {
         $x=(isScal($x)?tron($x,U):$x); if(!isTron($x)){fail('expecting :scal: or :tron:');};
         expect::{'text tupl'}($x->using); if(!isTupl($x->using)){$x->using=[$x->using];};
         expect::{'text tupl'}($x->where); if(!isTupl($x->where)){$x->where=[$x->where];};
         expect::{'text tupl'}($x->claim); if(!isTupl($x->claim)){$x->claim=[$x->claim];};
         $q=[using=>$x->using,count=>'rowid',where=>fuse($x->where,$x->claim),limit=>1];
         $r=$this->select($q); $z=[tron(['bool'=>false])]; if(count($r)>0){$z[0]->bool=true;}; return $z;
      }


      function ensure($x)
      {
         $x=(isScal($x)?tron($x,U):$x); if(!isTron($x)){fail('expecting :scal: or :tron:');};
         expect::{'text tupl'}($x->using); $tbl=$x->using; // if(!isTupl($x->using)){$x->using=[$x->using];};
         expect::{'text tupl'}($x->where); if(!isTupl($x->where)){$x->where=[$x->where];}; expect::{'tupl'}($x->claim);
         if(count($x->claim)<1){return;}; if(isFlat($x->claim)||isMixd($x->claim)){fail('expecting uniform multi-dimensional array');};
         $t=expect::{'tupl scal tron'}($x->claim[0]); $q=[using=>$tbl,fetch=>'*',where=>$x->where]; $r=$this->select($q); $q=$x->claim;
         if(count($r)<1){$r=$this->insert([using=>$tbl,write=>$q]); return $r;}; $c=keys($r[0]); $so=span($r);

         foreach($q as $k => $v)
         {
            if($t!=='tron'){$v=(($t==='tupl')?infuse($c,$v):tron($v));}; foreach($v as $vk => $vv){if(isText($vv)){$v->$vk=parsed($vv);};};
            $q[$k]=$v;
         };

         $d=diff($r,$q); if(span($d)<1){return true;}; $c=[];$w=[]; foreach($x->where as $i){$c[]=stub($i,' ')[0];};
         foreach($d as $di){foreach($c as $ci){$dc=$di->$ci; $w[]=("$ci = '$dc'");};}; unset($di); $z=tron(['deed'=>'?','done'=>0]);
         $r=$this->select([using=>$tbl,fetch=>'*',where=>$w]); $sq=span($q); $sr=span($r);


         if(($sr<=$sq)&&isin($q,$d))
         {
            if($sr<1){$r=$this->insert([using=>$tbl,write=>$d]); return $r;};
            foreach($d as $di){$r=$this->update([using=>$tbl,where=>$w,write=>$di]); $z->deed='INSERT'; $z->done+=$r->done;}; return $z;
         }
         elseif(($so>$sq)&&isin($r,$d))
         {
            foreach($d as $di){$r=$this->delete([using=>$tbl,where=>$w]); $z->deed='DELETE'; $z->done+=$r->done;}; return $z;
         }
      }


      function modify()
      {
         dump('TODO :: sqlite : modify()');
      }


      function invoke()
      {
         dump('TODO :: sqlite : invoke()');
      }


      function import()
      {
         $a=func_get_args(); if(isset($a[0])){if(isTupl($a[0])){$a=$a[0];}elseif(isScal($a[0])&&isset($a[0][using])){$a=$a[0][using];};};
         if(is_string($a)){$a=[$a];}; $d=null; if(!$this->exists()){$d=$this->create();}; foreach($a as $p)
         {$p=path($p); if($p===$d){continue;}; if(!isFile($p)){fail("expecting `$p` as readable file");}; $this->adjure(path::scan($p));};
         return true;
      }


      function export()
      {
         dump('TODO :: sqlite : export()');
      }
   }
# ---------------------------------------------------------------------------------------------------------------------------------------------



// $qr = file_get_contents(COREPATH."/sys/data.sql");
// $db = (new \SQLite3(COREPATH."/sys/data.sdb", SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE));
// $db->enableExceptions(true);
// $db->query($qr);
// $db->close();

// $db = (new \SQLite3(COREPATH."/sys/data.sdb", SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE));
// $db->enableExceptions(true);
// $pq = $db->prepare('SELECT * from conf');
// $rs = $pq->execute();
// $rn = $rs->numColumns();
//
// while ($row = $rs->fetchArray(SQLITE3_ASSOC))
// {
//    print_r($row); echo "\n\n";
// }
//
// $db->close();

// var_dump($rn);
