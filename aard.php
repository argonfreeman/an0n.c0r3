<?
namespace Anon;


# info :: file : read this
# ---------------------------------------------------------------------------------------------------------------------------------------------
# this file is the server of any interface; it is used to relay/serve requests according to feature, interface and config
# ---------------------------------------------------------------------------------------------------------------------------------------------
   require(psee('$/c0r3/lib/core/abec.php'));
# ---------------------------------------------------------------------------------------------------------------------------------------------




# tool :: c0r3 : handler
# ---------------------------------------------------------------------------------------------------------------------------------------------
   $d=USERDEED; $i=INTRFACE; $m=SITEMODE; $p=NAVIPATH;

   if($m==='DOWN'){render::status(503);}; $c=conf('c0r3/boot')->INTRFACE; if(($c!=='ANY')&&($i!==$c)){render::status(420);};
   $c=conf('c0r3/path')->$p; if(is_int($c)){render::status($c);}; $c=vars('permit')->path; $r=vars('permit')->rank;
   $d=0; if(!$c){$c=tron(['rank'=>$r,'mode'=>$m,'face'=>$i,'deed'=>USERDEED]);}; if($c->rank>$r){$d=401;};
   if($c->face!==$i){$d=420;}; if($c->mode!==$m){$d=428;}; if($c->deed!==USERDEED){$d=405;}; if($d){render::status($d);};

   if($c->face==='TPI')
   {
      $h=path(path::twig($p)); $l=path::leaf($p); $x=path::type($p);
      if($x==='php'){chdir($h); require("./$l"); exit;}; render::direct(path($p));
   };

   $g=path::goal($p); $x=$g->exec; $p=$g->path; $pc=path::code($p); $xc=($x?path::code($x):null);
   if(($x&&($xc!==200))||(!$x&&($pc!==200))){$c=($x?$xc:$pc); render::status($c);}; if(!$x||($i==='GUI')){render::parsed($p);};

   $a=$g->args; $s=span($a); $r=import($x,['_ARGS'=>$a]); $c=null; if(isTool($r)&&(frag($r,0,5)==='Anon\\')){$c=$r; $r=null;};
   if($c&&(!$s||(($s<2)&&($a[0]==='init')))){render::status(200);}elseif($c&&isFunc("$c::$a[0]")){$f=lpop($a); $r=call("$c::$f",$a);}
   elseif($c&&isin($c,'meta')&&!isFunc("$c::meta")&&isTron($c::$meta)){$r=$c::$meta;}; if(($r!==null)&&!isTron($r)){render::direct($r);};
   if(($s<1)||!isin($r,$a[0])){$r=null;};



   if(($r===null)&&($p!==$x)&&(!$s||isin($p,fuse($a,'/'))))
   {
      if(!isin($x,$p)&&exists($p)){render::parsed($p);}; $g=path::goal($p,1); if(!$g->exec){render::parsed($g->path);};
      $x=$g->exec; $a=$g->args; $s=span($a); $r=import($x,['_ARGS'=>$a]);
      if(!isTron($r)){if(($r===null)||($r===true)){render::status(200);}; render::direct($r);};
   };



   $b=''; $f=null; $l=dupe($a); $z=null;
   do{$q=lpop($l); $s=count($l); $b.="/$q"; $z=bore($r,$b); if(isFunc($z)){$f=$z;break;}}while($s);
   if(!$z&&!$f&&(count($a)>0)&&!!$r&&isin($x,'/aard.php')&&isin($p,rtrim($x,'/aard.php')))
   {
      $p=rtrim($x,'/aard.php'); $x=null;
      do{$q=lpop($a); $s=count($a); $p.="/$q"; $x=exists("$p/aard.php"); if(!$x){$x=exists("$p.php");}; if($x){break;}}while($s);
      if($x)
      {
         $b=''; $f=null; $z=null; $r=import($x,['_ARGS'=>$a]);
         do{$q=lpop($a); $s=count($a); $b.="/$q"; $z=bore($r,$b); if(isFunc($z)){$f=$z;break;}}while($s);
      };
   };


   if($f){$r=call($f,$l);}else{$r=$z;}; if($r===null){render::status(($f?200:404));}; render::direct($r);
# ---------------------------------------------------------------------------------------------------------------------------------------------
