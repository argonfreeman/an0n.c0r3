"use strict";

(function()
{
   if(!User('role').hasAny("worker","leader")){return};
   let wait = setInterval(()=>{if(!Select('#w0rkReplPanl')){return};clearInterval(wait); w0rkMenu.init('helpD3vlKnob');},50);
}());


Export
([
   {panl:
   [
      {note:'.cenmid', contents:
      [
         {icon:'tools', size:3},
         {h1:'{:(HOSTNAME):}'},
         {span:'under construction'}
      ]}
   ]}
]);
